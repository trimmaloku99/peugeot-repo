import React, { Component } from "react";
import { Link } from "react-router-dom";
import servicing3 from "../../images/servicing3.jpg";

export default class NderrimiFiltrave extends Component {
  render() {
    return (
      <section id="nderrimifiltrave">
        <h2 className="nav-title">NDËRRIMI I FILTRAVE</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              NDËRRIMI I FILTRAVE
            </li>
          </ol>
        </div>

        <div id="nderrimifiltrave-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={servicing3}></img>
            <p className="col-lg-6 p-4">
              <h5>NDËRRIMI I FILTRAVE</h5>
              <br />
              Filtrat e makinave janë të domosdoshëm nëse doni që makina juaj e
              dashur të funksionojë siç duhet. Kur një nga filtrat tuaj
              bllokohet, mund të shkaktojë probleme dhe të ndikojë në
              performancë. Ju mund të vini re një humbje të energjisë, tym të
              ndotur, dritë vaji ose madje edhe dështim të motorit. Filtrat e
              ndryshëm në makinën tuaj duhet të zëvendësohen rregullisht. Çdo
              makinë ka katër filtra kryesorë: filtri i kabinës, filtri i vajit,
              filtri i karburantit dhe filtri i ajrit. Funksioni i të gjithë
              këtyre filtrave është të lejojë prurjet dhe kapjen e papastërtive:
              pluhurin dhe ndotësit në ajër, papastërtitë në karburant ose
              papastërtinë në vajin e motorit. Nëse filtrat e automjetit tuaj
              nuk zëvendësohen me kohë, ato nuk do të funksionojnë siç duhet dhe
              në fund të fundit, kjo mund të shkaktojë një ndikim negativ në
              automjetin tuaj. Duke zëvendësuar filtrat, makina juaj do të jetë
              më efikase.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
