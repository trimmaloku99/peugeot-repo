import React, { Component } from "react";
import { Link } from "react-router-dom";
import xhama from "../../images/xhama.jpg";

export default class PaketaXhamave extends Component {
  render() {
    return (
      <section id="paketaxhamave">
        <h2 className="nav-title">PAKETA E XHAMAVE</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PAKETA E XHAMAVE
            </li>
          </ol>
        </div>

        <div id="paketaxhamave-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={xhama}></img>
            <p className="col-lg-6 p-4">
              <h5>RIPARIM XHAMIT TË DËMTUAR</h5>
              <br />
              Riparimi i xhamit të dëmtuar 2,500 Lekë përfshin: materiale dhe
              orë punë. <br /> Profesionistët e Peugeot janë këtu për ju për çdo
              dëmtim të xhamit, të cilat janë më të vogla se diametri deri 50 mm
              dhe nuk janë në fushë-pamjen e shoferit. Së pari, bëhet një
              vlerësim nëse dëmi është i riparueshëm, duke përdorur pajisje të
              specializuara të sofistikuara dhe më pas ajo do të riparohet në më
              pak se 1 orë. Në rast se dëmtimi nuk mund të riparohet, juve ju
              ofrohet zëvendësimi i xhamit kundrejt nje çmimi të personalizuar
              vetëm për ju.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
