import React, { Component } from "react";
import { Link } from "react-router-dom";
import intervalisherbimit from "../../images/intervalisherbimit.jpg";

export default class IntervaliSherbimit extends Component {
  render() {
    return (
      <section id="intervalisherbimit">
        <h2 className="nav-title">INTERVALET E SHËRBIMIT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              INTERVALET E SHËRBIMIT
            </li>
          </ol>
        </div>
        <img id="intervalisherbimit-img h-100" src={intervalisherbimit}></img>

        <div id="description-panel-interval">
          <p>
            Kujdes i plotë <br /> Çdo automjet ka ciklin e vet të mirëmbajtjes
            të përcaktuar nga prodhuesi në koordinim me karakteristikat e tij
            teknike. Zbuloni se cilat lloje kontrolli përfshijnë shërbimin e
            automjetit tuaj Peugeot.
          </p>
        </div>

        <div id="intervali">
          <div className="table-responsive">
            <table className="table table-striped">
              <tbody>
                <tr className="border-table">
                  <th scope="col">SHËRBIMI</th>
                  <th scope="col">PËRSHKRIMI</th>
                  <th scope="col">SHËRBIMI I PARË 2.500 km (3 muaj) </th>
                  <th scope="col">ÇDO 5.000 km (6 muaj)</th>
                  <th scope="col">ÇDO 10.000 km (1 vit)</th>
                </tr>
                <tr>
                  <td className="tabletd" rowSpan="2">
                    HEQ/RIMBUSH
                  </td>
                  <td>• Ndërrim i vajit të motorit</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr className="border-table">
                  <td scope="row">• Filtër karburanti</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td className="tabletd" rowSpan="4">
                    ZËVENDËSO
                  </td>
                  <td>• Filtër vaji</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Filtër karburanti</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr>
                  <td scope="row">• Filtri i kabinës</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr className="border-table">
                  <td scope="row">• Filtër ajri</td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td className="tabletd" rowSpan="6">
                    NIVELET
                  </td>
                  <td>• Vaji i motorit</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Alkool i frenave</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr>
                  <td scope="row">• Uji i fshirëseve të xhamit</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr>
                  <td scope="row">• Niveli i antifrizës</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Niveli i vajit të sistemit të drejtimit</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr className="border-table">
                  <td scope="row">• Niveli i UREA</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>{" "}
                <tr>
                  <td className="tabletd" rowSpan="18">
                    KONTROLLE
                  </td>
                  <td>• Sinjalet</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Gjendja e fenerëve dhe tapetet</td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Boria</td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Sprucatorët e ujit të xhamave</td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Fshirëset e xhamit</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">
                    • Gjendja e tensionuesit të rripit të jashtëm
                  </td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Frenat e dorës</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Gjendja e tubave të frenave</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Veshja e ferrotave</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Konsumi i disqeve</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Gjendja e mbrojtëseve plastike</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Amortizatorët</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Gjendja e gomave</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Presioni i gomave</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Kontroll komplet i automjetit</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Rrjedhje gazi</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Kandelet</td>
                  <td></td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
                <tr>
                  <td scope="row">• Gjendja e baterisë</td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                  <td></td>
                  <td className="text-center">
                    <i className="fas fa-check"></i>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </section>
    );
  }
}
