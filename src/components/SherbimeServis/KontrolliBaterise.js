import React, { Component } from "react";
import { Link } from "react-router-dom";
import bateria from "../../images/bateria.jpg";

export default class KontrolliBaterise extends Component {
  render() {
    return (
      <section id="kontrollibaterise">
        <h2 className="nav-title">KONTROLLI I BATERISË</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              KONTROLLI I BATERISË
            </li>
          </ol>
        </div>

        <div id="kontrollibaterise-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={bateria}></img>
            <p className="col-lg-6 p-4">
              <h5>KONTROLLI I BATERISË</h5>
              <br />
              POWER BULL + D - 12V | 44Ah | 390 A(IEC) Përmasat: 210x175x175
              Çmimi: 9,500 Lekë <br /> <br /> POWER BULL + D - 12V | 60Ah | 540
              A(EN) Përmasat: 241x175x175 Çmimi: 10,200 Lekë <br /> <br /> POWER
              BULL + D - 12V | 60Ah | 640 A(EN) Përmasat: 241x175x175 Çmimi:
              10,500 Lekë <br /> <br />
              POWER BULL + D - 12V | 70Ah | 720 A(EN) Përmasat: 278x175x175
              Çmimi: 16,500 Lekë <br /> <br /> POWER BULL + D - 12V | 70Ah | 760
              A(EN)STT Përmasat: 278x175x175 Çmimi: 22,800 Lekë <br /> <br />{" "}
              POWER BULL + D - 12V | 75Ah | 800 A(EN) Përmasat: 278x175x175
              Çmimi: 12,500 Lekë <br /> <br /> Për të përmbushur paketën, është
              e nevojshme të lini akumulatorin (e vjetër) në qendrën e servisit.
              Paketat janë të vlefshme për pjesët rezervë origjinale që gjenden
              në servisin e autorizuar të Peugeot në Shqipëri.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
