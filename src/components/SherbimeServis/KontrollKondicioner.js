import React, { Component } from "react";
import { Link } from "react-router-dom";
import servicing from "../../images/servicing2.jpg";

export default class KontrollKondicioner extends Component {
  render() {
    return (
      <section id="kontrollkondicioner">
        <h2 className="nav-title">KONTROLLI I KONDICIONERIT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              KONTROLLI I KONDICIONERIT
            </li>
          </ol>
        </div>

        <div id="kontrollkondicioner-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={servicing}></img>
            <p className="col-lg-6 p-4">
              <h5>KONTROLLI I KONDICIONERIT</h5>
              <br />
              KONTROLLI I PAJISJES KLIMATIKE <br /> Gjatë stinës së nxehtë të
              verës, ajri i kondicionuar është një domosdoshmëri në automjetin
              tuaj. Kontrolloni kondicionerin e Peugeot tuaj pranë servisit tonë
              për udhëtime sa më të freskëta. Për rezervime kontaktoni në nr. e
              telefonit 069 600 6060, ose na shkruani në adresen e-mail:
              recepsion@avel.com.al. Ju mirëpresim çdo ditë nga ora 08:00-16:30
              dhe ditën e Shtunë ora 08:00-13:00.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
