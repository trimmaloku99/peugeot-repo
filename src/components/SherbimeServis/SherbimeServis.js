import React, { Component } from "react";
import { Link } from "react-router-dom";
import sherbimservis from "../../images/sherbimservis.jpg";
import sistemfrenimi from "../../images/sistem-frenimi.jpg";
import Servicing from "../Servicing";

export default class SherbimeServis extends Component {
  render() {
    return (
      <section id="sherbimservis">
        <h2 className="nav-title">SERVISI PEUGEOT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              SERVISI PEUGEOT
            </li>
          </ol>
        </div>
        <img id="sherbimservis-img" src={sherbimservis}></img>

        <div id="description-panel">
          <p>
            Kontrollet dhe mirëmbajtja, dy formula unike dhe të personalizuara
            për udhëtim falas. Nga ndihma në rrugë, për të riparuar defektin,
            apo zëvendësimi i pjesëve të konsumuara, zgjidhni nivelin e
            ndërhyrjes në automjetit tuaj dhe përdorni garancinë e cilësisë
            Peugeot si dhe paketat promovuese që ne ofrojmë.
          </p>
        </div>

        <div id="sherbimet">
          <h2>SHËRBIMET TONA</h2>
          <Servicing />
          <div className="card">
            <Link
              to="/sherbime-servis/sistemi-i-frenimit"
              className="d-flex justify-content-center"
            >
              <img
                className="new-card h-100 card-group"
                src={sistemfrenimi}
                alt="Card cap"
                width="50%"
              />
            </Link>
          </div>
        </div>
      </section>
    );
  }
}
