import React, { Component } from "react";
import { Link } from "react-router-dom";
import polen from "../../images/polen-m.jpg";

export default class AntiAlergji extends Component {
  render() {
    return (
      <section id="antialergji">
        <h2 className="nav-title">FITRI I ANTIALERGJISË</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              FITRI I ANTIALERGJISË
            </li>
          </ol>
        </div>

        <div id="antialergji-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={polen}></img>
            <p className="col-lg-6 p-4">
              <h5>FILTËR ANTI-ALERGJIK</h5>
              <br />
              Nëpërmjet filtrit anti-alergjik, eleminoni në mënyrë të plotë
              bakteriet, grimcat e dëmshme dhe ju ofrohet mbrojtje, si dhe
              filtrim me efiçiencë të lartë. Filtri anti-alergjik i Peugeot
              eleminon grimcat e dëmshme deri në 94%.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
