import React, { Component } from "react";
import { Link } from "react-router-dom";
import kiti from "../../images/servicing1.jpg";

export default class KitDistributori extends Component {
  render() {
    return (
      <section id="kitdistributori">
        <h2 className="nav-title">KIT I DISTRIBUTORIT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              KIT I DISTRIBUTORIT
            </li>
          </ol>
        </div>

        <div id="kitdistributori-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={kiti}></img>
            <p className="col-lg-6 p-4">
              <h5>KIT I DISTRIBUTORIT</h5>
              <br />
              <h2>BËNI NJË NDALESË DHE KUJDESUNI PËR AUTOMJETIN TUAJ</h2>
              <br />
              Deri më datë 31 Korrik 2020, servisi i autorizuar Peugeot ju ofron
              riparimin e Kitit të Distributorit me një ulje prej 15% të vlerës
              për: rrip motori, tensionues rripi, rrul parazitar, pompë uji,
              antifrizë, përfshirë dhe orët e punës. Përfitoni nga kjo ofertë
              dhe jepini sigurinë e duhur automjetit tuaj Peugeot. Paraprakisht
              duhet të lini takimin tuaj në numrin e telefonit 0696006060 dhe të
              informoheni rreth masave higjeno sanitare, ose na shkruani në
              adresen e-mail: recepsion@avel.com.al. Ju mirëpresim nga ora
              08:00-16:30. Ju falemiderit!
            </p>
          </div>
        </div>
      </section>
    );
  }
}
