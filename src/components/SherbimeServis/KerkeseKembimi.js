import React, { Component } from "react";
import { Link } from "react-router-dom";
import MyForm from "../MyForm";

export default class KerkeseKembimi extends Component {
  render() {
    return (
      <section id="kerkesekembimi">
        <h2 className="nav-title">KËRKESË PËR SERVIS DHE PJESË KËMBIMI</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              KËRKESË PËR SERVIS DHE PJESË KËMBIMI
            </li>
          </ol>
        </div>

        <div id="kerkesekembimi-details">
          <h5 className="text-center">PLOTËSONI FORMULARIN E MËPOSHTËM:</h5>
          <p className="pt-4 pl-2">
            * TË PLOTËSOHEN TË GJITHA FUSHAT E SHËNUARA ME YLL
          </p>
          <div className="col-lg-12 backu">
            <span>TË DHËNA PERSONALE</span>
          </div>
          <MyForm />
          <p className="p-3" style={{ color: "#999999" }}>
            Mbrojtja e informacionit tuaj personal është përparësia jonë, për më
            shumë informacion ju lutemi lexoni me kujdes Politikat e privatësisë
            dhe mbrojtja e të dhënave personale.
            <br />
            Të dhënat personale të kësaj kërkese janë të destinuara për AVEL
            Sh.p.k., përfaqësues zyrëtar i PEUGEOT në Shqipëri. Të dhënat tuaja
            do të përdoren për të realizuar kërkesën, si dhe për analiza
            statistikore dhe kërkime.
            <br />
            Në varësi të pëlqimit tuaj, të shprehur më lart, ato gjithashtu do
            të përdoren për t’ju dërguar informacion mbi ofertat, lajmet dhe
            njoftimet (gazetat, ftesat dhe publikimet e tjera) nga AVEL Sh.p.k.
          </p>
        </div>
      </section>
    );
  }
}
