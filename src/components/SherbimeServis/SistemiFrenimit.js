import React, { Component } from "react";
import { Link } from "react-router-dom";
import sistemfrenimi from "../../images/sistem-frenimi.jpg";

export default class SistemiFrenimit extends Component {
  render() {
    return (
      <section id="sistemifrenimit">
        <h2 className="nav-title">SISTEMI I FRENIMIT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              SISTEMI I FRENIMIT
            </li>
          </ol>
        </div>

        <div id="sistemifrenimit-details">
          <div className="row">
            <img className="col-lg-6 p-4 h-100" src={sistemfrenimi}></img>
            <p className="col-lg-6 p-4">
              <h5>SISTEMI FRENIMIT</h5>
              <br />
              Sistemi i frenimit është pika më e rëndësishme e sigurisë së
              automjetit. Servisi i autorizuar Peugeot ju ofron të gjitha llojet
              e ferrotave dhe disqeve për një automjet të besueshëm në çdo kohë.
              Jepini sigurinë e duhur automjetit tuaj Peugeot duke zgjedhur
              cilësinë pranë servisit tonë. Për rezervime kontaktoni në numrin e
              telefonit 0696006060, ose na shkruani në adresën e-mail:
              recepsion@avel.com.al. Ju mirëpresim çdo dite nga ora 08:00 deri
              në 16:30 dhe ditën e Shtunë nga ora 08:00 deri në ora 13:00 në
              adresën: Autostrada Tiranë-Durrës Km.5, Rruga Monun Nr.58, Fshati
              Kashar, 1051 Tiranë.
            </p>
          </div>
        </div>
      </section>
    );
  }
}
