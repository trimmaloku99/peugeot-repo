import React, { Component, Suspense } from "react";
import { Link } from "react-router-dom";
// import Carousel from "./Carousel";
import FirstSection from "./FirstSection";
import Servicing from "./Servicing";
import Models from "./Models";
import Map from "./Map";
import Loading from "./Loading";
import "../App.css";
const Carousel = React.lazy(() => import("./Carousel")); // Lazy-loaded

export default class Home extends Component {
  render() {
    return (
      <div>
        <Suspense fallback={<Loading />}>
          <Carousel />
        </Suspense>
        <FirstSection />
        <h2 className="pt-5 mb-0 text-center">SERVISI</h2>
        <Servicing />
        <div id="servicing-btn">
          <Link className="card-btn" to="/sherbime-servis">
            Te gjitha sherbimet e servisit
          </Link>
        </div>
        <Models />
        <Map />
      </div>
    );
  }
}
