import React, { Component } from "react";
import winterdrive from "../../images/winter-drive.jpg";
import { Link } from "react-router-dom";

export default class ActiveDrive extends Component {
  render() {
    return (
      <section id="activedrive">
        <h2 className="nav-title">PEUGEOT WINTER DRIVE</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/oferta">OFERTA</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PEUGEOT WINTER DRIVE
            </li>
          </ol>
        </div>
        <div style={{ maxHeight: "500px" }}>
          <img id="activedrive-img" src={winterdrive}></img>
          <p className="legal-notice">
            * Modelet e paraqitura janë vetëm për qëllime ilustrative. Peugeot
            rezervon të drejtën për të bërë ndryshime.
          </p>
        </div>
        <p className="p-2">
          Modeli i ridizejnuar dhe kompakt PEUGEOT 308 i ka të gjitha cilësitë
          për një eksperiencë të paharrueshme në drejtimin e saj.
        </p>
        <p className="p-2">
          Krijuar sipas standardeve më të fundit, brendësia e automjetit me
          PEUGEOT i-Cockpit® është një tipar i gamës së PEUGEOT 308. Filozofia e
          PEUGEOT i-Cockpit® fokusohet në kontrollin e plotë të drejtimit, duke
          lejuar zotërim më të madh të rrugës. Elementët kryesorë janë timoni
          multifunksional dhe kompakt që ju ofron një gatishmëri më të madhe,
          një panel i cili siguron informacionin kyç të rrugës dhe një ekran me
          prekje 9,7" me ndjeshmërine e nivelit të një smartphone.
        </p>
        <p className="p-2">
          Linjat e mprehta dhe dizenjo e veçantë janë shenjat dalluese të markës
          PEUGEOT. PEUGEOT 308 nuk bën përjashtime, si brenda, ashtu dhe jashtë,
          për çdo detaj. Duke shfaqur dizenjon më të fundit të PEUGEOT, modeli
          308 menjëherë tërheq vëmendjen.
        </p>
        <p className="p-2">
          PEUGEOT 301 shfaq trendet më të fundit të markës. Me një pamje të
          fortë dhe moderne, PEUGEOT 301 është e pajisur gjithashtu me një grilë
          vertikale, fenerë të dritave gjatë ditës LED i, dhe me nje efekti të
          kthetrave të luanit në pjesën e prapme të automjetit, të cilat
          pasqyrojnë përmbajtjen e teknologjisë së lartë.
        </p>
        <p className="p-2">
          Zgjidhni midis një motori benzine, ose motori nafte sipas nevojave
          tuaja dhe shijoni jetën tuaj të përditshme:
        </p>
        <p className="p-2">
          <b>Peugeot 308</b>
          <br />
          Duke filluar nga 170 Euro / muaj
          <br />
          Motor 1.2L Puretech Niveli aktiv i pajisjeve
          <br />
          110 kuaj fuqi
          <br />
          Kambio manuale me 6 raporte
          <br />
          Stop &amp; Start
          <br />6 airbag, etj.
        </p>
        <p className="p-2">
          <b>Peugeot 301</b>
          <br />
          Duke filluar nga 140 Euro / muaj
          <br />
          Motor 1.5L BlueHdi Niveli aktiv i pajisjeve
          <br />
          100 kuaj fuqi
          <br />
          Kambio manual me 6 raporte
          <br />
          Stop &amp; Start
          <br />
          Sensor parkimi, etj.
          <br />
        </p>
        Për më shumë informacion, na vizitoni në ambjentet tona në AVEL Sh.p.k.
        – Tirana, Albania me adrese: Autostrada Tiranë-Durrës Km.5, Rruga Monun
        Nr.58, Fshati Kashar, 1051 Tiranë. Shqipëri.
      </section>
    );
  }
}
