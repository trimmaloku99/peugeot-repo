import React, { Component } from "react";
import { Link } from "react-router-dom";
import activedrive from "../../images/activedrive.jpg";
import winterdrive from "../../images/winterdrive.jpg";

export default class Oferta extends Component {
  render() {
    return (
      <section id="oferta">
        <h2 className="nav-title">OFERTA</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              OFERTA
            </li>
          </ol>
        </div>

        <div id="oferta-details">
          <div className="row">
            <div className="col-lg-6 ">
              <h2 className="text-center">PEUGEOT ACTIVE DRIVE</h2>
              <img className="img-fluid" src={activedrive}></img>
              <Link to="/oferta/peugeot-active-drive" className="more-btn">
                Më shumë
              </Link>
            </div>
            <div className="col-lg-6 ">
              <h2 className="text-center">PEUGEOT WINTER DRIVE</h2>
              <img className="img-fluid" src={winterdrive}></img>
              <Link to="/oferta/peugeot-winter-drive" className="more-btn">
                Më shumë
              </Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
