import React, { Component } from "react";
import activedrive from "../../images/active0drive.jpg";
import { Link } from "react-router-dom";

export default class ActiveDrive extends Component {
  render() {
    return (
      <section id="activedrive">
        <h2 className="nav-title">PEUGEOT ACTIVE DRIVE</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/oferta">OFERTA</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PEUGEOT ACTIVE DRIVE
            </li>
          </ol>
        </div>
        <div style={{ maxHeight: "500px" }}>
          <img id="activedrive-img" src={activedrive}></img>
          <p className="legal-notice">
            * Modelet e paraqitura janë vetëm për qëllime ilustrative. Peugeot
            rezervon të drejtën për të bërë ndryshime.
          </p>
        </div>
        <p className="p-2">
          Peugeot 208 dhe Peugeot 301, me nivel aktiv të pajisjeve, motorë të
          rinj nga gjenerata e fundit dhe një paketë sistemesh sigurie për
          kënaqësi edhe më të madhe të drejtimit.
        </p>
        <p className="p-2">
          Zgjidhni midis një motori benzine, ose motori nafte sipas nevojave
          tuaja dhe shijoni jetën tuaj të përditshme:
        </p>
        <p className="p-2">
          <b>Peugeot 208</b>
          <br />
          1.2 PureTech Niveli aktiv i pajisjeve
          <br />
          Duke filluar nga 115 Euro / muaj
          <br />
        </p>
        <p className="p-2">
          <b>Peugeot 301</b>
          <br />
          1.2 PureTech Niveli aktiv i pajisjeve
          <br />
          Duke filluar nga 105 Euro / muaj
          <br />
        </p>
        <p className="p-2">
          Për më shumë informacion, na vizitoni në ambjentet tona në AVEL
          Sh.p.k. – Tirana, Albania me adrese: Autostrada Tiranë-Durrës Km.5,
          Rruga Monun Nr.58, Fshati Kashar, 1051 Tiranë. Shqipëri.
        </p>
      </section>
    );
  }
}
