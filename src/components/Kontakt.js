import React, { Component } from "react";
import tel from "../images/tel.jpg";
import mail from "../images/mail.jpg";
import posta from "../images/posta.jpg";

export default class Kontakt extends Component {
  render() {
    return (
      <section id="kontakt">
        <h2 className="nav-title">КОNТАКТ</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              КОNТАКТ
            </li>
          </ol>
        </div>

        <div id="kontakt-details">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11745.940228311056!2d21.1376685!3d42.6086678!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbe7eb67e78d9457c!2sPEUGEOT!5e0!3m2!1sen!2s!4v1597410502896!5m2!1sen!2s"
            width="600"
            height="450"
            frameborder="0"
            allowfullscreen=""
            aria-hidden="false"
            tabindex="0"
          ></iframe>
        </div>

        <div id="kontakt-deck">
          <div className="card-deck">
            <div className="card">
              <img className="card-img-top" src={tel} alt="Card cap" />
              <div className="card-body">
                <h5 className="card-title">Adresa</h5>
                <br />
                <p className="card-text">
                  AVEL Sh.p.k. – Tirana, Albania
                  <br /> Autostrada Tiranë-Durrës Km 5, Rruga Monun Nr.58,
                  <br /> Fshati Kashar, 1051 Tiranë, Shqipëri. <br />
                  <br />
                  <strong>Zyra e shitjes</strong>
                  <br /> +355 69 20 81 552 | blend.demneri@avel.com.al <br />
                  +355 69 20 81 554 | erjon.alimerko@avel.com.al
                  <br />
                  <br /> <strong>Recepsioni i servisit </strong>
                  <br /> +355 69 60 06 060 | recepsion@avel.com.al
                  <br />
                  <br /> <strong>Servisi</strong>
                  <br />
                  +355 69 20 81 550 | adriatik.avdia@avel.com.al
                  <br />
                  +355 69 20 81 553 servisimekanik@avel.com.al
                </p>
              </div>
            </div>
            <div className="card">
              <img className="card-img-top" src={posta} alt="Card cap" />
              <div className="card-body">
                <h5 className="card-title">Orari i punës</h5>
                <br />
                <p className="card-text">
                  E Hënë – E Premte 08.00 – 16.30
                  <br /> E Shtunë 08.00-13.00
                  <br />
                  <br /> <b>Servisi:</b>
                  <br />
                  E Hënë – E Premte 08.00 – 16.30 <br /> Pushim 12.00 – 12.30
                  <br /> E Shtunë 08.00 – 13.00 <br /> <br /> <b>Marketing</b>
                  <br /> +355 68 60 62 822 | xhulian.xhumari@avel.com.al
                  <br />
                  <br /> <b>Pjesët e këmbimit</b>
                  <br /> +355 69 40 73 907 | petrit.kovaci@avel.com.al
                  <br /> <br /> <b>Komplimentet dhe ankesat</b>
                  <br /> +355 68 60 62 822| xhulian.xhumari@avel.com.al
                </p>
              </div>
            </div>
            <div className="card">
              <img className="card-img-top" src={mail} alt="Card cap" />
              <div className="card-body w-100 removeWidth">
                <h5 className="card-title">Е-mail</h5>
                <br />
                <p className="card-text">info@avel.com.al</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
