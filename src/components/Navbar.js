import React, { Component } from "react";
import { Link } from "react-router-dom";
import Vetura from "../components/Vetura";
import logo from "../images/logo.png";
import p108 from "../images/p108.png";
import p208 from "../images/208.png";
import p301 from "../images/301.png";
import p308 from "../images/308.png";
import p508 from "../images/508.png";
import rifter from "../images/rifter.png";
import partner from "../images/partner.png";
import traveller from "../images/Traveller.png";
import expert from "../images/expert.png";
import boxer from "../images/Boxer.png";
import p2008 from "../images/2008.png";
import p3008 from "../images/3008.png";
import p5008 from "../images/5008.png";
import $ from "jquery";
window.$ = $;

export default class Navbar extends Component {
  componentDidMount() {
    // MOBILE
    if (window.innerWidth < 992) {
      $("#panel-mobile").removeClass("hide");
      $("#panel-mobile").addClass("show");

      // toggler
      $(".navbar-toggler").click(function () {
        // $("body").toggleClass("hidden");
      });

      $("#go-back").click(function () {
        $("#panel-mobile #container").animate({ right: "-810px" });
        setTimeout(function () {
          $("#panel-mobile #container").removeClass("show");
          $("#panel-mobile #container").addClass("hide");
        }, 400);
      });

      $("#go-back-modelet").click(function () {
        $("#container-models").animate({ right: "-520px" });
        setTimeout(function () {
          $("#container-models").removeClass("show");
          $("#container-models").addClass("hide");
        }, 400);
      });

      $("#go-back-sub").click(function () {
        $("#sub-container").animate({ right: "-520px" });
        setTimeout(function () {
          $("#sub-container").removeClass("show");
          $("#sub-container").addClass("hide");
        }, 400);
      });

      $("#first-li").click(function () {
        $("#ul-text").text("GAMA");
        $("#panel-mobile #container").removeClass("hide");
        $("#panel-mobile #container").addClass("show");
        $("#panel-mobile #container").css("zIndex", "17");
        $("#panel-mobile #container .servis").removeClass("d-flex");
        $("#panel-mobile #container .servis").addClass("hide");
        $("#panel-mobile #container .infos").removeClass("d-flex");
        $("#panel-mobile #container .infos").addClass("hide");
        $("#panel-mobile #container .gama").removeClass("hide");
        $("#panel-mobile #container .gama").addClass("d-flex");
        $("#panel-mobile #container").animate({ right: "0px" });
      });
      // first-li children
      $("#container ul li:nth-child(1)").click(function () {
        $("#container-models").removeClass("hide");
        $("#container-models").addClass("show");
        $("#container-models").css("zIndex", "18");
        $("#container-models .vehicle:nth-child(1)").removeClass("hide");
        $("#container-models .vehicle:nth-child(2)").removeClass("hide");
        $("#container-models .vehicle:nth-child(3)").removeClass("hide");
        $("#container-models .vehicle:nth-child(4)").removeClass("hide");
        $("#container-models .vehicle:nth-child(5)").removeClass("hide");
        $("#container-models .vehicle:nth-child(6)").removeClass("hide");
        $("#container-models .vehicle:nth-child(7)").removeClass("hide");
        $("#container-models .vehicle:nth-child(8)").removeClass("hide");
        $("#container-models .vehicle:nth-child(9)").removeClass("hide");
        $("#container-models .vehicle:nth-child(10)").removeClass("hide");
        $("#container-models .vehicle:nth-child(11)").removeClass("hide");
        $("#container-models .vehicle:nth-child(12)").removeClass("hide");
        $("#container-models .vehicle:nth-child(13)").removeClass("hide");
        $("#container-models .vehicle:nth-child(14)").removeClass("hide");
        $("#container-models .vehicle:nth-child(1)").addClass("show");
        $("#container-models .vehicle:nth-child(2)").addClass("show");
        $("#container-models .vehicle:nth-child(3)").addClass("show");
        $("#container-models .vehicle:nth-child(4)").addClass("show");
        $("#container-models .vehicle:nth-child(5)").addClass("show");
        $("#container-models .vehicle:nth-child(6)").addClass("show");
        $("#container-models .vehicle:nth-child(7)").addClass("show");
        $("#container-models .vehicle:nth-child(8)").addClass("show");
        $("#container-models .vehicle:nth-child(9)").addClass("show");
        $("#container-models .vehicle:nth-child(10)").addClass("show");
        $("#container-models .vehicle:nth-child(11)").addClass("show");
        $("#container-models .vehicle:nth-child(12)").addClass("show");
        $("#container-models .vehicle:nth-child(13)").addClass("show");
        $("#container-models .vehicle:nth-child(14)").addClass("show");
        $("#container-models #ul-text").text("TE GJITHA MODELET PEUGEOT");
        $("#container-models").animate({ right: "0px" });
      });

      $("#container ul li:nth-child(2)").click(function () {
        $("#container-models").removeClass("hide");
        $("#container-models").addClass("show");
        $("#container-models").css("zIndex", "18");
        $("#container-models .vehicle:nth-child(10)").removeClass("show");
        $("#container-models .vehicle:nth-child(11)").removeClass("show");
        $("#container-models .vehicle:nth-child(12)").removeClass("show");
        $("#container-models .vehicle:nth-child(13)").removeClass("show");
        $("#container-models .vehicle:nth-child(14)").removeClass("show");
        $("#container-models .vehicle:nth-child(10)").addClass("hide");
        $("#container-models .vehicle:nth-child(11)").addClass("hide");
        $("#container-models .vehicle:nth-child(12)").addClass("hide");
        $("#container-models .vehicle:nth-child(13)").addClass("hide");
        $("#container-models .vehicle:nth-child(14)").addClass("hide");
        $("#container-models .vehicle:nth-child(1)").addClass("show");
        $("#container-models .vehicle:nth-child(2)").addClass("show");
        $("#container-models .vehicle:nth-child(3)").addClass("show");
        $("#container-models .vehicle:nth-child(4)").addClass("show");
        $("#container-models .vehicle:nth-child(5)").addClass("show");
        $("#container-models .vehicle:nth-child(6)").addClass("show");
        $("#container-models .vehicle:nth-child(7)").addClass("show");
        $("#container-models .vehicle:nth-child(8)").addClass("show");
        $("#container-models .vehicle:nth-child(9)").addClass("show");
        $("#container-models #ul-text").text("GAMA PER PASANGJERE");
        $("#container-models").animate({ right: "0px" });
      });

      $("#container ul li:nth-child(3)").click(function () {
        $("#container-models").removeClass("hide");
        $("#container-models").addClass("show");
        $("#container-models").css("zIndex", "18");
        $("#container-models .vehicle:nth-child(1)").removeClass("show");
        $("#container-models .vehicle:nth-child(2)").removeClass("show");
        $("#container-models .vehicle:nth-child(3)").removeClass("show");
        $("#container-models .vehicle:nth-child(5)").removeClass("show");
        $("#container-models .vehicle:nth-child(6)").removeClass("show");
        $("#container-models .vehicle:nth-child(8)").removeClass("show");
        $("#container-models .vehicle:nth-child(10)").removeClass("show");
        $("#container-models .vehicle:nth-child(11)").removeClass("show");
        $("#container-models .vehicle:nth-child(12)").removeClass("show");
        $("#container-models .vehicle:nth-child(13)").removeClass("show");
        $("#container-models .vehicle:nth-child(14)").removeClass("show");
        $("#container-models .vehicle:nth-child(1)").addClass("hide");
        $("#container-models .vehicle:nth-child(2)").addClass("hide");
        $("#container-models .vehicle:nth-child(3)").addClass("hide");
        $("#container-models .vehicle:nth-child(5)").addClass("hide");
        $("#container-models .vehicle:nth-child(6)").addClass("hide");
        $("#container-models .vehicle:nth-child(8)").addClass("hide");
        $("#container-models .vehicle:nth-child(10)").addClass("hide");
        $("#container-models .vehicle:nth-child(11)").addClass("hide");
        $("#container-models .vehicle:nth-child(12)").addClass("hide");
        $("#container-models .vehicle:nth-child(13)").addClass("hide");
        $("#container-models .vehicle:nth-child(14)").addClass("hide");
        $("#container-models .vehicle:nth-child(4)").addClass("show");
        $("#container-models .vehicle:nth-child(7)").addClass("show");
        $("#container-models .vehicle:nth-child(9)").addClass("show");
        $("#container-models #ul-text").text("GAMA SUV");
        $("#container-models").animate({ right: "0px" });
      });

      $("#container ul li:nth-child(4)").click(function () {
        $("#container-models").removeClass("hide");
        $("#container-models").addClass("show");
        $("#container-models").css("zIndex", "18");
        $("#container-models .vehicle:nth-child(1)").removeClass("show");
        $("#container-models .vehicle:nth-child(2)").removeClass("show");
        $("#container-models .vehicle:nth-child(3)").removeClass("show");
        $("#container-models .vehicle:nth-child(4)").removeClass("show");
        $("#container-models .vehicle:nth-child(5)").removeClass("show");
        $("#container-models .vehicle:nth-child(6)").removeClass("show");
        $("#container-models .vehicle:nth-child(7)").removeClass("show");
        $("#container-models .vehicle:nth-child(8)").removeClass("show");
        $("#container-models .vehicle:nth-child(12)").removeClass("show");
        $("#container-models .vehicle:nth-child(13)").removeClass("show");
        $("#container-models .vehicle:nth-child(14)").removeClass("show");
        $("#container-models .vehicle:nth-child(1)").addClass("hide");
        $("#container-models .vehicle:nth-child(2)").addClass("hide");
        $("#container-models .vehicle:nth-child(3)").addClass("hide");
        $("#container-models .vehicle:nth-child(4)").addClass("hide");
        $("#container-models .vehicle:nth-child(5)").addClass("hide");
        $("#container-models .vehicle:nth-child(6)").addClass("hide");
        $("#container-models .vehicle:nth-child(7)").addClass("hide");
        $("#container-models .vehicle:nth-child(8)").addClass("hide");
        $("#container-models .vehicle:nth-child(12)").addClass("hide");
        $("#container-models .vehicle:nth-child(13)").addClass("hide");
        $("#container-models .vehicle:nth-child(14)").addClass("hide");
        $("#container-models .vehicle:nth-child(9)").addClass("show");
        $("#container-models .vehicle:nth-child(10)").addClass("show");
        $("#container-models .vehicle:nth-child(11)").addClass("show");
        $("#container-models #ul-text").text("GAMA SUV");
        $("#container-models").animate({ right: "0px" });
      });

      $("#container ul li:nth-child(5)").click(function () {
        $("#container-models").removeClass("hide");
        $("#container-models").addClass("show");
        $("#container-models").css("zIndex", "18");
        $("#container-models .vehicle:nth-child(1)").removeClass("show");
        $("#container-models .vehicle:nth-child(2)").removeClass("show");
        $("#container-models .vehicle:nth-child(3)").removeClass("show");
        $("#container-models .vehicle:nth-child(4)").removeClass("show");
        $("#container-models .vehicle:nth-child(5)").removeClass("show");
        $("#container-models .vehicle:nth-child(6)").removeClass("show");
        $("#container-models .vehicle:nth-child(7)").removeClass("show");
        $("#container-models .vehicle:nth-child(8)").removeClass("show");
        $("#container-models .vehicle:nth-child(9)").removeClass("show");
        $("#container-models .vehicle:nth-child(10)").removeClass("show");
        $("#container-models .vehicle:nth-child(11)").removeClass("show");
        $("#container-models .vehicle:nth-child(1)").addClass("hide");
        $("#container-models .vehicle:nth-child(2)").addClass("hide");
        $("#container-models .vehicle:nth-child(3)").addClass("hide");
        $("#container-models .vehicle:nth-child(4)").addClass("hide");
        $("#container-models .vehicle:nth-child(5)").addClass("hide");
        $("#container-models .vehicle:nth-child(6)").addClass("hide");
        $("#container-models .vehicle:nth-child(7)").addClass("hide");
        $("#container-models .vehicle:nth-child(8)").addClass("hide");
        $("#container-models .vehicle:nth-child(9)").addClass("hide");
        $("#container-models .vehicle:nth-child(10)").addClass("hide");
        $("#container-models .vehicle:nth-child(11)").addClass("hide");
        $("#container-models .vehicle:nth-child(12)").addClass("show");
        $("#container-models .vehicle:nth-child(13)").addClass("show");
        $("#container-models .vehicle:nth-child(14)").addClass("show");
        $("#container-models #ul-text").text("AUTOMJETE TREGTARE");
        $("#container-models").animate({ right: "0px" });
      });

      $("#second-li").click(function () {
        $("#ul-text").text("SERVISI PEUGEOT");
        $("#panel-mobile #container").css("zIndex", "17");
        $("#panel-mobile #container").removeClass("hide");
        $("#panel-mobile #container").addClass("show");
        $("#panel-mobile #container .gama").removeClass("d-flex");
        $("#panel-mobile #container .gama").addClass("hide");
        $("#panel-mobile #container .infos").removeClass("d-flex");
        $("#panel-mobile #container .infos").addClass("hide");
        $("#panel-mobile #container .servis").removeClass("hide");
        $("#panel-mobile #container .servis").addClass("d-flex");
        $("#panel-mobile #container").animate({ right: "0px" });
      });

      //servisi peugeot
      $("#container ul li:nth-child(6)").click(function () {
        $("#panel-mobile #sub-container").css("zIndex", "17");
        $("#sub-container").removeClass("hide");
        $("#sub-container").addClass("show");
        $("#panel-mobile #sub-container .servis").removeClass("d-flex");
        $("#panel-mobile #sub-container .servis").addClass("hide");
        $("#panel-mobile #sub-container .promo").removeClass("d-flex");
        $("#panel-mobile #sub-container .promo").addClass("hide");
        $("#panel-mobile #sub-container .sub-infos").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-infos").addClass("hide");
        $("#panel-mobile #sub-container .kontakti").removeClass("d-flex");
        $("#panel-mobile #sub-container .kontakti").addClass("hide");
        $("#panel-mobile #sub-container .sub-servis").removeClass("hide");
        $("#panel-mobile #sub-container .sub-servis").addClass("d-flex");
        $("#panel-mobile #sub-container").animate({ right: "0px" });
      });

      // promocione
      $("#container ul li:nth-child(7)").click(function () {
        $("#sub-ul-text").text("PROMOCIONE");
        $("#panel-mobile #sub-container").css("zIndex", "17");
        $("#sub-container").removeClass("hide");
        $("#sub-container").addClass("show");
        $("#panel-mobile #sub-container .servis").removeClass("d-flex");
        $("#panel-mobile #sub-container .servis").addClass("hide");
        $("#panel-mobile #sub-container .sub-servis").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-servis").addClass("hide");
        $("#panel-mobile #sub-container .sub-infos").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-infos").addClass("hide");
        $("#panel-mobile #sub-container .kontakti").removeClass("d-flex");
        $("#panel-mobile #sub-container .kontakti").addClass("hide");
        $("#panel-mobile #sub-container .promo").removeClass("hide");
        $("#panel-mobile #sub-container .promo").addClass("d-flex");
        $("#panel-mobile #sub-container").animate({ right: "0px" });
      });

      $("#third-li").click(function () {
        $("#ul-text").text("INFORMACIONE");
        $("#panel-mobile #container").css("zIndex", "17");
        $("#panel-mobile #container").removeClass("hide");
        $("#panel-mobile #container").addClass("show");
        $("#panel-mobile #container .gama").removeClass("d-flex");
        $("#panel-mobile #container .gama").addClass("hide");
        $("#panel-mobile #container .promo").removeClass("d-flex");
        $("#panel-mobile #container .promo").addClass("hide");
        $("#panel-mobile #container .servis").removeClass("d-flex");
        $("#panel-mobile #container .servis").addClass("hide");
        $("#panel-mobile #container .infos").removeClass("hide");
        $("#panel-mobile #container .infos").addClass("d-flex");
        $("#panel-mobile #container").animate({ right: "11px" });
      });

      // informacione
      $("#container ul li:nth-child(8)").click(function () {
        $("#sub-ul-text").text("INFORMACIONE");
        $("#panel-mobile #sub-container").css("zIndex", "17");
        $("#sub-container").removeClass("hide");
        $("#sub-container").addClass("show");
        $("#panel-mobile #sub-container .gama").removeClass("d-flex");
        $("#panel-mobile #sub-container .gama").addClass("hide");
        $("#panel-mobile #sub-container .promo").removeClass("d-flex");
        $("#panel-mobile #sub-container .promo").addClass("hide");
        $("#panel-mobile #sub-container .sub-servis").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-servis").addClass("hide");
        $("#panel-mobile #sub-container .kontakti").removeClass("d-flex");
        $("#panel-mobile #sub-container .kontakti").addClass("hide");
        $("#panel-mobile #sub-container .sub-infos").removeClass("hide");
        $("#panel-mobile #sub-container .sub-infos").addClass("d-flex");
        $("#panel-mobile #sub-container").animate({ right: "0px" });
      });

      // kontakt
      $("#container ul li:nth-child(9)").click(function () {
        $("#sub-ul-text").text("KONTAKT");
        $("#panel-mobile #sub-container").css("zIndex", "17");
        $("#sub-container").removeClass("hide");
        $("#sub-container").addClass("show");
        $("#panel-mobile #sub-container .gama").removeClass("d-flex");
        $("#panel-mobile #sub-container .gama").addClass("hide");
        $("#panel-mobile #sub-container .promo").removeClass("d-flex");
        $("#panel-mobile #sub-container .promo").addClass("hide");
        $("#panel-mobile #sub-container .sub-servis").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-servis").addClass("hide");
        $("#panel-mobile #sub-container .sub-infos").removeClass("d-flex");
        $("#panel-mobile #sub-container .sub-infos").addClass("hide");
        $("#panel-mobile #sub-container .kontakti").removeClass("hide");
        $("#panel-mobile #sub-container .kontakti").addClass("d-flex");
        $("#panel-mobile #sub-container").animate({ right: "0px" });
      });
    }

    // DESKTOP
    if (window.innerWidth > 991) {
      $("#first-li").click(function () {
        $(".overlay-menu").removeClass("hide");
        $(".overlay-menu").addClass("show");
        $(".vehicle-block").css("display", "flex");
        $("#panel .row").css("height", "550px");
        $("#lefti").css("backgroundColor", "#fff");
        $("#all-models").removeClass("hide");
        $("#all-models").addClass("show");
        // je mbyllun,ather hapu
        if ($("#panel").hasClass("hide")) {
          $("#mbylle").toggleClass("show");
          $("#gama-submenu2").addClass("hide");
          $("#gama-submenu2").removeClass("show");
          $("#gama-submenu3").addClass("hide");
          $("#gama-submenu3").removeClass("show");
          $("#gama-submenu1").removeClass("hide");
          $("#gama-submenu1").addClass("show");
          $("#panel").slideToggle("slow");
          $("#panel").removeClass("hide");
          $("#panel").addClass("show");
        }
        $("#gama-submenu1 li:nth-child(1)").click(function () {
          $(".vehicle-block a:nth-child(1)").removeClass("hide");
          $(".vehicle-block a:nth-child(2)").removeClass("hide");
          $(".vehicle-block a:nth-child(3)").removeClass("hide");
          $(".vehicle-block a:nth-child(4)").removeClass("hide");
          $(".vehicle-block a:nth-child(5)").removeClass("hide");
          $(".vehicle-block a:nth-child(6)").removeClass("hide");
          $(".vehicle-block a:nth-child(7)").removeClass("hide");
          $(".vehicle-block a:nth-child(8)").removeClass("hide");
          $(".vehicle-block a:nth-child(9)").removeClass("hide");
          $(".vehicle-block a:nth-child(10)").removeClass("hide");
          $(".vehicle-block a:nth-child(11)").removeClass("hide");
          $(".vehicle-block a:nth-child(12)").removeClass("hide");
          $(".vehicle-block a:nth-child(13)").removeClass("hide");
          $(".vehicle-block a:nth-child(1)").addClass("show");
          $(".vehicle-block a:nth-child(2)").addClass("show");
          $(".vehicle-block a:nth-child(3)").addClass("show");
          $(".vehicle-block a:nth-child(4)").addClass("show");
          $(".vehicle-block a:nth-child(5)").addClass("show");
          $(".vehicle-block a:nth-child(6)").addClass("show");
          $(".vehicle-block a:nth-child(7)").addClass("show");
          $(".vehicle-block a:nth-child(8)").addClass("show");
          $(".vehicle-block a:nth-child(9)").addClass("show");
          $(".vehicle-block a:nth-child(10)").addClass("show");
          $(".vehicle-block a:nth-child(11)").addClass("show");
          $(".vehicle-block a:nth-child(12)").addClass("show");
          $(".vehicle-block a:nth-child(13)").addClass("show");
        });

        $("#gama-submenu1 li:nth-child(2)").click(function () {
          $(".vehicle-block").css("justify-content", "end");
          $(".vehicle-block a:nth-child(9)").removeClass("show");
          $(".vehicle-block a:nth-child(10)").removeClass("show");
          $(".vehicle-block a:nth-child(11)").removeClass("show");
          $(".vehicle-block a:nth-child(12)").removeClass("show");
          $(".vehicle-block a:nth-child(13)").removeClass("show");
          $(".vehicle-block a:nth-child(9)").addClass("hide");
          $(".vehicle-block a:nth-child(10)").addClass("hide");
          $(".vehicle-block a:nth-child(11)").addClass("hide");
          $(".vehicle-block a:nth-child(12)").addClass("hide");
          $(".vehicle-block a:nth-child(13)").addClass("hide");

          $(".vehicle-block a:nth-child(1)").addClass("show");
          $(".vehicle-block a:nth-child(2)").addClass("show");
          $(".vehicle-block a:nth-child(3)").addClass("show");
          $(".vehicle-block a:nth-child(4)").addClass("show");
          $(".vehicle-block a:nth-child(5)").addClass("show");
          $(".vehicle-block a:nth-child(6)").addClass("show");
          $(".vehicle-block a:nth-child(7)").addClass("show");
          $(".vehicle-block a:nth-child(8)").addClass("show");
        });

        // 3,6,8 kerre
        $("#gama-submenu1 li:nth-child(3)").click(function () {
          $(".vehicle-block").css("justify-content", "end");
          $(".vehicle-block a:nth-child(1)").removeClass("show");
          $(".vehicle-block a:nth-child(2)").removeClass("show");
          $(".vehicle-block a:nth-child(4)").removeClass("show");
          $(".vehicle-block a:nth-child(5)").removeClass("show");
          $(".vehicle-block a:nth-child(7)").removeClass("show");
          $(".vehicle-block a:nth-child(9)").removeClass("show");
          $(".vehicle-block a:nth-child(10)").removeClass("show");
          $(".vehicle-block a:nth-child(11)").removeClass("show");
          $(".vehicle-block a:nth-child(12)").removeClass("show");
          $(".vehicle-block a:nth-child(13)").removeClass("show");
          $(".vehicle-block a:nth-child(1)").addClass("hide");
          $(".vehicle-block a:nth-child(2)").addClass("hide");
          $(".vehicle-block a:nth-child(4)").addClass("hide");
          $(".vehicle-block a:nth-child(5)").addClass("hide");
          $(".vehicle-block a:nth-child(7)").addClass("hide");
          $(".vehicle-block a:nth-child(9)").addClass("hide");
          $(".vehicle-block a:nth-child(10)").addClass("hide");
          $(".vehicle-block a:nth-child(11)").addClass("hide");
          $(".vehicle-block a:nth-child(12)").addClass("hide");
          $(".vehicle-block a:nth-child(13)").addClass("hide");
          $(".vehicle-block a:nth-child(3)").addClass("show");
          $(".vehicle-block a:nth-child(6)").addClass("show");
          $(".vehicle-block a:nth-child(8)").addClass("show");
        });

        // 8,9,10
        $("#gama-submenu1 li:nth-child(4)").click(function () {
          $(".vehicle-block").css("justify-content", "end");
          $(".vehicle-block a:nth-child(1)").removeClass("show");
          $(".vehicle-block a:nth-child(2)").removeClass("show");
          $(".vehicle-block a:nth-child(3)").removeClass("show");
          $(".vehicle-block a:nth-child(4)").removeClass("show");
          $(".vehicle-block a:nth-child(5)").removeClass("show");
          $(".vehicle-block a:nth-child(6)").removeClass("show");
          $(".vehicle-block a:nth-child(7)").removeClass("show");
          $(".vehicle-block a:nth-child(11)").removeClass("show");
          $(".vehicle-block a:nth-child(12)").removeClass("show");
          $(".vehicle-block a:nth-child(13)").removeClass("show");
          $(".vehicle-block a:nth-child(1)").addClass("hide");
          $(".vehicle-block a:nth-child(2)").addClass("hide");
          $(".vehicle-block a:nth-child(3)").addClass("hide");
          $(".vehicle-block a:nth-child(4)").addClass("hide");
          $(".vehicle-block a:nth-child(5)").addClass("hide");
          $(".vehicle-block a:nth-child(6)").addClass("hide");
          $(".vehicle-block a:nth-child(7)").addClass("hide");
          $(".vehicle-block a:nth-child(11)").addClass("hide");
          $(".vehicle-block a:nth-child(12)").addClass("hide");
          $(".vehicle-block a:nth-child(13)").addClass("hide");
          $(".vehicle-block a:nth-child(8)").addClass("show");
          $(".vehicle-block a:nth-child(9)").addClass("show");
          $(".vehicle-block a:nth-child(10)").addClass("show");
        });

        // 11,12,13
        $("#gama-submenu1 li:nth-child(5)").click(function () {
          $(".vehicle-block").css("justify-content", "end");
          $(".vehicle-block a:nth-child(1)").removeClass("show");
          $(".vehicle-block a:nth-child(2)").removeClass("show");
          $(".vehicle-block a:nth-child(3)").removeClass("show");
          $(".vehicle-block a:nth-child(4)").removeClass("show");
          $(".vehicle-block a:nth-child(5)").removeClass("show");
          $(".vehicle-block a:nth-child(6)").removeClass("show");
          $(".vehicle-block a:nth-child(7)").removeClass("show");
          $(".vehicle-block a:nth-child(8)").removeClass("show");
          $(".vehicle-block a:nth-child(9)").removeClass("show");
          $(".vehicle-block a:nth-child(10)").removeClass("show");
          $(".vehicle-block a:nth-child(1)").addClass("hide");
          $(".vehicle-block a:nth-child(2)").addClass("hide");
          $(".vehicle-block a:nth-child(3)").addClass("hide");
          $(".vehicle-block a:nth-child(4)").addClass("hide");
          $(".vehicle-block a:nth-child(5)").addClass("hide");
          $(".vehicle-block a:nth-child(6)").addClass("hide");
          $(".vehicle-block a:nth-child(7)").addClass("hide");
          $(".vehicle-block a:nth-child(8)").addClass("hide");
          $(".vehicle-block a:nth-child(9)").addClass("hide");
          $(".vehicle-block a:nth-child(10)").addClass("hide");
          $(".vehicle-block a:nth-child(11)").addClass("show");
          $(".vehicle-block a:nth-child(12)").addClass("show");
          $(".vehicle-block a:nth-child(13)").addClass("show");
        });

        // first is already opened
        if ($("#panel").hasClass("show")) {
          $("#second-li").click(function () {
            // hide first
            $("#gama-submenu1").removeClass("show");
            $("#gama-submenu1").addClass("hide");
            // hide third
            $("#gama-submenu3").removeClass("show");
            $("#gama-submenu3").addClass("hide");
            // show 2nd
            $("#gama-submenu2").removeClass("hide");
            $("#gama-submenu2").addClass("show");
          });

          $("#third-li").click(function () {
            // hide first
            $("#gama-submenu1").removeClass("show");
            $("#gama-submenu1").addClass("hide");
            // hide second
            $("#gama-submenu2").removeClass("show");
            $("#gama-submenu2").addClass("hide");
            // show 3rd
            $("#gama-submenu3").removeClass("hide");
            $("#gama-submenu3").addClass("show");
          });
        }
      });

      $("#second-li").click(function () {
        $(".overlay-menu").removeClass("hide");
        $(".overlay-menu").addClass("show");
        $("#panel .row").css("height", "100%");
        $("#all-models").removeClass("show");
        $("#all-models").addClass("hide");
        $(".vehicle-block").css("display", "none");
        $("#lefti").css("backgroundColor", "#ececee");
        if ($("#panel").hasClass("hide")) {
          $("#mbylle").toggleClass("show");
          $("#gama-submenu1").removeClass("show");
          $("#gama-submenu1").addClass("hide");
          $("#gama-submenu3").removeClass("show");
          $("#gama-submenu3").addClass("hide");
          $("#gama-submenu2").removeClass("hide");
          $("#gama-submenu2").addClass("show");
          $("#panel").slideToggle("slow");
          $("#panel").removeClass("hide");
          $("#panel").addClass("show");
        }
        // second already opened
        if ($("#panel").hasClass("show")) {
          $("#first-li").click(function () {
            // hide second
            $("#gama-submenu2").removeClass("show");
            $("#gama-submenu2").addClass("hide");
            // hide third
            $("#gama-submenu3").removeClass("show");
            $("#gama-submenu3").addClass("hide");
            // show 1st
            $("#gama-submenu1").removeClass("hide");
            $("#gama-submenu1").addClass("show");
          });

          $("#third-li").click(function () {
            // hide second
            $("#gama-submenu2").removeClass("show");
            $("#gama-submenu2").addClass("hide");
            // hide 1st
            $("#gama-submenu1").removeClass("show");
            $("#gama-submenu1").addClass("hide");
            // show 3rd
            $("#gama-submenu3").removeClass("hide");
            $("#gama-submenu3").addClass("show");
          });
        }
      });

      $("#third-li").click(function () {
        $(".overlay-menu").removeClass("hide");
        $(".overlay-menu").addClass("show");
        $("#all-models").removeClass("show");
        $("#all-models").addClass("hide");
        $(".vehicle-block").css("display", "none");
        $("#lefti").css("backgroundColor", "#ececee");
        $("#gama-submenu3 .row").attr("style", "width:700px !important");
        $("#gama-submenu3 .row").css("z-index", "1");
        // $("#righti").css("z-index", "-1");
        if ($("#panel").hasClass("hide")) {
          $("#mbylle").toggleClass("show");
          $("#gama-submenu1").removeClass("show");
          $("#gama-submenu1").addClass("hide");
          $("#gama-submenu2").removeClass("show");
          $("#gama-submenu2").addClass("hide");
          $("#gama-submenu3").removeClass("hide");
          $("#gama-submenu3").addClass("show");
          $("#panel").slideToggle("slow");
          $("#panel").removeClass("hide");
          $("#panel").addClass("show");
        }
        // third already opened
        if ($("#panel").hasClass("show")) {
          $("#second-li").click(function () {
            // hide third
            $("#gama-submenu3").removeClass("show");
            $("#gama-submenu3").addClass("hide");
            // hide first
            $("#gama-submenu1").removeClass("show");
            $("#gama-submenu1").addClass("hide");
            // show 2nd
            $("#gama-submenu2").removeClass("hide");
            $("#gama-submenu2").addClass("show");
          });

          $("#second-li").click(function () {
            // hide third
            $("#gama-submenu3").removeClass("show");
            $("#gama-submenu3").addClass("hide");
            // hide 1st
            $("#gama-submenu1").removeClass("show");
            $("#gama-submenu1").addClass("hide");
            // show 2nd
            $("#gama-submenu2").removeClass("hide");
            $("#gama-submenu2").addClass("show");
          });
        }
      });

      $("#mbylle").click(function () {
        $(".overlay-menu").removeClass("show");
        $(".overlay-menu").addClass("hide");
        $("#panel").slideToggle("slow");
        $("#mbylle").toggleClass("show");
        setTimeout(function () {
          $("#panel").removeClass("show");
          $("#panel").addClass("hide");
        }, 550);
      });
      // mrena tbardhes
      $(function () {
        $("#lefti ul li").click(function () {
          // remove classes from all
          $("#lefti ul li").removeClass("aktiv");
          // add class to the one we clicked
          $(this).addClass("aktiv");
        });
      });
    }
  }

  render() {
    function mbyllPanelin() {
      $("#panel").slideToggle("slow");
      $("#mbylle").toggleClass("show");
      setTimeout(function () {
        $("#panel").removeClass("show");
        $("#panel").addClass("hide");
        $(".overlay-menu").removeClass("show");
        $(".overlay-menu").addClass("hide");
      }, 550);
    }

    function kollapso() {
      if ($("#navbarTogglerDemo01").hasClass("show")) {
        $(".navbar-toggler").click();
      }
    }

    return (
      <div className="d-flex">
        <nav className="navbar navbar-expand-lg navbar-dark">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
            <span className="menu-mobile-text">MENU</span>
          </button>
          <Link
            onClick={(mbyllPanelin, kollapso)}
            className="navbar-brand"
            to="/"
          >
            <img src={logo} alt="card" />
          </Link>

          <div
            className="collapse navbar-collapse pt-2"
            id="navbarTogglerDemo01"
          >
            <div id="panel-mobile" className="hide">
              <div id="container" className="">
                <div className="get-back">
                  <span className="pl-4" id="go-back">
                    <i className="fa fa-angle-left customsize"></i>
                  </span>
                  <span id="ul-text" className="pl-4">
                    GAMA
                  </span>
                </div>
                <div className="row">
                  <ul className="navbar-nav mr-auto pt-4 pl-3 mt-lg-0 sizing">
                    <li className="nav-item gama pl-5">
                      <a className="nav-link ">TE GJITHA MODELET PEUGEOT</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item gama pl-5">
                      <a className="nav-link ">GAMA PËR PASAGJERË</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item gama pl-5">
                      <a className="nav-link ">GAMA SUV</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item gama pl-5">
                      <a className="nav-link ">
                        AUTOMJETE ME SHUMË VENDE (7,8,9)
                      </a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item gama pl-5">
                      <a className="nav-link">AUTOMJETE TREGTARE</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item servis hide pl-5">
                      <a className="nav-link">SERVISI PEUGEOT</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item servis hide pl-5">
                      <a className="nav-link">PROMOCIONE</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item infos hide pl-5">
                      <a className="nav-link">INFORMACIONE</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                    <li className="nav-item infos hide pl-5">
                      <a className="nav-link">KONTAKT</a>
                      <span className="">
                        <i className="fas fa-angle-right"></i>
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="sub-container" className="">
                <div className="get-back">
                  <span className="pl-4" id="go-back-sub">
                    <i className="fa fa-angle-left customsize"></i>
                  </span>
                  <span id="sub-ul-text" className="pl-4">
                    SERVISI PEUGEOT
                  </span>
                </div>
                <div className="row">
                  <ul className="navbar-nav mr-auto pt-4 pl-3 mt-lg-0 sizing">
                    <Link to="/sherbime-servis" onClick={kollapso}>
                      <li className="nav-item sub-servis hide pl-5">
                        <a className="nav-link">INTERVALI SHERBIMIT</a>
                      </li>
                    </Link>
                    <Link to="/vendndodhja" onClick={kollapso}>
                      <li className="nav-item sub-servis hide pl-5">
                        <a className="nav-link">VENDNDODHJA</a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/pjese">
                      <li className="nav-item sub-servis hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KERKESE PER SERVIS DHE PJESE KEMBIMI
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/sistemi-i-frenimit">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          SISTEMI I FRENIMIT
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/kontrolli-i-kondicionerit">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KONTROLLI I KONDICIONERIT
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/filtri-i-antialergjise">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          FILTRI I ANTIALERGJISE
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/kit-distributori">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KIT I DISTRIBUTORIT
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/kontrolli-i-baterise">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KONTROLL I BATERISE
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/paketa-e-xhamave">
                      <li className="nav-item promo hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          PAKETA E XHAMAVE
                        </a>
                      </li>
                    </Link>
                    <Link to="/rreth-nesh">
                      <li className="nav-item sub-infos hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          RRETH NESH
                        </a>
                      </li>
                    </Link>
                    <Link to="/vendndodhja">
                      <li className="nav-item sub-infos hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          RRJETI PEUGEOT
                        </a>
                      </li>
                    </Link>
                    <Link to="/politikat">
                      <li className="nav-item sub-infos hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE
                          PERSONALE
                        </a>
                      </li>
                    </Link>
                    <Link to="/kontakt">
                      <li className="nav-item kontakti hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KONTAKT
                        </a>
                      </li>
                    </Link>
                    <Link to="/automjete/oferte-automjete">
                      <li className="nav-item kontakti hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KËRKONI OFERTË PËR AUTOMJET
                        </a>
                      </li>
                    </Link>
                    <Link to="/automjete/test-drive">
                      <li className="nav-item kontakti hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          TEST DRIVE
                        </a>
                      </li>
                    </Link>
                    <Link to="/sherbime-servis/pjese">
                      <li className="nav-item kontakti hide pl-5">
                        <a className="nav-link" onClick={kollapso}>
                          KËRKESË PËR SERVIS DHE PJESË KËMBIMI
                        </a>
                      </li>
                    </Link>
                  </ul>
                </div>
              </div>
              <div id="container-models" className="">
                <div className="get-back">
                  <span className="pl-4" id="go-back-modelet">
                    <i className="fa fa-angle-left customsize"></i>
                  </span>
                  <span id="ul-text" className="pl-4">
                    GAMA
                  </span>
                </div>
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-108"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p108}
                  modelName={"108"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-208"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p208}
                  modelName={"208"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-2008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p2008}
                  modelName={"SUV 2008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-301"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p301}
                  modelName={"301"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-308"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p308}
                  modelName={"308"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-3008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p3008}
                  modelName={"SUV 3008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-508"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p508}
                  modelName={"508 E RE"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-5008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p5008}
                  modelName={"SUV 5008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-rifter"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={rifter}
                  modelName={"Rifter"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-traveller"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={traveller}
                  modelName={"Traveller"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-partner"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={partner}
                  modelName={"Partner"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-expert"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={expert}
                  modelName={"Expert"}
                  height={"60px"}
                />
                <Vetura
                  kliku={kollapso}
                  hrefi={"/automjete/peugeot-boxer"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={boxer}
                  modelName={"Boxer"}
                  height={"60px"}
                />
              </div>
            </div>
            <ul className="navbar-nav mr-auto pt-4 pl-3 mt-lg-0 sizing">
              <li
                id="faqja-kryesore"
                className="nav-item pl-3"
                onClick={kollapso}
              >
                <Link className="nav-link" to="/">
                  <i className="fas fa-home"></i> FAQJA KRYESORE
                </Link>
              </li>
              <li id="first-li" className="nav-item pl-3 active slidable">
                <a className="nav-link">
                  GAMA <span className="sr-only">(current)</span>
                </a>
                <span className="pr-2">
                  <i className="fas fa-angle-right"></i>
                </span>
              </li>
              <li id="second-li" className="nav-item pl-3 slidable">
                <a className="nav-link">SERVISI PEUGEOT</a>
                <span className="pr-2">
                  <i className="fas fa-angle-right"></i>
                </span>
              </li>
              <li
                className="nav-item pl-3"
                onClick={() => {
                  mbyllPanelin();
                  kollapso();
                }}
              >
                <Link className="nav-link" to="/vendndodhja">
                  VENDNDODHJA
                </Link>
              </li>
              <li
                className="nav-item pl-3"
                onClick={() => {
                  mbyllPanelin();
                  kollapso();
                }}
              >
                <Link className="nav-link" to="/financimi">
                  FINANCIMI
                </Link>
              </li>
              <li id="third-li" className="nav-item pl-3 slidable">
                <a className="nav-link">INFORMACIONE</a>
                <span className="pr-2" style={{ color: "#2b547e" }}>
                  <i className="fas fa-angle-right"></i>
                </span>
              </li>
              <li
                className="nav-item pl-3"
                onClick={() => {
                  mbyllPanelin();
                  kollapso();
                }}
              >
                <Link
                  className="nav-link"
                  to="/automjete/automjete-te-perdorura"
                >
                  AUTOMJETE TË PËRDORURA
                </Link>
              </li>
            </ul>
          </div>
          <div className="right-header pt-1">
            <i className="fas fa-car"></i>
            <span className="pr-2">
              <Link to="/automjete/oferte-automjete">
                Kerkoni <br />
                <strong>oferte per automjete</strong>
              </Link>
            </span>
            <i className="fas fa-wheelchair"></i>
            <span className="pr-2">
              <Link to="/automjete/test-drive">
                Kerkoni <br />
                <strong>test drive</strong>
              </Link>
            </span>
            <i className="fas fa-map-marker-alt"></i>
            <span className="pr-2">
              <Link to="/vendndodhja">
                Rrjeti <br />
                <strong>peugeot</strong>
              </Link>
            </span>
          </div>
        </nav>

        <div id="panel" className="hide">
          <div className="row">
            <div id="lefti" className="col-4">
              <ul id="gama-submenu1" className="hide">
                <li className="aktiv">
                  Te gjitha modelet peugeot
                  <span>
                    <i className="fas fa-angle-right"></i>
                  </span>
                </li>
                <li>
                  Gama per pasangjere
                  <span>
                    <i className="fas fa-angle-right"></i>
                  </span>
                </li>
                <li>
                  Gama suv
                  <span>
                    <i className="fas fa-angle-right"></i>
                  </span>
                </li>
                <li>
                  Automjete me shum vende (7,8,9)
                  <span>
                    <i className="fas fa-angle-right"></i>
                  </span>
                </li>
                <li>
                  Automjete tregtare
                  <span>
                    <i className="fas fa-angle-right"></i>
                  </span>
                </li>
              </ul>
              <ul id="gama-submenu2" className="hide">
                <h4
                  style={{
                    color: "#0062c5",
                    fontWeight: "normal",
                    padding: "30px 0 0px 16px",
                  }}
                >
                  <Link
                    onClick={mbyllPanelin}
                    to="/sherbime-servis"
                    className="text-left"
                  >
                    SERVISI
                  </Link>
                </h4>
                <div className="row w-100 align-items-baseline">
                  <div className="col-md-6">
                    <p className="pt-2 mb-0">
                      <strong>SERVISI PEUGEOT</strong>
                    </p>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/intervali-sherbimit"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Intervali i shërbimit
                      </Link>
                    </li>
                    <li>
                      <Link onClick={mbyllPanelin} to="/vendndodhja">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Vendndodhja
                      </Link>
                    </li>
                    <li>
                      <Link onClick={mbyllPanelin} to="/sherbime-servis/pjese">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Kërkesë për servis dhe pjesë këmbimi
                      </Link>
                    </li>
                  </div>

                  <div className="col-md-6">
                    <p className="mb-0">
                      <strong>PROMOCIONE</strong>
                    </p>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/sistemi-i-frenimit"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Sistemi i frenimit
                      </Link>
                    </li>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/kontrolli-i-kondicionerit"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Kontrolli i kondicionerit
                      </Link>
                    </li>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/filtri-i-antialergjise"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Filtri i antialergjisë
                      </Link>
                    </li>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/kit-distributori"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Kit i distributorit
                      </Link>
                    </li>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/kontrolli-i-baterise"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Kontroll i baterisë
                      </Link>
                    </li>
                    <li>
                      <Link
                        onClick={mbyllPanelin}
                        to="/sherbime-servis/paketa-e-xhamave"
                      >
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Paketa e xhamave
                      </Link>
                    </li>
                  </div>
                </div>
              </ul>
              <ul id="gama-submenu3" className="hide">
                <Link
                  onClick={mbyllPanelin}
                  className="pt-2 mb-0"
                  to="/kontakt"
                >
                  <h4
                    style={{
                      color: "#0062c5",
                      fontWeight: "normal",
                      padding: "30px 0 0px 16px",
                    }}
                  >
                    INFORMACIONE
                  </h4>
                </Link>
                <div className="row w-100 align-items-baseline">
                  <div className="col-md-4">
                    <strong>INFORMACIONE</strong>

                    <li>
                      <Link onClick={mbyllPanelin} to="/rreth-nesh">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        RRETH NESH
                      </Link>
                    </li>
                    <li>
                      <Link onClick={mbyllPanelin} to="/vendndodhja">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        VENDNDODHJA
                      </Link>
                    </li>
                    <li>
                      <Link onClick={mbyllPanelin} to="/kontakt">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        KONTAKT
                      </Link>
                    </li>
                    <li>
                      <Link onClick={mbyllPanelin} to="/politikat">
                        <span
                          className="pr-2"
                          style={{ color: "#0062c5", fontWeight: "bold" }}
                        >
                          <i className="fas fa-angle-right"></i>
                        </span>
                        Politikat e privatesise dhe mbrojtja e te dhenave
                        personale
                      </Link>
                    </li>
                  </div>

                  <div className="col-md-4">
                    <Link
                      onClick={mbyllPanelin}
                      className="mb-0"
                      to="/vendndodhja"
                    >
                      <strong>RRJETI PEUGEOT</strong>
                    </Link>
                  </div>

                  <div id="info-last" className="col-md-4">
                    <Link
                      onClick={mbyllPanelin}
                      className="mb-0"
                      to="/automjete/oferte-automjete"
                    >
                      <strong>KERKONI OFERTE PER AUTOMJET</strong>
                    </Link>
                    <br />
                    <Link
                      onClick={mbyllPanelin}
                      className="mb-0"
                      to="/automjete/test-drive"
                    >
                      <strong>TEST DRIVE</strong>
                    </Link>
                    <br />
                    <Link
                      onClick={mbyllPanelin}
                      className="mb-0"
                      to="/sherbime-servis/pjese"
                    >
                      <strong>KERKESE PER SERVIS DHE PJESE KEMBIMI</strong>
                    </Link>
                  </div>
                </div>
              </ul>
            </div>
            <div id="righti" className="col-8">
              <div className="vehicle-block col-md-12">
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-108"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p108}
                  modelName={"108"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-208"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p208}
                  modelName={"208"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-2008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p2008}
                  modelName={"SUV 2008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-301"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p301}
                  modelName={"301"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-308"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p308}
                  modelName={"308"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-3008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p3008}
                  modelName={"SUV 3008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-508"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p508}
                  modelName={"508 E RE"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-5008"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={p5008}
                  modelName={"SUV 5008"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-rifter"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={rifter}
                  modelName={"Rifter"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-traveller"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={traveller}
                  modelName={"Traveller"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-partner"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={partner}
                  modelName={"Partner"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-expert"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={expert}
                  modelName={"Expert"}
                  height={"60px"}
                />
                <Vetura
                  kliku={mbyllPanelin}
                  hrefi={"/automjete/peugeot-boxer"}
                  classname={"vehicle col-md-4 col-xl-2"}
                  image={boxer}
                  modelName={"Boxer"}
                  height={"60px"}
                />
              </div>
              <Link
                onClick={mbyllPanelin}
                className="p-3"
                id="all-models"
                style={{
                  position: "absolute",
                  bottom: "0px",
                  right: 0,
                  cursor: "pointer",
                  color: "#0062c5",
                }}
                to="/automjete"
              >
                TE GJITHA MODELET{" "}
                <span
                  className="pr-2"
                  style={{ color: "#0062c5", fontWeight: "bold" }}
                >
                  <i className="fas fa-angle-right"></i>
                </span>
              </Link>
            </div>
          </div>
          <button className="hide" id="mbylle">
            <strong>X MBYLLE</strong>
          </button>
        </div>
        <div className="overlay-menu"></div>
      </div>
    );
  }
}
