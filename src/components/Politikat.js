import React, { Component } from "react";
import politikat from "../images/politikat.jpg";

export default class RrethNesh extends Component {
  render() {
    return (
      <section id="politikat">
        <h2 className="nav-title">
          POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE PERSONALE
        </h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE PERSONALE
            </li>
          </ol>
        </div>
        <img id="politikat-img" src={politikat}></img>

        <div>
          <h2 className="p-5 text-center">
            POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE PERSONALE
          </h2>
          <p className="p-3">
            Kjo politikë mbi të Dhënat Personale rregullon mënyrën me të cilën
            Shoqeria AVEL Sh.p.k. me adresë Autostrada Tiranë-Durrës Km 5, Rruga
            Monun Nr.58, Fshati Kashar, 1051 Tiranë, Shqipëri, mbledh, përdor
            dhe mban të dhënat e mbledhura gjatë vizitës në faqet tona të
            internetit. Kjo politikë e privatësisë zbatohet vetëm në faqet e
            internetit (në pronësi të AVEL Sh.p.k.) dhe nuk zbatohet për të
            dhënat personale të mbledhura në mënyra të tjera. Kjo politikë nuk
            zbatohet nëse faqet tona të internetit vizitohen nëpërmjet faqeve të
            tjera. <br />
            <br /> Gjatë vizitës tuaj në internet, privatësia juaj respektohet
            plotësisht dhe aktivitetet tona korrespondojnë me ligjet europiane,
            Rregulloren e Përgjithshme të Mbrojtjes së të Dhënave (Rregullorja e
            Përgjithshme e Mbrojtjes së të Dhënave 2016/679 e datës 27 prill
            2016) dhe ligjet në Republikën e Shqipërisë. <br />
            <br />
            Informacioni që mbledhim:
            <br />
            <br />
            <b>Të dhënat publike</b>
            <br />
            <br /> Është e mundur që ju të vizitoni dhe të përdorni faqen tonë
            pa zbuluar identitetin tuaj, apo ndonjë të dhënë tjetër personale që
            lidhet me të.
            <br />
            <br /> Kur vizitoni faqen tonë të internetit, merrni parasysh
            natyrën e internetit, mbledhja e të dhënave nga përdoruesit, si dhe
            për treguesit statistikorë që përdoren për të përmirësuar cilësinë e
            internetit të faqes. Informacion i tillë mund të përfshijë emrin e
            shfletuesit, numrin e vizitave, kohën mesatare të shpenzuar në faqe,
            llojin e kompjuterit dhe informacionin teknik për lidhjen e përdorur
            nga përdoruesi gjatë vizitës, siç është sistemi operativ dhe
            furnizuesi i shërbimit të internetit, adresave IP (Internet
            Protocol) që janë caktuar nga ISP-të dhe janë të ndryshme për
            secilin përdorues të internetit. <br />
            <br />
            <b>Të dhëna Personale</b>
            <br />
            <br /> Kur përdorni disa funksionalitete (aplikacione, kërkesa) në
            faqen tone, si forma të kontaktit, kërkesat e ofertës së
            automjeteve, test drive, kërkesa për servis dhe pjesë këmbimi,
            kërkesa për lajme, njoftime dhe promovime, etj. Shoqëria AVEL
            Sh.p.k. përpunon të dhënat tuaja personale dhe informacionin e
            kontaktit, vetëm nëse ju i dorëzoni vullnetarisht në faqen tonë të
            internetit për përdorimin e shërbimeve dhe produkteve tona. Këto të
            dhëna personale mund të përfshijnë emrin, mbiemrin, e-mail, numrin e
            shasisë, numrin e telefonit dhe / ose informacione të tjera të
            kontaktit, të cilat përdoren në përputhje me politikën e privatësisë
            dhe qëllimin për të cilin i keni lënë. <br />
            <br />
            <b>Objektivat, përpunimi dhe përdorimi i të dhënave</b>
            <br />
            <br /> Objektivat për të cilat mbledhim informacionin, janë si më
            poshtë: <br />
            <br />
            <ul className="pl-4 pr-4 pb-4">
              <li>Përpunimi i kërkesës tuaj;</li>
              <li>Menaxhimi i marrëdhënieve me klientët;</li>
              <li>
                Kryerja e hulumtimeve, kërkimeve, apo statistikave për të
                vlerësuar më mirë cilësinë e produkteve, si dhe parashikimi i
                nevojave të klientëve të saj;
              </li>
              <li>Analizimi dhe optimizimi i faqes së internetit;</li>
              <li>
                Dërgimi i ofertave tona promocionale, lajmet dhe ngjarjet
                (gazetat, ftesat, pyetësorët, sondazhet dhe botimet e tjera).
              </li>
            </ul>
            <b>Marrësit e të dhënave personale</b>
            <br />
            <br /> Të dhënat personale që ne përpunojmë, do të ndahen nga
            Shoqëria AVEL Sh.p.k. tek një numër i kufizuar përdoruesish, sipas
            qëllimit të përpunimit, si më poshtë:
            <br />
            <br />
            <ul className="pl-4 pr-4 pb-4">
              <li>
                Çdo kompani që i përket të njëjtit grup si Shoqëria AVEL
                Sh.p.k.;
              </li>
              <li>
                Çdo ofrues i shërbimeve të palës së tretë dhe / ose ndonjë
                kompani që i përket grupit të njëjtë si Shoqëria AVEL Sh.p.k, të
                përfshirë në ofrimin e servisit që kërkohet nga ju për qëllimet
                e lartpërmendura;
              </li>
              <li>
                Çdo partner tregëtar i Shoqërisë AVEL Sh.p.k., është i detyruar
                të ruajë konfidencialitetin.
              </li>
            </ul>
            <b>Qëllimi i ndarjes</b>
            <br />
            <br />
            <ul className="pl-4 pr-4 pb-4">
              <li>Për analizimin dhe zhvillimin e produkteve që ne ofrojmë;</li>
              <li>
                Për kryerjen e fushatave të marketingut, kryerjen e anketave të
                kënaqësisë së klientit dhe propozimin e ofertave tregtare të
                përshtatura për nevojat e klientëve;
              </li>
              <li>
                Për qëllime statistikore dhe përpunim statistikor mbi të dhëna
                anonime.
              </li>
            </ul>
            <b>Siguria dhe ruajtja e të dhënave</b>
            <br />
            <br />I gjithë komunikimi ndërmjet përdoruesve dhe faqes, bëhet
            përmes një protokolli të sigurt të koduar (https).
            <br />
            <br />
            Përpunimi i të dhënave tuaja personale do të bëhet në përputhje me
            rregullat për mbrojtjen e të dhënave personale të Republikës së
            Shqipërisë, duke marrë të gjitha masat teknike dhe organizative të
            përcaktuara për sigurimin e konfidencialitetit dhe mbrojtjes në
            përpunimin e të dhënave personale që kemi. Punonjësit e Shoqërisë
            AVEL Sh.p.k. janë të detyruar të mbajnë të gjitha informatat
            (përfshirë të dhënat personale) konfidenciale gjatë kryerjes së
            detyrave të tyre. Shoqëria AVEL Sh.p.k. zbaton masat e sigurisë për
            mbledhjen, ruajtjen dhe përpunimin e informacionit për t’u mbrojtur
            nga aksesimi, ndryshimi, zbulimi, ose shkatërrimi i të dhënave tuaja
            personale, si dhe të dhënat jo-personale të ruajtura në faqen tonë.
            Të dhënat personale mund të zbulohen vetëm tek autoritetet
            kompetente, siç parashikon ligji.
            <br />
            <br />
            Të dhënat do të ruhen për një periudhë të caktuar kohore, për aq
            kohë sa arsyeja për të cilin i keni lënë ato është akoma e vlefshme.
            Gjatë kësaj periudhe, ju rezervoni të gjitha të drejtat tuaja
            individuale, si ato të aksesimit, korrigjimit, fshirjes, kufizimit
            të përpunimit, transferimit, kundërshtimit dhe e drejta për të mos
            iu nënshtruar vendimmarrjes së automatizuar, duke përfshirë edhe
            profilizimin.
            <br />
            <br />
            Periudha e ruajtjes së të dhënave tuaja personale të mbledhura nga
            Shoqëria AVEL Sh.p.k., mund të përcaktohet me qëllim të përpunimit
            të të dhënave sipas kritereve të mëposhtme:
            <br />
            <br />
            <ul className="pl-4 pr-4 pb-4">
              <li>
                Kohëzgjatjes së marrëveshjes së lidhur me ju në përputhje me
                ligjin;
              </li>
              <li>Kohëzgjatjes marrëdhënies biznesit me ju.</li>
            </ul>
            Të dhënat e ruajtura për qëllime të caktuara, do të mbahen sipas
            nevojës për qëllime të përpunimit të të dhënave personale të
            përcaktuara në nenin "Objektivat, përpunimi dhe përdorimi i të
            dhënave".
            <br />
            <br />
            <b>Shfletuesit në faqen e internetit</b>
            <br />
            <br />
            “Cookies” janë skedarë të vegjël të tekstit që rregjistrohen në
            pajisjen tuaj (kompjuter, telefon celular, tabletë, etj.) për të
            mbajtur gjurmët e lëvizjes së shfletuesit tënd nga faqja e
            internetit dhe nuk janë të lidhur me të dhënat tuaja personale që ju
            keni dhënë. Me ndihmën e këtyre skedarëve, ndiqet lëvizja e
            vizitorëve, duke mbledhur të dhëna për të përmirësuar përvojën e
            përdoruesit dhe për të optimizuar dhe rritur funksionalitetin e
            faqes. Faqja ka një lloj “cookie” që ruhet në kompjuterin e
            përdoruesit për aq kohë sa është caktuar në shfletues. Përdoruesit
            mund të zgjedhin që të refuzojnë “cookie”. Nëse përdoruesit zgjedhin
            këtë mundësi, një pjesë e faqes së web-it mund të mos funksionojë
            mirë.
            <br />
            <br />
            Kur vizitoni njërën nga faqet tona të internetit, ose përdorni
            aplikacionet tona, ose mjete të tjera dixhitale, në varësi të
            pëlqimit tuaj, ne mund të përdorim “cookie” dhe teknologji të tjera
            për të mbledhur të dhënat e mëposhtme:
            <br />
            <br />
            <ul className="pl-4 pr-4 pb-4">
              <li>
                Adresën IP, informacionin e lidhjes, llojin e shfletuesit,
                vendndodhjen, zonën orare, sistemin operativ dhe informacione të
                tjera teknike;
              </li>
              <li>
                Informacion për vizitën tuaj, duke përfshirë faqet e internetit
                që vizitoni para dhe pas faqes sonë të internetit dhe produkteve
                që shihni;
              </li>
              <li>
                Kohëzgjatja e vizitave në materialet e faqes sonë të internetit
                dhe informacionin rreth ndërveprimit midis faqeve.
              </li>
            </ul>
            Shoqëria AVEL Sh.p.k. përdor “cookie” të palëve të treta (Google
            Analytics). Këto “cookie” janë për statistikat anonime dhe nuk
            përfshijnë informacion personal.
            <br />
            <br />
            Mos harroni se ju gjithmonë keni kontroll të plotë mbi “cookie”.
            Shumica e motorrëve të kërkimit, ofrojnë përjashtimin e plotë të
            “cookie” përmes opsioneve, për çdo shfletues veç e veç.
            <br />
            <br />
            <b>Marketingu</b>
            <br />
            <br />
            Informacioni i mbledhur përmes përdorimit të “cookie” mund të
            përdoret për marketing në Google AdWords dhe Facebook, ose për
            reklamim në faqet e interneteve të tjera që do të vizitohet nga
            përdoruesi, i cili më parë vizitoi faqen tonë të internetit.
            <br />
            <br />
            Google, Facebook dhe palët e tjera të treta shfaqin AdWords. Të
            ashtuquajturat "reklama" nëpër faqe dhe aplikacione të ndryshme që
            përdoruesi viziton dhe përdor. Këto "reklama" bazohen gjithashtu në
            vizitat e mëparshme të përdoruesve në faqen tonë të internetit duke
            përdorur “cookie”.
            <br />
            <br />
            Përdoruesit mund të personalizojnë preferencat e tyre mbi mënyrën se
            si Google dhe Facebook i reklamojnë ato duke hyrë në faqet e
            mëposhtme:
            <br />
            <br />
            Për "Google":
            <br />
            https://support.google.com/ads/answer/2662922?hl=en,
            <br />
            Për "Facebook":
            <br />
            https://www.facebook.com/ads/website_custom_audiences/.
            <br />
            Përdoruesi mund të heqë dorë tërësisht nga ky lloj reklamimi bazuar
            në interesin e tij të mëparshëm duke vendosur cilësimet e “cookie”
            në shfletuesin që përdoret nga përdoruesi. DoNotTrack ("DNT") është
            një funksionalitet që i lejon përdoruesit të ngrijë shfletuesin e
            tij në një mënyrë që informon faqet e internetit se lëvizja e tij në
            internet nuk është e gatshme të shihet. Funksioni "DNT", përdoruesi
            mund të aktivizojë, ose çaktivizojë, duke vizituar seksionin
            "Preferencat", ose "Cilësimet", në varësi të llojit të shfletuesit
            të internetit që përdor.
            <br />
            <br />
            <b>Liria e zgjedhjes, marrëveshja dhe të drejtat tuaja</b>
            <br />
            <br />
            Ju kontrolloni informacionin që jepni në faqen tonë të internetit.
            Sidoqoftë, nëse vendosni të mos lini të dhënat tuaja, ju lutem vini
            re se ju nuk mund të jeni në gjendje të përdorni disa pjesë të faqes
            së internetit.
            <br />
            <br />
            Duke plotësuar kërkesat në dispozicion (formularët e kontaktit) në
            faqen tonë të internetit, ju pranoni këtë politike privatësie dhe ju
            pranoni që të kontaktoheni sipas informacioneve të specifikuara në
            kontakt.
            <br />
            <br />
            Ne do t'i respektojmë plotësisht të gjitha të drejtat tuaja në
            përputhje me rregullat për mbrojtjen e të dhënave personale të
            Republikës së Shqipërisë dhe Rregulloreve të Përgjithshme për
            Mbrojtjen e të Dhënave Personale të Anëtarëve të Bashkimit Evropian
            (G.D.P.R.).
            <br />
            <br />
            Në pajtim me rregulloret ekzistuese për mbrojtjen e të dhënave
            personale, ju keni të drejtë të aksesoni, korrigjoni, fshini,
            kufizoni përpunimin, merrni një kopje të të dhënave tuaja personale
            për qëllimet tuaja, ose transferoni atë tek një ofrues tjetër i
            shërbimit, sipas dëshirës tuaj, si dhe të drejtën për të
            kundërshtuar përpunimin e të dhënave tuaja personale kur këto të
            dhëna përpunohen për qëllime të marketingut të drejtpërdrejtë dhe të
            mos i nënshtrohen vendimmarrjes automatike.
            <br />
            <br />
            Të gjitha këto të drejta, aplikohen brenda kufijve të parashikuar
            nga rregulloret ekzistuese për mbrojtjen e të dhënave personale.
            <br />
            <br />
            Këto të drejta mund të merren thjesht duke i kërkuar Shoqërisë AVEL
            Sh.p.k. në adresë e e-mail: info@avel.com.al
            <br />
            <br />
            <b>Ndryshime në politikën e privatësisë të të dhënave personale</b>
            <br />
            <br />
            Shoqëria AVEL Sh.p.k. ka të drejtë të përditësojë këtë politike
            privatësie në çdo kohë. Kur ta bëjmë këtë, ne do të japim njoftim në
            faqen e internetit ku dhe do të freskohet data e përditësimit në
            fund të kësaj faqeje. Përdoruesi pranon dhe pajtohet që është
            përgjegjësia e tij të kontrollojë periodikisht Politikën e
            Privatësisë dhe të jetë i vetëdijshëm për ndryshimet.
            <br />
            <br />
            <b>Pranimi i kushteve</b>
            <br />
            <br />
            Duke përdorur këtë faqe, Përdoruesi pajtohet me dispozitat e kësaj
            Politike të Privatësisë. Nëse nuk pajtoheni me Politikën, mos e
            përdorni faqen e internetit.
          </p>
        </div>
      </section>
    );
  }
}
