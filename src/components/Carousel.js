import React from "react";
import { Link } from "react-router-dom";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import covid from "../images/covid19.jpg";
import car2008 from "../images/2008.jpg";
import car208 from "../images/peugeot-new208.jpg";
import rifter from "../images/rifter.jpg";
import car508 from "../images/50888.jpg";
import partner from "../images/partner2.jpg";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";

function Carousel() {
  return (
    <section id="main-carousel">
      <Splide
        options={{
          type: "loop",
          rewind: true,
          lazyLoad: "nearby",
          autoplay: true,
          pagination: false,
        }}
      >
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={covid} alt="Image 1" />
          <Link to="/covid-19" className="more-btn">
            Më shumë
          </Link>
        </SplideSlide>
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={car2008} alt="Image 2" />
          <Link to="/automjete/peugeot-2008" className="more-btn">
            Më shumë
          </Link>
        </SplideSlide>
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={car208} alt="Image 3" />
          <Link to="/automjete/peugeot-208" className="more-btn">
            Më shumë
          </Link>
        </SplideSlide>
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={rifter} alt="Image 4" />
          <Link to="/automjete/peugeot-rifter" className="more-btn">
            Më shumë
          </Link>
        </SplideSlide>
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={car508} alt="Image 5" />
          <Link to="/automjete/peugeot-508" className="more-btn">
            Më shumë
          </Link>
        </SplideSlide>
        <SplideSlide>
          <img className="img-fluid" data-splide-lazy={partner} alt="Image 6" />
          <Link
            to="/lajme/partner-furgoni-nderkombetar-i-vitit"
            className="more-btn"
          >
            Më shumë
          </Link>
        </SplideSlide>
      </Splide>
      {/* <Link
        to="/automjete/peugeot-2008"
        id="one-for-all"
        className="more-btn hide"
      >
        Më shumë
      </Link> */}
    </section>
  );
}

export default Carousel;
