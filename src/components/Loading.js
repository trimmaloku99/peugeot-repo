import React from "react";

function Loading() {
  return (
    <div className="row justify-content-center p-5">
      <div className="spinner-border text-primary" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
}

export default Loading;
