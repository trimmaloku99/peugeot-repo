import React from "react";
import { Link } from "react-router-dom";
import car2008 from "../images/2008.png";
import car3008 from "../images/3008.png";
import car5008 from "../images/5008.png";

function Models() {
  return (
    <section className="pt-5">
      <h2 className="p-4 text-center">MODELET E REJA PEUGEOT</h2>
      <div id="car-models" className="card-group">
        <div className="card">
          {/* <Link to="/automjete/peugeot-2008"> */}
          <div className="card-body">
            <h5 className="card-title">PEUGEOT E RE 2008</h5>
            <img
              className="card-img-top"
              src={car2008}
              alt="Card image cap"
              width="240"
            />
            <p className="card-text">ELEGANCE HIGH TECH</p>
            <Link to="/automjete/peugeot-2008" className="more-btn">
              Më shumë
            </Link>
          </div>
          {/* </Link> */}
        </div>
        <div className="card">
          {/* <Link to="/automjete/suv-3008"> */}
          <div className="card-body">
            <h5 className="card-title">SUV 3008</h5>
            <img
              className="card-img-top"
              src={car3008}
              alt="Card image cap"
              width="240"
            />
            <p className="card-text">KRIJUAR PER AVENTURA</p>
            <Link to="/automjete/suv-3008" className="more-btn">
              Më shumë
            </Link>
          </div>
          {/* </Link> */}
        </div>
        <div className="card">
          {/* <Link to="/automjete/suv-5008"> */}
          <div className="card-body">
            <h5 className="card-title">SUV 5008</h5>
            <img
              className="card-img-top"
              src={car5008}
              alt="Card image cap"
              width="240"
            />
            <p className="card-text">STIL I GUXIMSHEM</p>
            <Link to="/automjete/suv-5008" className="more-btn">
              Më shumë
            </Link>
          </div>
          {/* </Link> */}
        </div>
      </div>
    </section>
  );
}

export default Models;
