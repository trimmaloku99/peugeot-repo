import React, { Component } from "react";
import Vetura from "../components/Vetura";
import peugeot208 from "../images/208shi.jpg";
import p108 from "../images/p108.png";
import p208 from "../images/208.png";
import p301 from "../images/301.png";
import p308 from "../images/308.png";
import p508 from "../images/508.png";
import rifter from "../images/rifter.png";
import partner from "../images/partner.png";
import traveller from "../images/Traveller.png";
import expert from "../images/expert.png";
import boxer from "../images/Boxer.png";
import p2008 from "../images/2008.png";
import p3008 from "../images/3008.png";
import p5008 from "../images/5008.png";
import $ from "jquery";
window.$ = $;

export default class Automjetet extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numbs: 0,
    };
  }

  total = () => {
    this.setState({ numbs: $("#automjetet-details .vehicle.show").length });
  };

  componentDidMount() {
    this.setState({
      numbs: $("#automjetet-details .vehicle.show").length,
    });
    this.numbs = $("#automjetet-details .vehicle.show").length;

    $(function () {
      $("#automjetet-details ul li").click(function () {
        // remove classes from all
        $("#automjetet-details ul li").removeClass("current");
        // add class to the one we clicked
        $(this).addClass("current");
      });
    });

    // first
    $("#automjetet-details ul li:nth-child(1)").click(function () {
      $("#automjetet-details a:nth-child(1)").removeClass("hide");
      $("#automjetet-details a:nth-child(2)").removeClass("hide");
      $("#automjetet-details a:nth-child(3)").removeClass("hide");
      $("#automjetet-details a:nth-child(4)").removeClass("hide");
      $("#automjetet-details a:nth-child(5)").removeClass("hide");
      $("#automjetet-details a:nth-child(6)").removeClass("hide");
      $("#automjetet-details a:nth-child(7)").removeClass("hide");
      $("#automjetet-details a:nth-child(8)").removeClass("hide");
      $("#automjetet-details a:nth-child(9)").removeClass("hide");
      $("#automjetet-details a:nth-child(10)").removeClass("hide");
      $("#automjetet-details a:nth-child(11)").removeClass("hide");
      $("#automjetet-details a:nth-child(12)").removeClass("hide");
      $("#automjetet-details a:nth-child(13)").removeClass("hide");
      $("#automjetet-details a:nth-child(1)").addClass("show");
      $("#automjetet-details a:nth-child(2)").addClass("show");
      $("#automjetet-details a:nth-child(3)").addClass("show");
      $("#automjetet-details a:nth-child(4)").addClass("show");
      $("#automjetet-details a:nth-child(5)").addClass("show");
      $("#automjetet-details a:nth-child(6)").addClass("show");
      $("#automjetet-details a:nth-child(7)").addClass("show");
      $("#automjetet-details a:nth-child(8)").addClass("show");
      $("#automjetet-details a:nth-child(9)").addClass("show");
      $("#automjetet-details a:nth-child(10)").addClass("show");
      $("#automjetet-details a:nth-child(11)").addClass("show");
      $("#automjetet-details a:nth-child(12)").addClass("show");
      $("#automjetet-details a:nth-child(13)").addClass("show");
    });

    // second
    $("#automjetet-details ul li:nth-child(2)").click(function () {
      $("#automjetet-details a:nth-child(9)").removeClass("show");
      $("#automjetet-details a:nth-child(10)").removeClass("show");
      $("#automjetet-details a:nth-child(11)").removeClass("show");
      $("#automjetet-details a:nth-child(12)").removeClass("show");
      $("#automjetet-details a:nth-child(13)").removeClass("show");
      $("#automjetet-details a:nth-child(9)").addClass("hide");
      $("#automjetet-details a:nth-child(10)").addClass("hide");
      $("#automjetet-details a:nth-child(11)").addClass("hide");
      $("#automjetet-details a:nth-child(12)").addClass("hide");
      $("#automjetet-details a:nth-child(13)").addClass("hide");
      $("#automjetet-details a:nth-child(1)").addClass("show");
      $("#automjetet-details a:nth-child(2)").addClass("show");
      $("#automjetet-details a:nth-child(3)").addClass("show");
      $("#automjetet-details a:nth-child(4)").addClass("show");
      $("#automjetet-details a:nth-child(5)").addClass("show");
      $("#automjetet-details a:nth-child(6)").addClass("show");
      $("#automjetet-details a:nth-child(7)").addClass("show");
      $("#automjetet-details a:nth-child(8)").addClass("show");
    });

    // third
    $("#automjetet-details ul li:nth-child(3)").click(function () {
      // $("#automjetet-details").css("justify-content", "end");
      $("#automjetet-details a:nth-child(1)").removeClass("show");
      $("#automjetet-details a:nth-child(2)").removeClass("show");
      $("#automjetet-details a:nth-child(4)").removeClass("show");
      $("#automjetet-details a:nth-child(5)").removeClass("show");
      $("#automjetet-details a:nth-child(7)").removeClass("show");
      $("#automjetet-details a:nth-child(9)").removeClass("show");
      $("#automjetet-details a:nth-child(10)").removeClass("show");
      $("#automjetet-details a:nth-child(11)").removeClass("show");
      $("#automjetet-details a:nth-child(12)").removeClass("show");
      $("#automjetet-details a:nth-child(13)").removeClass("show");
      $("#automjetet-details a:nth-child(1)").addClass("hide");
      $("#automjetet-details a:nth-child(2)").addClass("hide");
      $("#automjetet-details a:nth-child(4)").addClass("hide");
      $("#automjetet-details a:nth-child(5)").addClass("hide");
      $("#automjetet-details a:nth-child(7)").addClass("hide");
      $("#automjetet-details a:nth-child(9)").addClass("hide");
      $("#automjetet-details a:nth-child(10)").addClass("hide");
      $("#automjetet-details a:nth-child(11)").addClass("hide");
      $("#automjetet-details a:nth-child(12)").addClass("hide");
      $("#automjetet-details a:nth-child(13)").addClass("hide");
      $("#automjetet-details a:nth-child(3)").addClass("show");
      $("#automjetet-details a:nth-child(6)").addClass("show");
      $("#automjetet-details a:nth-child(8)").addClass("show");
    });

    // fourth
    $("#automjetet-details ul li:nth-child(4)").click(function () {
      // $(".vehicle-block").css("justify-content", "end");
      $("#automjetet-details a:nth-child(1)").removeClass("show");
      $("#automjetet-details a:nth-child(2)").removeClass("show");
      $("#automjetet-details a:nth-child(3)").removeClass("show");
      $("#automjetet-details a:nth-child(4)").removeClass("show");
      $("#automjetet-details a:nth-child(5)").removeClass("show");
      $("#automjetet-details a:nth-child(6)").removeClass("show");
      $("#automjetet-details a:nth-child(7)").removeClass("show");
      $("#automjetet-details a:nth-child(10)").removeClass("show");
      $("#automjetet-details a:nth-child(12)").removeClass("show");
      $("#automjetet-details a:nth-child(13)").removeClass("show");
      $("#automjetet-details a:nth-child(1)").addClass("hide");
      $("#automjetet-details a:nth-child(2)").addClass("hide");
      $("#automjetet-details a:nth-child(3)").addClass("hide");
      $("#automjetet-details a:nth-child(4)").addClass("hide");
      $("#automjetet-details a:nth-child(5)").addClass("hide");
      $("#automjetet-details a:nth-child(6)").addClass("hide");
      $("#automjetet-details a:nth-child(7)").addClass("hide");
      $("#automjetet-details a:nth-child(10)").addClass("hide");
      $("#automjetet-details a:nth-child(12)").addClass("hide");
      $("#automjetet-details a:nth-child(13)").addClass("hide");
      $("#automjetet-details a:nth-child(8)").addClass("show");
      $("#automjetet-details a:nth-child(9)").addClass("show");
      $("#automjetet-details a:nth-child(11)").addClass("show");
    });

    // fifth
    $("#automjetet-details ul li:nth-child(5)").click(function () {
      // $(".vehicle-block").css("justify-content", "end");
      $("#automjetet-details a:nth-child(1)").removeClass("show");
      $("#automjetet-details a:nth-child(2)").removeClass("show");
      $("#automjetet-details a:nth-child(3)").removeClass("show");
      $("#automjetet-details a:nth-child(4)").removeClass("show");
      $("#automjetet-details a:nth-child(5)").removeClass("show");
      $("#automjetet-details a:nth-child(6)").removeClass("show");
      $("#automjetet-details a:nth-child(7)").removeClass("show");
      $("#automjetet-details a:nth-child(8)").removeClass("show");
      $("#automjetet-details a:nth-child(9)").removeClass("show");
      $("#automjetet-details a:nth-child(11)").removeClass("show");
      $("#automjetet-details a:nth-child(1)").addClass("hide");
      $("#automjetet-details a:nth-child(2)").addClass("hide");
      $("#automjetet-details a:nth-child(3)").addClass("hide");
      $("#automjetet-details a:nth-child(4)").addClass("hide");
      $("#automjetet-details a:nth-child(5)").addClass("hide");
      $("#automjetet-details a:nth-child(6)").addClass("hide");
      $("#automjetet-details a:nth-child(7)").addClass("hide");
      $("#automjetet-details a:nth-child(8)").addClass("hide");
      $("#automjetet-details a:nth-child(9)").addClass("hide");
      $("#automjetet-details a:nth-child(11)").addClass("hide");
      $("#automjetet-details a:nth-child(10)").addClass("show");
      $("#automjetet-details a:nth-child(12)").addClass("show");
      $("#automjetet-details a:nth-child(13)").addClass("show");
    });
  }
  render() {
    return (
      <section id="automjetet">
        <h2 className="nav-title">GAMA PEUGEOT</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item">GAMA</li>
            <li className="breadcrumb-item" aria-current="page">
              MODELET TONA
            </li>
          </ol>
        </div>
        <img id="automjetet-img" src={peugeot208}></img>

        <div id="automjetet-details" className="pt-3">
          <ul>
            <li onClick={this.total} className="current">
              TE GJITHA MODELET
            </li>
            <li onClick={this.total}>GAMA PËR PASAGJERË</li>
            <li onClick={this.total}>GAMA SUV</li>
            <li onClick={this.total}>AUTOMJETE ME SHUMË VENDE (7,8,9)</li>
            <li onClick={this.total}>AUTOMJETE TREGTARE</li>
          </ul>
          <p id="available">
            <strong style={{ color: "#007be0" }}>{this.state.numbs}</strong>{" "}
            MODELE TE DISPONUESHME
          </p>

          <div className="row">
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p108}
              modelName={"108"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p208}
              modelName={"208"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p2008}
              modelName={"SUV 2008"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p301}
              modelName={"301"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p308}
              modelName={"308"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p3008}
              modelName={"SUV 3008"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p508}
              modelName={"508 E RE"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={p5008}
              modelName={"SUV 5008"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={rifter}
              modelName={"Rifter"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={traveller}
              modelName={"Traveller"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={partner}
              modelName={"Partner"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={expert}
              modelName={"Expert"}
              hrefi={"/"}
            />
            <Vetura
              classname={"vehicle col-md-3 show"}
              image={boxer}
              modelName={"Boxer"}
              hrefi={"/"}
            />
          </div>
        </div>
      </section>
    );
  }
}
