import React, { Component } from "react";
import { Link } from "react-router-dom";
import facebookSocial from "../images/facebook.png";
import $ from "jquery";
window.$ = $;

export default class Footer extends Component {
  componentDidMount() {
    $(window).bind("scroll", function () {
      if (
        $(window).scrollTop() >=
        $("#footer-content").offset().top +
          $("#footer-content").outerHeight() -
          window.innerHeight
      ) {
        $("#go-top a").fadeIn("slow");
        $("#go-top a").removeClass("hide");
        $("#go-top a").addClass("show");
      } else {
        $("#go-top a").fadeOut("slow", function () {
          $("#go-top a").removeClass("show");
          $("#go-top a").addClass("hide");
        });
      }
    });

    $("#go-top a").on("click", function (e) {
      e.preventDefault();
      $("html,body").animate({
        scrollTop: 0,
      });
    });
  }
  render() {
    return (
      <section className="pt-5">
        <div id="footer-content" className="mb-3">
          <div className="row p-3 m-0">
            <div className="col-lg-4 col-md-4 info">
              <p className="info-title">INFORMACION</p>
              <ul>
                <Link to="/kontakt">
                  <li>KONTAKT</li>
                </Link>
                <Link to="/automjete/oferte-automjete">
                  {" "}
                  <li>KËRKONI NJË OFERTË</li>
                </Link>
                <Link to="/automjete/test-drive">
                  <li>TEST DRIVE</li>
                </Link>
                <Link to="/sherbime-servis/pjese">
                  <li>KËRKESË PËR SERVIS DHE PJESË KËMBIMI</li>
                </Link>
              </ul>
              <p className="info-title">ZYRAT E SHITJES DHE SERVISIT</p>
            </div>
            <div className="col-lg-4 col-md-4 info">
              <p className="info-title">GAMA PEUGEOT</p>
              <ul>
                <Link to="/automjete">
                  <li>AUTOMJETE PËR PASAGJERË</li>
                </Link>
                <Link to="/automjete">
                  <li>AUTOMJETE TREGTARE</li>
                </Link>
              </ul>
            </div>
            <div className="col-lg-4 col-md-4 info text-center">
              <p className="info-title text-left">RRJETET SOCIALE</p>
              <a
                href="https://www.facebook.com/PeugeotKosova/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={facebookSocial} alt="Facebook Logo" />
              </a>
            </div>
          </div>

          <div className="row ml-5 mr-5">
            <Link
              to="/automjete"
              id="gama-alt"
              className="col-lg-4 col-md-4 alt-peugeot"
            >
              GAMA PEUGEOT
            </Link>
            <a
              href="http://www.peugeot.com/en"
              target="_blank"
              rel="noopener noreferrer"
              id="int-alt"
              className="col-lg-4 col-md-4 alt-peugeot text-center"
            >
              PEUGEOT INTERNATIONAL
            </a>
          </div>

          <div id="harta" className="pb-4">
            <div id="harta-faqes">
              <a
                id="harta-faqes-text"
                className="text-center"
                data-toggle="collapse"
                href="#harta-details"
                role="button"
                aria-expanded="false"
                aria-controls="harta-details"
              >
                HARTA E FAQES <i className="fas fa-angle-down"></i>
              </a>{" "}
              <div id="harta-details" className="collapse hide">
                <div className="row p-3">
                  <div className="col-lg-3 info">
                    <p className="info-title">GAMA</p>
                    <ul>
                      <Link to="/automjete">
                        <li>AUTOMJETE PËR PASAGJERË</li>
                      </Link>
                      <Link to="/automjete">
                        <li>AUTOMJETE TREGTARE</li>
                      </Link>
                    </ul>
                    <Link to="/automjete/automjete-te-perdorura">
                      <p className="info-title">AUTOMJETE TË PËRDORURA</p>
                    </Link>
                  </div>
                  <div className="col-lg-3 info">
                    <Link to="/sherbime-servis">
                      <p className="info-title">SERVISI PEUGEOT</p>
                    </Link>
                    <ul>
                      <Link to="/sherbime-servis">
                        <li>SERVISI</li>
                      </Link>
                      <Link to="/sherbime-servis">
                        <li>PROMOCIONE</li>
                      </Link>
                    </ul>
                  </div>
                  <div className="col-lg-3 info">
                    <p className="info-title">INFORMACION</p>
                    <ul>
                      <Link to="/rreth-nesh">
                        <li>RRETH NESH</li>
                      </Link>
                      <Link to="/kontakt">
                        <li>KONTAKT</li>
                      </Link>
                      <Link to="/politikat">
                        <li>
                          POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE
                          PERSONALE
                        </li>
                      </Link>
                    </ul>
                    <Link to="/vendndodhja">
                      <p className="info-title">
                        ZYRAT E SHITJES DHE TË SERVISIT
                      </p>
                    </Link>
                  </div>
                  <div className="col-lg-3 info">
                    <p className="info-title">LAJME DHE OFERTA</p>
                    <ul>
                      <Link to="/lajme">
                        <li>LAJME</li>
                      </Link>
                      <Link to="/oferta">
                        <li>OFERTA</li>
                      </Link>
                    </ul>
                  </div>
                  <div
                    id="harta-button-div"
                    className="row w-100 justify-content-end"
                  >
                    <Link to="/harta-e-faqes">
                      <button id="harta-button">HARTA E FAQES</button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="legal-text" className="text-center p-2">
          Faqja www.peugeot.al zotërohet dhe administrohet nga AVEL Sh.p.k. me
          adresë Autostrada Tiranë-Durrës Km.5, Rruga Monun Nr.58, Fshati
          Kashar, 1051 Tiranë, Shqipëri. <br />
        </div>
        <div id="legal-text2" className="text-center pb-5">
          POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE PERSONALE
        </div>

        <div id="go-top">
          <a className="hide">
            <i className="fas fa-chevron-up"></i>
          </a>
        </div>
      </section>
    );
  }
}
