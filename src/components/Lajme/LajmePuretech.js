import React, { Component } from "react";
import { Link } from "react-router-dom";
import puretech from "../../images/puretech.jpg";

export default class LajmePuretech extends Component {
  render() {
    return (
      <section id="lajmepuretech">
        <h2 className="nav-title">
          PËR HERË TË KATËRT, PURETECH TURBO MERR ÇMIMIN "MOTORI I VITIT"
        </h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/lajme">LAJME</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PËR HERË TË KATËRT, PURETECH TURBO MERR ÇMIMIN "MOTORI I VITIT"
            </li>
          </ol>
        </div>

        <div id="lajmepuretech-details">
          <h2 className="text-center">
            PËR HERË TË KATËRT, PURETECH TURBO MERR ÇMIMIN "MOTORI I VITIT"
          </h2>
          <div className="row">
            <p className="col-lg-6 p-4">
              <br />
              Motori Turbo PureTech 110 S&S dhe 130 S&S me tre cilindra benzine
              në kategorinë 1L deri në 1.4L u zgjodh "Motori i Vitit" për herë
              të katërt! Gjenerata e re e motorit PureTech vjen me një
              performancë të përmirësuar rritje të efikasitetit duke reduktuar
              konsumin. Këta motorë garantojnë: -Uljen e konsumit me një
              mesatare prej 4% krahasuar me gjeneratën e mëparshme të motorëve;
              -Zvogëlimin me 75% të shkarkimeve të grimcave në sajë të filtrit
              FAP dhe përputhshmërisë me standardet RDE të parashikuara për të
              hyrë në fuqi nga viti 2020; -Ajo përshtatet me platformën e re CMP
              për automjetet urbane, limuzine dhe SUV kompakt. -Këto motorë të
              gjeneratës së re të benzinës së pari janë inkorporuar në vitin
              2017 dhe që janë aplikuar masivisht në automjete nga Grupi PSA në
              më shumë se 70 vende. Këta motorë janë në përputhje me rregulloret
              evropiane Euro 6. Alan Rapozo, Drejtor i Sektorit Motorik dhe
              Shasise në Grupin PSA, tha: "Jemi krenar për këtë çmim prestigjioz
              për të katërtin vit radhazi, që është provë e kompetencës dhe
              ekspertizës së ekipeve tona të projektimit. Sot, ky motor është
              pjesë e shumë automjeteve dhe ka qenë një sukses i madh për
              klientët tanë. Konkurrueshmëria dhe efikasiteti i games së
              motorëve të benzinës, veçanërisht motori me tre cilindra PureTech,
              është i njohur në të gjithë botën ". Aktualisht, motori me tre
              cilindra i benzinës PureTech prodhohet në fabrikat franceze në Pas
              de Calais dhe Moselle dhe në Xiang Yang të Kinës.
            </p>
            <img className="col-lg-6 p-4 h-100" src={puretech}></img>
          </div>
        </div>
      </section>
    );
  }
}
