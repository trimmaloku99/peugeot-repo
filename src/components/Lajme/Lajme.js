import React, { Component } from "react";
import { Link } from "react-router-dom";
import partneri from "../../images/peugeot-partner.jpg";
import p508 from "../../images/508shi.jpg";
import dakar from "../../images/dakar.jpg";
import puretech from "../../images/puretech.jpg";

export default class Lajme extends Component {
  render() {
    return (
      <section id="lajme">
        <h2 className="nav-title">LAJME</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              LAJME
            </li>
          </ol>
        </div>

        <div id="lajme-details">
          <div className="row">
            <div className="card col-lg-4">
              <img className="w-100" src={partneri}></img>
              <div className="card-body w-100 removeWidth">
                <h2 className="card-title">
                  PEUGEOT e re PARTNER: Furgoni ndërkombëtar i vitit 2019
                </h2>
                <br />
                <p className="card-text">
                  PEUGEOT e re PARTNER zgjidhet Furgoni ndërkombëtar për vitin
                  2019, çmim që njeh përkushtimin e ekipeve të PEUGEOT në një
                  projekt që është në qendër të strategjisë së zhvillimit të
                  markës, dhe që i jepet për të pestën herë që kur trofeu u
                  krijua në vitin 1992.
                </p>
                <Link to="/lajme/partner-furgoni-nderkombetar-i-vitit">
                  <button className="card-btn">Më shumë</button>
                </Link>
              </div>
            </div>
            <div className="card col-lg-4">
              <img className="w-100" src={dakar}></img>
              <div className="card-body w-100 removeWidth">
                <h2 className="card-title">
                  Dakar 2018: Fitorja e tretë rradhazi për Peugeot
                </h2>
                <br />
                <p className="card-text">
                  Pas 8793 kilometrave të dunave, kalimeve malore, shtigjeve
                  shkëmbore dhe rreziqeve të ndryshme përmes Perusë, Bolivisë
                  dhe Argjentinës, ekipi i Carlos Sainz / Lucas Cruz i dha
                  Peugeot 3008 DKR Maxi një fitore të mrekullueshme për Rallyin
                  e 40-të të Dakarit.
                </p>
                <Link to="/lajme/dakar-2018">
                  <button className="card-btn">Më shumë</button>
                </Link>
              </div>
            </div>
            <div className="card col-lg-4">
              <img className="w-100" src={p508}></img>
              <div className="card-body w-100 removeWidth">
                <h2 className="card-title">
                  Peugeot 508: Makina më e bukur e vitit 2018 në Europë
                </h2>
                <br />
                <p className="card-text">
                  Peugeot mori dy çmime në Festivalin e 34-të Ndërkombëtar të
                  Automobilave të mbajtur më 29 janar 2019 në Hotel des
                  Invalides në Paris. Peugeot 508 i ri fitoi çmimin Grand Prix
                  për makinën më të bukur të vitit 2018 dhe Koncepti Peugeot
                  e-LEGEND mori Çmimin e Madh Prix për Konceptin më të bukur të
                  vitit. Peugeot 508 fitoi çmimin me mbi 34% të votave.
                </p>
                <Link to="/lajme/508-makina-me-e-bukur">
                  <button className="card-btn">Më shumë</button>
                </Link>
              </div>
            </div>
            <div className="card col-lg-4">
              <img className="w-100" src={puretech}></img>
              <div className="card-body w-100 removeWidth">
                <h2 className="card-title">
                  Për herë të katërt, motori i benzinës PureTech merr cmimin
                  "Motori i vitit"
                </h2>
                <br />
                <p className="card-text">
                  23.3.2017 Motori Turbo PureTech 110 S&S dhe 130 S&S me tre
                  cilindra benzinë në kategorinë 1L deri në 1.4L u zgjodh
                  "Motori i Vitit" për herë të katërt! Gjenerata e re e motorit
                  PureTech vjen me një performancë të përmirësuar në rritje të
                  efikasitetit duke reduktuar konsumin. Këta motorë garantojnë:
                </p>
                <Link to="/lajme/puretech-motori-vitit">
                  <button className="card-btn">Më shumë</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
