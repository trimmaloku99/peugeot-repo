import React, { Component } from "react";
import { Link } from "react-router-dom";
import sl1 from "../../images/sl1.jpg";

export default class LajmeDakar extends Component {
  render() {
    return (
      <section id="lajmedakar">
        <h2 className="nav-title">
          DAKAR 2018: FITORJA E TRETË RRADHAZI PËR PEUGEOT
        </h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/lajme">LAJME</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              DAKAR 2018: FITORJA E TRETË RRADHAZI PËR PEUGEOT
            </li>
          </ol>
        </div>

        <div id="lajmedakar-details">
          <div className="row">
            <p className="col-lg-6 p-4">
              <br />
              Pas 8793 kilometrave të dunave, kalimeve malore, shtigjeve
              shkëmbore dhe rreziqeve të ndryshme përmes Perusë, Bolivisë dhe
              Argjentinës, ekipi i Carlos Sainz / Lucas Cruz i dha Peugeot 3008
              DKR Maxi një fitore të mrekullueshme për Rallyin e 40-të të
              Dakarit. Fitorja e Sainz do të thotë gjithashtu një fitore e tretë
              radhazi për ekipin Peugeot. Për fat të keq, ekipi francez ka
              njoftuar tashmë se kjo do të jetë ngjarja përfundimtare e Dakarit.
              "Peugeot tashmë ka dy fitore, por unë mendoj se e kemi merituar
              këtë, sepse ne kemi investuar shumë në këtë veturë", tha Sainz,
              duke shtuar, "Ne patëm momente të mira dhe të këqija, por
              gjithmonë dhamë maksimumin. Sidomos në këtë gare, ishte vërtet e
              vështirë. Në fillim, thashë se do të jemi më të kujdesshëm, por
              Peugeot tha se duhej të shkonim me fuqi të plotë. Fillimisht ecnim
              përpara dhe më pas ne thjesht nuk duhej të bënim gabime. " Rruga
              për në edicionin e 40-të të Dakarit u tregua veçanërisht e
              vështirë nga ana mekanike. Pas këtij testi të vështirë të
              qëndrueshmërisë së gati 9,000 kilometrave, jo vetëm që Peugeot
              3008 DKR Maxi fitoi shtatë nga 13 fazat, por gjithashtu tregoi
              besueshmëri të jashtëzakonshme. PEUGEOT Total Team këtë vit fiton
              suksesin tashmë mbresëlënës të Peugeot në Rally Dakar dhe është
              fitorja e tyre e shtatë në tetë përpjekje! Ja një permbledhje e
              shpejtë e gjithçkaje qe Peugeot shënoi ne Dakar: 1988: Win
              Kankkunen / Piironen (205 T16 Grand Raid), 1989: 1-2 për Vatanen /
              Berglund dhe Ickx / Tarin (405 T16 Grand Raid) 1990: 1-2-3 për
              Vatanen / Berglund, Valdegard / Fenouil (dy 405 T16 Grand Raid),
              Ambrosino / Baumgartner (205 T16 Grand Raid) 2016: Fitorja për
              Peterhansel / Cottret (2008DKR) 2017: 1-2-3 për Peterhansel /
              Cottret, Loeb / Elena dhe Despres / Castera (3008 DKR) 2018:
              Fitorja për Sainz / Cruz (3008DKR Maxi). Ky sukses i tretë i
              radhës i ekipit PEUGEOT Total ne rally-n më të pasur të botës
              pasqyron suksesin tregtar të SUV Peugeot 3008, i cili u emërua
              "Makina e Vitit" në vitin 2017.
            </p>
            <img className="col-lg-6 p-4 h-100" src={sl1}></img>
          </div>
        </div>
      </section>
    );
  }
}
