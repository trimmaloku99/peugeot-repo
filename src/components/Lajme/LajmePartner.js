import React, { Component } from "react";
import { Link } from "react-router-dom";
import partneri from "../../images/peugeot-partner.jpg";

export default class LajmePartner extends Component {
  render() {
    return (
      <section id="lajmepartner">
        <h2 className="nav-title">
          PEUGEOT E RE PARTNER: FURGONI NDËRKOMBËTAR I VITIT 2019
        </h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/lajme">LAJME</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PEUGEOT E RE PARTNER: FURGONI NDËRKOMBËTAR I VITIT 2019
            </li>
          </ol>
        </div>

        <div id="lajmepartner-details">
          <h2 className="text-center">
            PEUGEOT e re PARTNER: Furgoni ndërkombëtar i vitit 2019
          </h2>
          <div className="row">
            <p className="col-lg-6 p-4">
              <br />
              PEUGEOT e re PARTNER: Furgoni ndërkombëtar i vitit 2019 PEUGEOT e
              re PARTNER zgjidhet Furgoni ndërkombëtar për vitin 2019, çmim që
              njeh përkushtimin e ekipeve të PEUGEOT në një projekt që është në
              qendër të strategjisë së zhvillimit të markës dhe që i jepet për
              të pestën herë që kur trofeu u krijua në vitin 1992. Dy inovacione
              të mëdha tërhoqën vëmendjen: Treguesi i mbingarkesës për ngarkim
              të sigurt dhe Surround Rear Vision që ofron vizion me kamera.
              PEUGEOT PARTNER aplikon për herë të parë në këtë segment PEUGEOT
              i-Cockpit®, duke ofruar një eksperiencë të re drejtimi për
              produktivitet akoma më të mirë. Çdo profesionist do të jetë në
              gjendje të krijojë konfigurimin e vet që do t'i lejojë ata të
              punojnë në mënyrë efektive, duke përfshirë versionin Grip për të
              përmbushur nevojën për shërbime të forta në të gjitha rrethanat,
              ose versionin Asphalt për ata që shpenzojnë shumë kohë në
              automjetin e tyre. PEUGEOT e re PARTNER e forcon ambicjen akoma më
              tej për të zotëruar të pamundurën.
            </p>
            <img className="col-lg-6 p-4 h-100" src={partneri}></img>
          </div>
        </div>
      </section>
    );
  }
}
