import React, { Component } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Link } from "react-router-dom";
import p508 from "../../images/p508.jpg";
import p508hint from "../../images/p508hint.jpg";
import p508gt from "../../images/p508gt.jpg";
import p508v from "../../images/p508v.jpg";
import p508b from "../../images/p508b.jpg";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
export default class Lajme508 extends Component {
  render() {
    return (
      <section id="lajme508">
        <h2 className="nav-title">
          PEUGEOT 508: MAKINA MË E BUKUR E VITIT 2018
        </h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/lajme">LAJME</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              PEUGEOT 508: MAKINA MË E BUKUR E VITIT 2018
            </li>
          </ol>
        </div>

        <div id="lajme508-details">
          <h2 className="text-center">
            PEUGEOT 508: MAKINA MË E BUKUR E VITIT 2018
          </h2>
          <div className="row">
            <p className="col-lg-6 p-4">
              <br />
              Peugeot mori dy çmime në Festivalin e 34-të Ndërkombëtar të
              Automobilave të mbajtur më 29 janar 2019 në Hotel des Invalides në
              Paris. Peugeot 508 i ri fitoi çmimin Grand Prix për makinën më të
              bukur të vitit 2018 dhe Koncepti Peugeot e-LEGEND mori Çmimin e
              Madh Prix për Konceptin më të bukur të vitit. Peugeot 508 fitoi
              çmimin me mbi 34% të votave. Peugeot 508 ka pasur mbi 10,000
              urdhra në Evropë që nga fillimi i saj në Tetor 2018. Çdo vit,
              International Automobile Festival shpërblen planet më atraktive të
              automobilave. Arkitekti Jean-Michel Wilmotte dhe dizajni i modës
              Chantal Thomass kryesuan një juri të ekspertëve nga sektorët e
              automobilave, modës, kulturës dhe dizajnit.
            </p>
            <div className="col-lg-6 p-4" id="508-carousel">
              <Splide
                options={{
                  type: "loop",
                  rewind: true,
                  lazyLoad: true,
                  autoplay: true,
                  pagination: false,
                }}
              >
                <SplideSlide>
                  <img className="img-fluid w-100" src={p508} alt="Image 1" />
                </SplideSlide>
                <SplideSlide>
                  <img
                    className="img-fluid w-100"
                    src={p508hint}
                    alt="Image 2"
                  />
                </SplideSlide>
                <SplideSlide>
                  <img className="img-fluid w-100" src={p508gt} alt="Image 3" />
                </SplideSlide>
                <SplideSlide>
                  <img className="img-fluid w-100" src={p508v} alt="Image 4" />
                </SplideSlide>
                <SplideSlide>
                  <img className="img-fluid w-100" src={p508b} alt="Image 5" />
                </SplideSlide>
              </Splide>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
