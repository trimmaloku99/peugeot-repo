import React, { Component } from "react";
import peugeoti from "../images/peugeoti.jpg";

export default class RrethNesh extends Component {
  render() {
    return (
      <section id="rrethnesh">
        <h2 className="nav-title">RRETH NESH</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              RRETH NESH
            </li>
          </ol>
        </div>
        <img id="rrethnesh-img" src={peugeoti}></img>

        <div>
          <h2 className="p-5 text-center">
            Prezantimi i Shoqërisë Avel Sh.p.k.
          </h2>
          <p className="p-3">
            Shoqëria me përgjegjësi të kufizuar Avel Sh.p.k. u themelua në
            Tiranë më datën 22.10.2002. <br />
            <br /> Shoqëria Avel Sh.p.k. ka marrë ekskluzivitetin nga firma
            Automobiles Peugeot – Paris, Francë për shitjen e automjeteve dhe
            pjesëve të këmbimit Peugeot, që prej datës 01.01.2003, për të gjithë
            territorin e Republikës së Shqipërisë. <br />
            <br />
            Ekipi ynë me përvojë, vazhdimisht ofron ekspertizë me qëllim që të
            ndihmojë klientët tanë për plotësimin e të gjitha kërkesave dhe
            problemeve të tyre. <br />
            <br /> Ekipi ynë është këshilltari juaj në çdo moment. Jemi
            gjithmonë të gatshëm të ofrojmë asistencë për t'ju çuar drejt
            zgjidhjes më të mirë të mundshme. Gjithashtu, pranë ekipit tonë do
            të merrni të gjitha informacionet me detajet më të imta për mënyrat
            e shkëmbimit të automjeteve të përdorura me automjete të reja.{" "}
            <br />
            <br /> Ekipi ynë do t'ju ndihmojnë të merrni në konsideratë edhe
            opsionet e financimit dhe do t'ju ofrojë programe të sigurimit të
            përshtatura sipas nevojave tuaja individuale. <br />
            <br /> Konceptimi i ndërtesës së Shoqërisë Avel Sh.p.k., Servisit,
            Sallonit të Automjeteve, Aparaturave dhe Kualifikimi i Personelit,
            është bërë sipas Standartave të kompanise Automobiles Peugeot
            Francë. <br />
            <br /> Në Servisin e Shoqërisë Avel Sh.p.k. – Tiranë, kryhen
            shërbimet e mëposhtme : <br />
            <br />
            <ul className="p-4">
              <li>Diagnostikim i defektit me aparaturë kompjuterike</li>
              <li>
                Diagnostikim në banko-provë për sistemet e frenimit, drejtimit
                dhe amortizimit
              </li>
              <li>Punime motorrike të pjesëshme dhe kapitale</li>
              <li>Punime xhenerike të pjesëshme dhe kapitale</li>
              <li>Punime llamarine të pjesëshme dhe kapitale</li>
              <li>Lyerje me bojë</li>
              <li>Punime elektroauto</li>
              <li>Montim vetëm i pjesëve origjinale</li>
              <li>
                Lidhje direkte me internet për riparim defektesh me platformën e
                servisit Peugeot në Francë
              </li>
              <li>Konvergjencë</li>
              <li>Montim, çmontim dhe ballancim gomash</li>
              <li>Shërbim i plotë për sistemin e ajrit të kondicionua</li>
              <li>Garanci për të gjitha shërbimet e kryera</li>
            </ul>
            <br />
            <br /> Jemi i vetmi servis i besueshëm për mirëmbajtjen dhe
            riparimin e automjeteve Peugeot, gjë që tregohet nga numri i madh i
            shërbimeve që ne kryejmë, jo vetëm për automjetet Peugeot të shitura
            nga Shoqëria jonë, por edhe nga ato që klientë të ndryshëm i kanë
            blerë jashtë Shqipërisë. <br />
            <br />
            Avel Sh.p.k. që nga viti 2010, është fitues i Trofeut Ndërkombëtar
            të Pas Shitjes.
            <br />
            <br /> Avel Sh.p.k. ofron të gjitha llojet e shërbimeve për
            automjetet Peugeot, sipas standarteve të Automobilave Peugeot
            France. <br />
            <br />
            Duke patur parasysh cilësinë e automjeteve që ofrojmë, çmimet e
            shitjes së tyre, shërbimin tonë të kualifikuar, parashikojmë që
            automjetet Peugeot nga viti në vit të shtohen në tregun Shqiptar të
            automjeteve.
          </p>
        </div>
      </section>
    );
  }
}
