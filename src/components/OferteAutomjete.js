import React, { Component } from "react";
import { Link } from "react-router-dom";
import OferteForm from "../components/OferteForm";

export default class OferteAutomjete extends Component {
  render() {
    return (
      <section id="oferteautomjete">
        <h2 className="nav-title">KËRKONI OFERTË PËR MAKINË</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              <Link to="/sherbime-servis">SERVISI PEUGEOT</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              KËRKONI OFERTË PËR MAKINË
            </li>
          </ol>
        </div>

        <div id="oferteautomjete-details">
          <h5 className="text-center">
            Ekipi Peugeot është në dispozicionin tuaj
            <br /> Ju lutem na kontaktoni në:
          </h5>
          <p className="p-4">
            Zyra e shitjes: AVEL Sh.p.k. – Peugeot Albania. Adresa: Autostrada
            Tiranë-Durrës Km 5, Rruga Monun Nr.58, Fshati Kashar, 1051 Tirane,
            Shqipëri.
            <br /> Rezervim Servisi: +355 69 600 6060
            <br /> Tel.: +355 48 200 703 Cel.: +355 69 20 81 552; +355 69 20 81
            554
            <br /> E-mail; info@avel.com.al
          </p>
          <h5 className="pt-4 pb-4 text-center">
            Për çdo kërkesë, plotësoni informacionin e meposhtëm:
          </h5>
          <p className="p-4 pl-2">
            * TË PLOTËSOHEN TË GJITHA FUSHAT E SHËNUARA ME YLL
          </p>
          <div className="col-lg-12 backu">
            <span>ZGJIDHNI MODELIN</span>
          </div>
          <OferteForm />

          <p className="p-3" style={{ color: "#999999" }}>
            Mbrojtja e informacionit tuaj personal është përparësia jonë, për më
            shumë informacion ju lutemi lexoni me kujdes Politikat e privatësisë
            dhe mbrojtja e të dhënave personale.
            <br />
            Të dhënat personale të kësaj kërkese janë të destinuara për AVEL
            Sh.p.k., përfaqësues zyrëtar i PEUGEOT në Shqipëri. Të dhënat tuaja
            do të përdoren për të realizuar kërkesën, si dhe për analiza
            statistikore dhe kërkime.
            <br />
            Në varësi të pëlqimit tuaj, të shprehur më lart, ato gjithashtu do
            të përdoren për t’ju dërguar informacion mbi ofertat, lajmet dhe
            njoftimet (gazetat, ftesat dhe publikimet e tjera) nga AVEL Sh.p.k.
          </p>
        </div>
      </section>
    );
  }
}
