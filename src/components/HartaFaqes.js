import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class HartaFaqes extends Component {
  render() {
    return (
      <section id="hartafaqes">
        <h2 className="nav-title">HARTA E FAQES</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              HARTA E FAQES
            </li>
          </ol>
        </div>

        <div id="hartafaqes-details">
          <div className="row">
            <div className="col-md-4">
              <Link to="/automjete">
                <h2>GAMA</h2>
              </Link>
              <ul className="pl-2 pt-3">
                <p className="font-light">
                  <Link to="/automjete">
                    <strong>GAMA PEUGEOT</strong>
                  </Link>
                </p>
                <li>
                  <Link to="automjete/peugeot-108">108</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-208">208</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-2008">PEUGEOT SUV 2008</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-301">301</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-308">308</Link>
                </li>

                <li>
                  <Link to="automjete/peugeot-3008">PEUGEOT SUV 3008</Link>
                  <ul className="pl-2">
                    <li>
                      <Link to="automjete/peugeot-3008/stil">STIL</Link>
                    </li>
                    <li>
                      <Link to="automjete/peugeot-3008/eksperienca-ne-drejtim">
                        EKSPERIENCA NË DREJTIM
                      </Link>
                    </li>
                    <li>
                      <Link to="automjete/peugeot-3008/teknologjia">
                        ТEKNOLOGJIA
                      </Link>
                    </li>
                    <li>
                      <Link to="automjete/peugeot-3008/informacione-teknike">
                        INFORMACIONE TEKNIKE
                      </Link>
                    </li>
                    <li>
                      <Link to="automjete/peugeot-3008/galeria">GALERIA</Link>
                    </li>
                  </ul>
                </li>
                <li>
                  <Link to="automjete/peugeot-508">508</Link>
                </li>

                <li>
                  <Link to="automjete/peugeot-5008">PEUGEOT SUV 5008</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-traveller">Traveller</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-rifter">Rifter</Link>
                </li>
                <p className="font-light pt-3">
                  <Link to="automjete#automjete-pune">
                    <strong>AUTOMJETE PUNE</strong>
                  </Link>
                </p>
                <li>
                  <Link to="automjete/peugeot-partner">Partner</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-expert">Expert</Link>
                </li>
                <li>
                  <Link to="automjete/peugeot-boxer">Boxer</Link>
                </li>
                <p className="font-light pt-3">
                  <Link to="oferta">
                    <strong>OFERTA</strong>
                  </Link>
                </p>

                <p className="font-light">
                  <Link to="automjete/automjete-te-perdorura">
                    <strong>AUTOMJETE TË PËRDORURA</strong>
                  </Link>
                </p>
              </ul>
            </div>

            <div className="col-md-4">
              <Link to="/sherbime-servis">
                <h2>SERVISI PEUGEOT</h2>
              </Link>
              <ul className="pl-2 pt-3">
                <p className="font-light">
                  <Link to="sherbime-servis">
                    <b>SERVISI</b>
                  </Link>
                </p>
                <li>
                  <Link to="sherbime-servis/intervali-sherbimit">
                    INTERVALET E SHËRBIMIT
                  </Link>
                </li>
                <p className="font-light pt-3">
                  <Link to="sherbime-servis">
                    <b>PROMOCIONE</b>
                  </Link>
                </p>

                <li>
                  <Link to="sherbime-servis/filtri-i-antialergjise">
                    FILTRI I ANTIALERGJISË
                  </Link>
                </li>
                <li>
                  <Link to="sherbime-servis/kit-distributori">
                    KIT I DISTRIBUTORIT
                  </Link>
                </li>

                <li>
                  <Link to="sherbime-servis/kontrolli-i-baterise">
                    KONTROLLI I BATERISË
                  </Link>
                </li>
                <li>
                  <Link to="sherbime-servis/paketa-e-xhamave">
                    PAKETA E XHAMAVE
                  </Link>
                </li>
                <li>
                  <Link to="sherbime-servis/sistemi-i-frenimit">
                    SISTEMI I FRENIMIT
                  </Link>
                </li>
                <li>
                  <Link to="sherbime-servis/kontrolli-i-kondicionerit">
                    KONTROLLI I KONDICIONERIT
                  </Link>
                </li>

                <br />
                <br />
              </ul>
            </div>

            <div className="col-md-4">
              <h2 className="text-muted">INFORMACION</h2>
              <ul className="pl-2 pt-3">
                <p className="font-light">
                  <Link to="kontakt">
                    <b>КОNТАКТ</b>
                  </Link>
                </p>
                <li>
                  <Link to="rreth-nesh">RRETH NESH</Link>
                </li>
                <p className="font-light pt-3">
                  <Link to="politikat">
                    <b>
                      POLITIKAT E PRIVATËSISË DHE MBROJTJA E TË DHËNAVE
                      PERSONALE
                    </b>
                  </Link>
                </p>
                <p className="font-light">
                  <Link
                    to="automjete/oferte-automjete"
                    title="KËRKONI OFERTË PËR AUTOMJET"
                  >
                    <b>KËRKONI OFERTË PËR AUTOMJET</b>
                  </Link>
                </p>
                <p className="font-light">
                  <Link to="automjete/test-drive" title="TEST DRIVE">
                    <b>TEST DRIVE</b>
                  </Link>
                </p>
                <p className="font-light">
                  <Link to="sherbime-servis/pjese">
                    <b>KËRKESË PËR SERVIS DHE PJESË KËMBIMI</b>
                  </Link>
                </p>
              </ul>
            </div>

            <div className="col-md-4 pt-5">
              <Link to="/vendndodhja">
                <h2>VENDNDODHJA</h2>
              </Link>
            </div>
            <div className="col-md-4 pt-5">
              <Link to="/financimi">
                <h2>FINANCIMI</h2>
              </Link>
            </div>
            <div className="col-md-4 pt-5">
              <Link to="/lajme">
                <h2>LAJME</h2>
              </Link>
              <ul className="pl-2 pt-3">
                <p className="font-light">
                  <Link to="/lajme">
                    <b>LAJME</b>
                  </Link>
                </p>
                <li>
                  <Link
                    to="lajme/puretech-motori-vitit"
                    title="Për herë të katërt motori PureTech merr çmimin motori i vitit"
                  >
                    Për herë të katërt motori PureTech merr çmimin motori i
                    vitit
                  </Link>
                </li>
                <li>
                  <Link
                    to="lajme/508-makina-me-e-bukur"
                    title="Peugeot 508, makina më e bukur e vitit 2018"
                  >
                    Peugeot 508, makina më e bukur e vitit 2018
                  </Link>
                </li>
                <li>
                  <Link
                    to="lajme/dakar-2018"
                    title="Dakar 2018: Fitorja e tretë rradhazi për Peugeot"
                  >
                    Dakar 2018: Fitorja e tretë rradhazi për Peugeot
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
