import React, { Component } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Link } from "react-router-dom";
import pgt208 from "../../images/208gt.jpg";
import p208d from "../../images/p208d.jpg";
import p208d2 from "../../images/p208d2.jpg";
import p208model from "../../images/p208model.jpg";
import p208model2 from "../../images/p208model2.jpg";
import p208model3 from "../../images/p208model3.jpg";
import p208model4 from "../../images/p208model4.jpg";
import p208model5 from "../../images/p208model5.jpg";
import p208panel from "../../images/p208panel.jpg";
import p208ekran from "../../images/p208ekran.jpg";
import p208intel from "../../images/p208intel.jpg";
import p208left from "../../images/208left.jpg";
import p208right from "../../images/208right.jpg";
import p208motor from "../../images/p208motor.jpg";
import p208motor1 from "../../images/p208motor1.jpg";
import p208motor2 from "../../images/p208motor2.jpg";
import p208motor3 from "../../images/p208motor3.jpg";
import p208tech from "../../images/p208tech.jpg";
import p208cover1 from "../../images/p208cover1.png";
import p208cover2 from "../../images/p208cover2.png";
import p208ndihma from "../../images/p208ndihma.jpg";
import p208kendi from "../../images/p208kendi.jpg";
import p208korsi from "../../images/p208korsi.jpg";
import p208g1 from "../../images/p208g1.jpg";
import p208g2 from "../../images/p208g2.jpg";
import p208g3 from "../../images/p208g3.jpg";
import p208g4 from "../../images/p208g4.jpg";
import p208g5 from "../../images/p208g5.jpg";
import p208gmotor from "../../images/p208gmotor.jpg";
import p208ge from "../../images/p208ge.jpg";
import p208gtel from "../../images/p208gtel.jpg";
import p208b1 from "../../images/p208b1.jpg";
import p208b2 from "../../images/p208b2.jpg";
import p2008 from "../../images/p2008cover.jpg";
import p208ulse from "../../images/p208ulse.jpg";
import park2008 from "../../images/park2008.jpg";
import p2008girl from "../../images/p2008girl.jpg";
import p2008above from "../../images/p2008above.jpg";
import p2008city from "../../images/p2008city.jpg";
import p2008fener from "../../images/p2008fener.jpg";
import p2008bagazh from "../../images/p2008bagazh.jpg";
import p2008drita from "../../images/p2008drita.jpg";
import p2008tavan from "../../images/p2008tavan.jpg";
import p2008ulse from "../../images/p2008ulse.jpg";
import p2008hapsira from "../../images/p2008hapsira.jpg";
import p2008left from "../../images/p2008left.jpg";
import p2008right from "../../images/p2008right.jpg";
import p2008inst from "../../images/p2008inst.jpg";
import p2008gisht from "../../images/p2008gisht.jpg";
import p2008audio from "../../images/p2008audio.jpg";
import p2008man from "../../images/p2008man.jpg";
import p2008mot1 from "../../images/p2008mot1.jpg";
import p2008mot2 from "../../images/p2008mot2.jpg";
import p2008mot3 from "../../images/p2008mot3.jpg";
import p2008mot4 from "../../images/p2008mot4.jpg";
import p2008mot5 from "../../images/p2008mot5.jpg";
import p2008desert from "../../images/p2008desert.jpg";
import p2008park from "../../images/p2008park.jpg";
import p2008coveri1 from "../../images/p2008coveri1.jpg";
import p2008coveri2 from "../../images/p2008coveri2.jpg";
import p2008coveri3 from "../../images/p2008coveri3.jpg";
import p2008tech from "../../images/p2008tech.jpg";
import p2008sherbim from "../../images/p2008sherbim.jpg";
import p2008vid from "../../images/p2008vid.mp4";
import p2008vid4 from "../../images/p2008vid4.mp4";
import p2008vid5 from "../../images/p2008vid5.mp4";
import p2008vid6 from "../../images/p2008vid6.mp4";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional
import ReactCompareImage from "react-compare-image";
import $ from "jquery";
window.$ = $;

export default class Peugeot2008 extends Component {
  componentDidMount() {
    // mobile
    if (window.innerWidth < 992) {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().top;
      var elementPosition2 = document
        .getElementById("galeria")
        .getBoundingClientRect().left;

      //top offset of static/fixed div
      var staticOffset = $("#myHeader2").offset().top;
      //window height
      var wpHeight = $(window).height();

      //point when the user scrolls at the bottom of div
      //static/fixed div height + top offset - viewport height
      var treshold = staticOffset + $("#myHeader2").height() + wpHeight;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
          // bot
          $("#myHeader2").addClass("stickyb");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition2 + treshold + 3500) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }
    // desktop
    else {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().left;
      var elementPosition2 = document
        .getElementById("footer-content")
        .getBoundingClientRect().top;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition) {
          // bot
          $("#myHeader2").addClass("stickyb");
        }
        if ($(window).scrollTop() > elementPosition2 + 2300) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }
    $(function () {
      $(".fixed-menu .row p").click(function () {
        // remove classes from all
        $(".fixed-menu .row p").removeClass("current");
        // add class to the one we clicked
        $(this).addClass("current");
      });
    });

    $("#dizejno .overlay-foto").click(function () {
      $("html, body").animate(
        {
          scrollTop: $("#dizejno #accordion").offset().top - 400,
        },
        1000
      );
    });

    $("#pozicioni .overlay-foto").click(function () {
      $("html, body").animate(
        {
          scrollTop: $("#pozicioni #accordion2").offset().top - 400,
        },
        1000
      );
    });

    $(".scrollTo").on("click", function (e) {
      e.preventDefault();
      var target = $(this).attr("href");
      $("html, body").animate(
        {
          scrollTop: $(target).offset().top - 200,
        },
        1000
      );
    });

    $("#p2008-vid").bind("play", function (e) {
      $(".legal-notice").toggleClass("permas");
    });
    $("#p2008-vid").bind("pause", function (e) {
      $(".legal-notice").toggleClass("permas");
    });
  }
  // pauseVideo = () => {
  //   // Pause as well
  //   this.refs.vidRef.pause();
  //   this.refs.vidRef2.pause();
  // };

  render() {
    return (
      <section className="car">
        <div id="bottom-nav">
          <h2 className="nav-title">2008 SUV</h2>
          <div aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">FAQJA KRYESORE</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/automjete">GAMA</Link>
              </li>
              <li className="breadcrumb-item" aria-current="page">
                2008
              </li>
            </ol>
          </div>
        </div>
        <video
          id="p2008-vid"
          className="car-vid img-fluid"
          poster={p2008}
          controls
        >
          <source src={p2008vid} type="video/mp4"></source>
        </video>
        <p class="legal-notice" style={{ bottom: "133px" }}>
          * Modelet e paraqitura janë vetëm për qëllime ilustrative. Peugeot
          rezervon të drejtën për të bërë ndryshime.
        </p>
        <div className="fixed-menu" id="myHeader" style={{ width: "100%" }}>
          <div className="row m-0 flex-nowrap">
            <p className="p-3 m-0 col-md-6 text-center current flex-nowrap">
              PEUGEOT E RE 2008 SUV
            </p>
            <div className="p-3 m-0 col-md-6 text-center flex-nowrap">
              <Link to="/peugeot-e-208">
                <p className="m-0">PEUGEOT E RE E-2008 SUV</p>
              </Link>
            </div>
          </div>
          <ul className="table-responsive">
            <a href="#dizejno" className="scrollTo">
              <li>DIZEJNO</li>
            </a>
            <a href="#cockpit" className="scrollTo">
              <li>i-COCKPIT</li>
            </a>
            <a href="#motori" className="scrollTo">
              <li>MOTORI</li>
            </a>
            <a href="#teknologjia" className="scrollTo">
              <li>TEKNOLOGJIA</li>
            </a>
            <a href="#sherbimet" className="scrollTo">
              <li>SHËRBIMET</li>
            </a>
            <a href="#galeria" className="scrollTo">
              <li>GALERIA</li>
            </a>
          </ul>
        </div>

        <div className="content">
          <p className="text-center">
            Largohuni prej të zakonshmet me PEUGEOT 2008 SUV, i dizenjuar për
            stilin tuaj të jetës: me PEUGEOT 3D i-Cockpit®, teknologjinë e
            gjeneratës tjetër dhe një përzgjedhje të motorëve të fuqishëm me
            djegie të brendshme, efikase në konsum si dhe motorëve elektrikë.
          </p>
          <div id="content-butona" className="text-center pt-4">
            <button className="more-btn mb-3">KËRKONI NJË TEST DRIVE</button>
            <button className="more-btn mb-3">
              KËRKONI OFERTË PËR AUTOMJET
            </button>
          </div>
        </div>

        <div id="dizejno">
          <h2 className="text-center p-5">DIZENJO</h2>
          <Splide
            options={{
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              pagination: false,
            }}
          >
            <SplideSlide>
              <img
                className="img-fluid"
                data-splide-lazy={park2008}
                alt="Image 1"
              />
            </SplideSlide>
            <SplideSlide>
              <img
                className="img-fluid"
                data-splide-lazy={p2008girl}
                alt="Image 2"
              />
            </SplideSlide>
            <SplideSlide>
              <img
                className="img-fluid"
                data-splide-lazy={p2008above}
                alt="Image 3"
              />
            </SplideSlide>
            <SplideSlide>
              <img
                className="img-fluid"
                data-splide-lazy={p2008city}
                alt="Image 5"
              />
            </SplideSlide>
          </Splide>
          <p className="pt-5 pb-5">
            *SUV GODITËS. Me linjat e saj të lemuara dhe të fuqishme, PEUGEOT e
            re 2008 SUV ofron fuqi dhe efikasitet mjeshtëror. Grila e saj e
            mrekullueshme, kofano horizontale dhe më e lartë, përmirësojnë
            dizenjon e sofistikuar dhe unike. Në pjesën e prapme, spoiler,
            shirit i zi me shkëlqim dhe shufra kromi{" "}
            <Tippy content="Standart,opsional, ose i padisponueshëm sipas versionit">
              <span className="isTip">i</span>
            </Tippy>{" "}
            , afirmojnë personalitetin e tij sportiv.
          </p>
          <div className="row m-0">
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseOne"
              aria-expanded="true"
              aria-controls="collapseOne"
            >
              <div className="titull">
                <h5>FENERËT</h5>
              </div>
              <img className="w-100 pr-1 " src={p2008fener} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
            >
              <div className="titull">
                <h5>BAGAZHI</h5>
              </div>
              <img
                className="w-100 pl-0 pr-0 pl-lg-1 pr-lg-1"
                src={p2008bagazh}
              />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseThree"
              aria-expanded="true"
              aria-controls="collapseThree"
            >
              <div className="titull">
                <h5>DRITAT E PASME</h5>
              </div>
              <img className="w-100 pl-0 pl-md-1" src={p2008drita} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
          </div>
          <div className="row pt-1 m-0">
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseFour"
              aria-expanded="true"
              aria-controls="collapseFour"
            >
              <div className="titull">
                <h5>TAVAN PANORAMIK ME HAPJE</h5>
              </div>
              <img className="img-fluid  pr-1 pb-md-0 pb-1" src={p2008tavan} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseFive"
              aria-expanded="true"
              aria-controls="collapseFive"
            >
              <div className="titull">
                <h5>HAPËSIRA E BRENDSHME</h5>
              </div>
              <img className="img-fluid  pr-1 pl-1" src={p2008hapsira} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseSix"
              aria-expanded="true"
              aria-controls="collapseSix"
            >
              <div className="titull">
                <h5>HAPËSIRA E BRENDSHME</h5>
              </div>
              <img className="img-fluid  pl-lg-1 pl-0" src={p2008ulse} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
          </div>
          {/* accordion 1 */}
          <div id="accordion">
            <div class="card">
              <div
                id="collapseOne"
                class="collapse"
                aria-labelledby="headingOne"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">NJË PRANI IMPONUESE</h2>
                  Me dizenjon e jashme të mrekullueshëme, SUV i ri PEUGEOT 2008
                  shfaq një prani imponuese. Kofano horizontale plotësohet nga
                  një grilë kromi e zbukuruar me nënshkrimin emblematik të
                  markës në formën e tre kthetrave të luanit dhe fenerëve FULL
                  LED{" "}
                  <Tippy content="Dritat Full LED: Dritat diodë përdorin më pak energji dhe sigurojnë ndriçim më të fuqishëm. Në dispozicion në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  që theksojnë pamjen bashkëkohore dhe të sofistikuar.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseTwo"
                class="collapse"
                aria-labelledby="headingTwo"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">
                    SPACIOZ DHE I LEHTË PËR TU PËRDORUR
                  </h2>
                  Me një prag të ulët dhe me hapje të gjerë, hapësira e bagazhit
                  e kombinuar me sediljen e pasme të palosshme 1/3 - 2/3{" "}
                  <Tippy content="Në varësi të nivelint të pajisjeve dhe llojit të motorit, vëllimi i bagazhit mund të shkojë nga 434 litra në 1,044 litra në standarti VDA kur sedilja e pasme është e palosur.">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  ofron hapësirë të bollshme funksionale. Bagazhi modular me dy
                  nivele, në pozicionin e lartë, ju lejon të ngarkoni dhe
                  shkarkoni sendet shpejt dhe lehtë dhe siguron vëllim edhe më
                  të madh në pozicionin më të ulët. Dhe, nuk ka absolutisht
                  asnjë kompromis për cilindo nivel që ju zgjidhni: bagazhi ka
                  të njëjtin kapacitet në të gjithë gamën.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseThree"
                class="collapse"
                aria-labelledby="headingThree"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">EKSELENCË NË ÇDO DETAJ</h2>
                  Pamja e pjesës së pasme të PEUGEOT 2008 SUV, me rripin e saj
                  të zi me shkëlqim{" "}
                  <Tippy content="Disponueshmëria varet nga lloji i motorit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , zbukurohet nga një spoiler dhe nga një marmitë kromi e
                  dyfishtë, e cila plotëson në mënyrë të përkryer formën e saj.
                  Përveç dritave të pasme LED, 3 fenerët e kthetrave të luanit
                  ndriçojnë ditën dhe natën, duke theksuar fuqinë teknologjike
                  dhe dukshmërinë e automjetit.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseFour"
                class="collapse"
                aria-labelledby="headingFour"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">PAMJE ME KËND TË GJERË</h2>
                  Tavani panoramik{" "}
                  <Tippy content="Në dispozicion si opsion, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  në PEUGEOT 2008 SUV siguron shumë dritë natyrale në pjesen e
                  pasagjerëve. Të gjithë pasagjerët, përfshirë ata në vendet e
                  pasme, mund të shijojne plotësisht panoramën nëpërmjet tavanit
                  prej xhami.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseFive"
                class="collapse"
                aria-labelledby="headingFive"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">HAPËSIRË E RIMODELUAR</h2>
                  <p>
                    Brendësia është maksimalizuar posaçërisht në pjesën e pasme
                    për të siguruar më shumë rehati dhe më shumë hapësirë për të
                    gjithë pasagjerët tuaj, së bashku me ndarje të ndryshme
                    magazinimi të pozicionuara në mënyrë strategjike në pjesën e
                    pasagjerëve.
                  </p>
                  <p>
                    Për më tepër, admironi cilësinë e izolimit të zërit të
                    krahasueshëm me atë të makinave të pasagjerëve.
                  </p>
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseSix"
                class="collapse"
                aria-labelledby="headingSix"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">KOMFORTI I BRENDSHËM</h2>
                  Uluni të qetë në vendet ergonomike me linja dinamike{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  në PEUGEOT 2008 SUV e ri. Sediljet e pasagjerëve në vendet e
                  para ofrojnë opsionin e ngrohjes{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , gjithashtu ofrojnë funksion masazhi dhe mund të kontrollohen
                  në mënyrë elektronike{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm në varësi të modelit. Funksioni i masazhit dhe kontrolli elektrik janë të disponueshëm vetëm për sediljen e shoferit">
                    <span className="isTip">i</span>
                  </Tippy>
                  .
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="cockpit">
          <h2 className="text-center pt-5 pb-1">3D i-COCKPIT</h2>
          <p className="text-center pb-2">
            EKSPERIENCA E DREJTIMIT E RISHPIKUR
          </p>
          <ReactCompareImage leftImage={p2008left} rightImage={p2008right} />
          <p className="pr-2 pl-2 pl-md-0 pt-3">
            Provoni një eksperiencë të re drejtimi me PEUGEOT 3D i-Cockpit®{" "}
            <Tippy content="Standart ose i padisponueshëm në varësi të modelit">
              <span className="isTip">i</span>
            </Tippy>
            ergonomi intuitive, timon kompakt, ekran 10” HD e konfigurueshme{" "}
            <Tippy content="Standart ose i padisponueshëm në varësi të modelit">
              <span className="isTip">i</span>
            </Tippy>
            me panel kontrolli 3D dixhital{" "}
            <Tippy content="Standart ose i padisponueshëm në varësi të modelit">
              <span className="isTip">i</span>
            </Tippy>
            . E pajisur me hapje të dyerve pa e prekur dorezën, ndezjen e
            automjetit me buton duke patur celësin në xhep{" "}
            <Tippy content="Open and Go (Hapeni derën pa çelës dhe ndizni automjetin me butonin Start/Stop). Standart, opsion, ose i padisponueshëm në varësi të modelit">
              <span className="isTip">i</span>
            </Tippy>{" "}
            ju jep ndjesinë e zotërimit të teknologjisë më të fundit. Mund edhe
            të harroni të mbyllni derën, teknologjia ju jep sigurinë duke e
            mbyllur automatikisht vetë sapo të largoheni nga automjeti më
            celësin në xhep, shoqëruar me një sinjal akustik. Futuni në bordin e
            PEUGEOT e-2008 SUV dhe shijo atë cfarë të ofron PEUGEOT 3D
            i-Cockpit. Optimizoni autonominë e baterisë me frenat rigjeneruese
            dhe merrni detajet në kohe reale sesi po operon sistemi elektrik.
          </p>

          <div id="motor-deck">
            <div className="card-deck m-0">
              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p2008inst}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>PËR NJË REAGIM MË TË SHPEJTË</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Paneli dixhital 3D{" "}
                    <Tippy content="Standart ose i padisponueshëm në varësi të modelit">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    shfaq të gjitha informacionet e drejtimit që nevojiten nën
                    fushë-pamjen e drejtuesit, sic është sistemi i navigimit dhe
                    sistemet e ndihmës së drejtimit, e vendosur në dy nivele.
                    Informacionet më të rëndësishme, sic është shpejtësia dhe
                    sinjalet e sigurisë, shfaqen në formë hologrami në menyrë që
                    të tërheqë sa më shpejt vëmendjen e drejtuesit.
                  </p>
                </div>
              </div>

              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p2008gisht}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>SUV ULTRA - INTELIGJENT</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Falë navigimit 3D me sistemin e trafikut TomTom®{" "}
                    <Tippy content="Shërbimet TomTom® ju lejojnë të shfaqni të gjitha informacionet e nevojshme në lidhje me drejtimin në kohë reale, (trafiku në kohë reale, çmimi i karburantit, parkimi, kushtet e motit, kërkimet lokale). Të gjitha këto shërbime ofrohen për një periudhë 3 vjeçare.">
                      <span className="isTip">i</span>
                    </Tippy>
                    , ju merrni informacione rreth trafikut, kohës së udhëtimit
                    dhe informacioneve me karakter risku në kohë reale. Qëndroni
                    lidhur duke shfaqur në ekranin me prekje aplikacionet tuaja
                    të preferuara nga smartphone juaj nëpërmjet Mirror Screen{" "}
                    <Tippy content="E disponueshme si standarde, tipari i Mirror Screen ju lejon të shikoni aplikacionet nga smartphone tuaj në ekranin me prekje. Funksioni i Mirror Screen funksionon përmes Android Auto (për telefonat Android), Apple CarPlay(për telefonat iOS), ose me MirrorLink (për telefonat inteligjentë Android të pajtueshëm me MirrorLink), me kusht që të keni internet në celularin tuaj. Vetëm këto tre aplikacione specifike do të funksionojnë kur automjeti është ndalur, ose në lëvizje. Kur drejtoni automjetin, disa veçori do të çaktivizohen. Mësoni më shumë në faqen e PEUGEOT në vendin tuaj.">
                      <span className="isTip">i</span>
                    </Tippy>
                    . Smartphone juaj ndërkohë karikohet{" "}
                    <Tippy content="Karikimi pa tel me induksion magnetik. Standart, ose opsion në varësi të versionit">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    në vendin afër jush në qendër të kroskotit. Gjithashtu
                    ofrohen edhe portat USB{" "}
                    <Tippy content="Deri në 4 porte USB">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    si për pasagjerët para edhe për ato prapa.
                  </p>
                </div>
              </div>
              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p2008audio}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>SISTEMI AUDIO FOCAL® - TINGUJ PERFEKT</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Lejoni muzikën tuaj të preferuar që të mbushë hapësirën në
                    bordin e PEUGEOT 2008 SUV, duke iu ofruar një tingull
                    gjallërues të gjeneruar prej sistemit ekskluziv premium
                    Hi-Fi Focal®{" "}
                    <Tippy content="Focal është një markë franceze, një lider botëror në prodhimin e sistemeve audio me cilësi të lartë. Në dispozicion në varësi të versionit dhe llojit të motorit">
                      <span className="isTip">i</span>
                    </Tippy>
                    . 10 altoparlant - 1 në qendër, 4 altoparlantë të vegjël, 4
                    të tjerë me volum të ulët dhe 1 në pjesën e pasme – të
                    shperndare në të gjithë pjesën interiore, për të dhënë një
                    tingull kompleks dhe dinamik, do t'ju bëjë të ndiheni sikur
                    jeni në një skenë muzike, duke tejkaluar limitet e Hi-Fi.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="motori">
          <h2 className="text-center pt-5 pb-1">МОТОRËT</h2>
          <p className="text-center pb-2">LIRIA E ZGJEDHJES</p>
          <img className="pb-3 img-fluid" src={p2008man}></img>
          <p className="pl-2 pl-lg-0">
            Kënaqni dëshirat tuaja falë lirisë për të zgjedhur motorin që ju
            përshtatet:
          </p>

          <ul className="p2008 pl-2 pl-lg-0">
            <li>
              <p>
                Me modelin elektrik, shijoni udhëtimin kudo që të doni duke
                përfshirë vendet me trafik të kufizuar.
              </p>
            </li>
            <li>
              <p>
                Me modelet benzinë dhe naftë ju mund të zgjidhni gamën e
                motorëve me performancë të lartë dhe efikase Euro 6{" "}
                <Tippy content="Standardi i ri i emetimit Euro 6 (EURO 6d-TEMP-EVAP-ISC) i cili hyri në fuqi më 1 shtator 2019">
                  <span className="isTip">i</span>
                </Tippy>{" "}
                të pajisur me Stop & Start{" "}
                <Tippy content="Fikje automatike dhe sistem i ndezjes së motorit për të kursyer karburant dhe për të reduktuar ndotjen">
                  <span className="isTip">i</span>
                </Tippy>
                .
              </p>
            </li>
          </ul>

          <div className="row pt-5">
            <div className="col-lg-3 col-12 pr-0">
              <div
                className="nav flex-column nav-pills"
                id="v-pills-tab"
                role="tablist"
                aria-orientation="vertical"
              >
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center active"
                  id="v-pills-home-tab"
                  data-toggle="pill"
                  href="#v-pills-home"
                  role="tab"
                  aria-controls="v-pills-home"
                  aria-selected="true"
                  onClick={this.pauseVideo}
                >
                  MOTOR ELEKTRIK <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-profile-tab"
                  data-toggle="pill"
                  href="#v-pills-profile"
                  role="tab"
                  aria-controls="v-pills-profile"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  MOTOR BENZINE <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-messages-tab"
                  data-toggle="pill"
                  href="#v-pills-messages"
                  role="tab"
                  aria-controls="v-pills-messages"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  MOTOR NAFTE <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-settings-tab"
                  data-toggle="pill"
                  href="#v-pills-settings"
                  role="tab"
                  aria-controls="v-pills-settings"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  TRANSMISION AUTOMATIK{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-korsia-tab"
                  data-toggle="pill"
                  href="#v-pills-korsia"
                  role="tab"
                  aria-controls="v-pills-korsia"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  TRANSMISION MANUAL
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-9 col-12 pl-0">
              <div className="tab-content" id="v-pills-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="v-pills-home"
                  role="tabpanel"
                  aria-labelledby="v-pills-home-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p2008mot1} />
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">DREJTIM ELEKTRIK</p>

                      <p>
                        I përshtatshëm, i lehtë për t'u perdorur në qytet,i
                        qëndrueshëm dhe i sigurt në rrugë, SUV i ri PEUGEOT
                        e-2008 është i pajisur me një motor elektrik 100 kW (136
                        kf). Ky motor jep përshpejtimin e menjëhershëm me 260
                        Nm. Ne bordin e SUV-it të ri PEUGEOT e-2008 do të
                        shijoni ngarje të rehatshme, dinamike dhe të qetë.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-profile"
                  role="tabpanel"
                  aria-labelledby="v-pills-profile-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p2008mot2} />
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">
                        PURETECH: PERFORMANCË DHE EFIÇIENCË
                      </p>

                      <p>
                        SUV i ri PEUGEOT 2008 është i pajisur me gjeneratën e
                        fundit të motorëve me benzinë Euro 6{" "}
                        <Tippy content="Standarti i ri i emetimit Euro 6 (EURO 6d-TEMP-EVAP-ISC) në fuqi nga 1 Shtator 2019">
                          <span className="isTip">i</span>
                        </Tippy>
                        , PureTech, duke ofruar ekonomi më të mirë të
                        karburantit, kënaqësi drejtimi dhe performancë të
                        shkëlqyeshme.
                      </p>

                      <p>4 motorë benzine janë në dispozicion:</p>
                      <ul className="p2008">
                        <li>
                          <p>1,2L PureTech 100 S&S BVM6</p>
                        </li>
                        <li>
                          <p>1,2L PureTech 130 S&S BVM6</p>
                        </li>
                        <li>
                          <p>1,2L PureTech 130 S&S EAT8</p>
                        </li>
                        <li>
                          <p>1,2L PureTech 155 S&S EAT8</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-messages"
                  role="tabpanel"
                  aria-labelledby="v-pills-messages-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p2008mot3} />
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">
                        BLUE HDi: TEKNOLOGJI EKSKLUZIVE
                      </p>

                      <p>
                        Motorët naftë Euro 6{" "}
                        <Tippy content="Standarti i ri i emetimit Euro 6 (EURO 6d-TEMP-EVAP-ISC) në fuqi nga 1 Shtator 2019">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        të ofruar në SUV PEUGEOT 2008 janë të pajisura me
                        teknologji ekskluzive BlueHDi{" "}
                        <Tippy content="E zhvilluar nga PSA Group, teknologjia BlueHDi përdor sisteme SCR (Reduktimi Katalitik Selektiv) dhe sistemet AdBlue. * Shndërron 90% të NOx në avujt e ujit (H2O) dhe azot (N2).">
                          <span className="isTip">i</span>
                        </Tippy>
                        , duke bërë të mundur kombinime me performanca të larta
                        me emetime të ulëta.
                      </p>

                      <p>2 motorë naftë janë në dispozicion:</p>
                      <ul className="p2008">
                        <li>
                          <p>1,5L BlueHDi 100 S&S BVM6</p>
                        </li>
                        <li>
                          <p>1,5L BlueHDi 130 S&S EAT8</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-settings"
                  role="tabpanel"
                  aria-labelledby="v-pills-settings-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p2008mot4} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">
                        EAT8: EFIÇIENT DHE I SHPEJTËI
                      </p>

                      <p>
                        Ekonomik si një transmision manual, transmisioni i ri
                        automatik EAT8{" "}
                        <Tippy content="EAT8 (Transmetimi automatik efiçient 8) - Në varësi të modelit dhe motorit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        lejon zhvendosjen e shpejtë dhe të saktë falë pajisjes
                        "Shift and Park by wire"{" "}
                        <Tippy content="Levë ingranazhesh elektrike">
                          <span className="isTip">i</span>
                        </Tippy>
                      </p>

                      <p>
                        Kompakte dhe e kombinuar me teknologjinë "Stop & Start",
                        optimizon konsumin dhe efikasitetin e karburantit.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-korsia"
                  role="tabpanel"
                  aria-labelledby="v-pills-korsia-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p2008mot5} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">
                        TRANSMISION MANUAL ME 6 RAPORTE SHPEJTËSIE
                      </p>

                      <p>
                        PEUGEOT E RE 2008 SUV mund të pajiset edhe me
                        transmision manual me 6 raporte shpejtësie BVM6:
                      </p>

                      <ul className="p2008">
                        <li>
                          <p>1,2L PureTech 100 S&S BMV6</p>
                        </li>
                        <li>
                          <p>1,2L PureTech 130 S&S BMV6</p>
                        </li>
                        <li>
                          <p>1,5L BlueHDi 100 S&S BVM6</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="teknologjia">
          <h2 className="text-center pt-5 pb-1">TEKNOLOGJIA</h2>
          <p className="text-center pb-2">SUV I TË ARDHMES</p>
          <img className="pb-3 img-fluid" src={p2008tech}></img>
          <p className="pl-2 pl-lg-0">
            Për një siguri më të mirë, ndihmë në manovrim dhe parashikim më të
            mirë të mjedisit, SUV i ri PEUGEOT 2008 është i pajisur me
            gjeneratën e fundit të teknologjisë i dhe ndihmës në drejtuese{" "}
            <Tippy content="Standart,opsion, ose i padisponueshëm në varësi të modelit">
              <span className="isTip">i</span>
            </Tippy>
            .
          </p>

          <Splide
            id="pershtat-slide"
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              pagination: true,
            }}
          >
            <SplideSlide className="d-flex pt-4 align-self-baseline">
              <video poster={p2008coveri1} controls>
                <source src={p2008vid4} type="video/mp4"></source>
              </video>
              <div className="pl-2">
                <b>SHIJONI NJË EKSPERIENCË GJYSMË AUTONOME</b>
                <br />
                <br />
                <p>
                  SUV i ri PEUGEOT 2008 është i pajisur me teknologjinë më të
                  fundit të markës, siç është PEUGEOT Drive Assist Plus
                  <Tippy content="Standart,opsion, ose i padisponueshëm në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>
                  e cila ju siguron një përvojë gjysmë-autonome.
                </p>
                <p>
                  Me sistemin e pozicionimit në rrugë
                  <Tippy content="Sistemi mban pozicionin në korsinë e zgjedhur nga shoferi">
                    <span className="isTip">i</span>
                  </Tippy>
                  dhe me Adaptive Cruise Control e pajisur me Stop &amp; Go
                  <Tippy content="Sistemi përdor radar në mes të parakolpit të përparmë dhe një aparat fotografik në majë të xhamit të përparmë.">
                    <span className="isTip">i</span>
                  </Tippy>
                  , rregullon automatikisht shpejtësinë sipas makinës përpara
                  jush, duke mbajtur një distancë të sigurt.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4 align-self-baseline">
              <img
                className="img-fluid col-md-6"
                data-splide-lazy={p2008desert}
                alt="Image 2"
              />
              <div className="pl-2 pt-2 ">
                <b>UDHËTONI PËRTEJ KUSHTEVE TË RRUGËS</b>
                <br />
                <br />
                <p>
                  Përshtani SUV-in tuaj PEUGEOT 2008 sipas kushteve të motit dhe
                  rrugës me Advance Grip Control
                  <Tippy content="Sistemi i avancuar i menaxhimit. Në dispozicion si opsion në varësi të versionit dhe llojit të motorit">
                    <span className="isTip">i</span>
                  </Tippy>
                  duke zgjedhur një nga tre opsionet: rërë, baltë dhe borë.
                </p>
                <p>
                  Në rrugë me pjerrësi, opsioni Hill Assist Descent Control
                  <Tippy content="Në dispozicion si opsion në varësi të versionit dhe llojit të motorit">
                    <span className="isTip">i</span>
                  </Tippy>
                  ndihmon në rregullimin e shpejtësisë.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4 align-self-baseline">
              <video poster={p2008coveri2} controls>
                <source src={p2008vid5} type="video/mp4"></source>
              </video>
              <div className="pl-2 pt-2 ">
                <b>SIGURIMI OPTIMAL</b>
                <br />
                <br />
                <p>
                  Për sigurinë maksimale gjatë drejtimit, ekzistojnë një sërë
                  teknologjish{" "}
                  <Tippy content="Standart,opsion, ose i padisponueshëm në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  e cila do t'ju ndihmojë:
                  <ul class="p2008 pt-3">
                    <li>
                      <p>Monitorimi i këndit të vdekur</p>
                    </li>
                    <li>
                      <p>
                        ‘Active Safety Brake’
                        <span
                          class="has-tip infobulle tooltipstered"
                          aria-haspopup="true"
                          data-tooltip=""
                        >
                          i
                        </span>
                        ,
                      </p>
                    </li>
                    <li>
                      <p>
                        ‘Distance Alert’
                        <span
                          class="has-tip infobulle tooltipstered"
                          aria-haspopup="true"
                          data-tooltip=""
                        >
                          i
                        </span>
                        ,
                      </p>
                    </li>
                    <li>
                      <p>
                        Sistemi i paralajmërimit për kalimin pa dashje të
                        korsisë,
                      </p>
                    </li>
                    <li>
                      <p>Aktivizimi automatik i dritave të gjata,</p>
                    </li>
                    <li>
                      <p>
                        Sistemi i njohjes së kufirit dhe rekomandimet të
                        shpejtësisë...
                      </p>
                    </li>
                  </ul>
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4 align-self-baseline">
              <img
                className="img-fluid"
                data-splide-lazy={p2008park}
                alt="Image 4"
              />
              <div className="pl-2 pt-2">
                <b>PARKIM PERFEKT </b>
                <br />
                <br />
                <p>
                  Lehtësoni hyrjen dhe daljen nga hapësira e parkimit duke
                  aktivizuar teknologjinë ‘Full Park Assist’
                  <Tippy content="Ndihmë aktive në parkim paralel dhe të zhdrejtë. Një opsion që do të jetë në dispozicion nga viti 2020, ose i padisponueshëm në varësi të modelit">
                    <span className="isTip">i</span>
                  </Tippy>
                  përmes levës së ingranazheve.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4 align-self-baseline">
              <video poster={p2008coveri3} controls>
                <source src={p2008vid6} type="video/mp4"></source>
              </video>
              <div className="pl-2 ">
                <b>DREJTONI PEUGEOT E RE ME 3D i-COCKPIT®</b>
                <br />
                <p className="pt-4">
                  Shijoni ergonominë dhe estetikën në PEUGEOT 3D i-Cockpit®.{" "}
                  <br /> Zbuloni të gjitha opsionet në dispozicion për ju, si
                  për modelin me motorët me djegie të brendshme, ashtu edhe për
                  modelin elektrik:
                </p>
                <ul class="p2008">
                  <li>
                    <p>TIMON KOMPAKT</p>
                  </li>
                  <li>
                    <p>
                      Ekran me prekje 10” HD{" "}
                      <Tippy content="Standart ose i padisponueshëm në varësi të modelit">
                        <span className="isTip">i</span>
                      </Tippy>
                    </p>
                  </li>
                  <li>
                    <p>
                      Paneli i kontrollit dixhital 3D i rregullueshëm
                      <Tippy content="Standart,opsion, ose i padisponueshëm në varësi të modelit">
                        <span className="isTip">i</span>
                      </Tippy>{" "}
                      me dy nivele të leximit të informacionit.
                    </p>
                  </li>
                  <li>
                    <p>
                      Vend për karikimin e smartphone tuaj
                      <Tippy content="Karikimi wireless me induksion magnetik. Standart, ose opsion në varësi të versionit">
                        <span className="isTip">i</span>
                      </Tippy>
                    </p>
                  </li>
                  <li>
                    <p>
                      Navigim me 3D TomTom® Traffic
                      <Tippy content="Shërbimet TomTom® ju lejojnë të shfaqni të gjitha informacionet e nevojshme në lidhje me drejtimin në kohë reale, (trafiku në kohë reale, çmimi i karburantit, parkimi, kushtet e motit, kërkimet lokale). Të gjitha këto shërbime ofrohen për një periudhë 3 vjeçare.">
                        <span className="isTip">i</span>
                      </Tippy>
                      <em></em>
                    </p>
                  </li>
                  <li>
                    <p>
                      Mirror screen
                      <em>
                        <Tippy content="E disponueshme si standarde, tipari i Mirror Screen ju lejon të shikoni aplikacionet nga smartphone tuaj në ekranin me prekje. Funksioni i Mirror Screen funksionon përmes Android Auto (për telefonat Android), Apple CarPlay (për telefonat iOS), ose me MirrorLink (për telefonat inteligjentë Android të pajtueshëm me MirrorLink), me kusht që të keni internet në celularin tuaj. Vetëm këto tre aplikacione specifike do të funksionojnë kur automjeti është ndalur, ose në lëvizje. Kur drejtoni automjetin, disa veçori do të çaktivizohen. Mësoni më shumë në faqen e PEUGEOT në vendin tuaj. ">
                          <span className="isTip">i</span>
                        </Tippy>
                      </em>
                    </p>
                  </li>
                  <li>
                    <p>
                      Informacione të hollësishme për drejtimin dhe më shumë ...
                    </p>
                  </li>
                </ul>
              </div>
            </SplideSlide>
          </Splide>
        </div>

        <div id="sherbime">
          <h2 className="text-center pt-5 pb-1">SHËRBIME TË LIDHURA</h2>
          <p className="text-center pb-2">
            PËRMIRËSONI PËRVOJËN TUAJ ME PEUGEOT
          </p>
          <img className="pb-3 img-fluid" src={p2008sherbim}></img>

          <p className="pl-2 pl-lg-0">
            Me disponueshmërinë e më shumë shërbimeve, përfitimet janë të
            pafundme.
          </p>
        </div>

        <div id="galeria">
          <h2 className="text-center pt-5 pb-1">GALERIA</h2>
          <Splide
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              // height: "360px",
              // autoplay: true,
              pagination: true,
            }}
          >
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208g1} />
                  <img className="w-100" src={p208g2} />
                  <img className="w-100" src={p208g3} />
                  <img className="w-100" src={p208g4} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208g5}
                />
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <img
                  className="img-fluid col-lg-6 pl-0 pr-1"
                  src={p208gmotor}
                />
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208ge} />
                  <img className="w-100" src={p208gtel} />
                  <img className="w-100" src={p208g4} />
                  <img className="w-100" src={p208motor3} />
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208b1} />
                  <img className="w-100" src={p208ulse} />
                  <img className="w-100" src={p208b2} />
                  <img className="w-100 h-100" src={p208right} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208tech}
                />
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208b1} />
                  <img className="w-100" src={p208ulse} />
                  <img className="w-100" src={p208b2} />
                  <img className="w-100 h-100" src={p208right} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208tech}
                />
              </div>
            </SplideSlide>
          </Splide>
          <div className="pl-2 pt-5 pl-xl-0">
            <p>
              * Konsumi i karburantit dhe emetimit të CO<sub>2</sub>
            </p>
            <p>
              <em>
                Të dhënat e vlerësuara që nga Tetori 2019, parashikohen vetëm si
                informacione që ende i nënshtrohen aprovimit të WLTP përveç
                modelit elektrik.
              </em>
            </p>
            <ul class="p2008">
              <li>
                <p>
                  Vlerat WLTP <sup>(1)</sup>: Konsumi i karburantit 4.5 deri 6.2
                  e kombinuar, 4.9 deri 8.1 me shpejtësi të ulët, 4.2 deri në
                  6.2 me shpejtësi mesatare, 3.9 në 5.3 me shpejtësi të lartë,
                  dhe 5 deri në 6 WLTP me shpejtësi shumë të lartë (l/100 km) -
                  emetim i CO
                  <sub>2</sub> (i kombinuar) WLTP: 118 deri në 141 (g / km).
                </p>
              </li>
            </ul>
            <p>
              (1) Konsumi i karburantit dhe vlerat e CO<sub>2</sub> plotësojnë
              çertifikimin WLTP (Rregullorja e BE 2017/948). Nga 1 shtatori
              2018, automjetet e reja pranohen bazuar në standartin WLTP, e cila
              është një proçedurë e re, më realiste për matjen e konsumit të
              karburantit dhe emetimeve të CO<sub>2</sub>. Kjo proçedurë WLTP
              zëvendëson plotësisht ciklin e ri evropian të drejtimit (NEDC),
              një proçedurë e përdorur më parë. Për shkak se kushtet e provës
              ishin më realiste, konsumi i karburantit dhe emetimet e CO
              <sub>2</sub> të matura me procedurën WLTP ishin në shumë raste më
              të larta se ato të matura me procedurën NEDC. Vlera e konsumit të
              karburantit dhe emetimeve të CO<sub>2</sub> mund të ndryshojë
              sipas pajisjeve, opsioneve dhe llojeve të gomave specifike.
              Kontaktoni shitësin tuaj për më shumë informacion ose vizitoni
              faqen e internetit PEUGEOT në vendin tuaj.
            </p>
            <ul class="p2008">
              <li>
                <p>
                  Vlerat e NEDC <sup>(2)</sup>: Konsumi i karburantit nga 3.6 në
                  5 e kombinuar, nga 3.3 në 4.5 në rrugë jashtë qytetit dhe nga
                  4.2 në 5.8 në qytet NEDC (l/100 km ) - CO<sub>2</sub>{" "}
                  (kombinuar) NEDC: 85 deri në 107 (g/km).
                </p>
              </li>
            </ul>
            <p>
              2) Vlerat e konsumit të karburantit dhe emetimet e CO<sub>2</sub>{" "}
              përcaktohen në bazë të standartit WLTP (Rregullorja e BE 2017/948)
              dhe vlerat që rezultojnë konvertohen në NEDC për të lejuar
              krahasimin me automjetet e tjera. Ju lutemi kontaktoni shitësin
              tuaj për më shumë informacion. Vlerat nuk marrin parasysh kushtet
              e përdorimit, stilin e drejtimit, pajisjet ose opsionet dhe mund
              të ndryshojnë sipas llojit të gomës.
            </p>
          </div>
        </div>

        <div className="crumbi mt-4" id="myHeader2">
          <div className="row">
            <div id="test" className="col-lg-4 text-center">
              <Link to="/automjete/test-drive">TEST DRIVE</Link>
            </div>
            <div id="ofert" className="col-lg-8 pl-5">
              <Link to="/automjete/oferte-automjete">
                KËRKONI OFERTË PËR AUTOMJET{" "}
              </Link>
              <i className="fas fa-angle-right pl-3"></i>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
