import React, { Component } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Link } from "react-router-dom";
import pgt208 from "../../images/208gt.jpg";
import p208d from "../../images/p208d.jpg";
import p208d2 from "../../images/p208d2.jpg";
import p208model from "../../images/p208model.jpg";
import p208model2 from "../../images/p208model2.jpg";
import p208model3 from "../../images/p208model3.jpg";
import p208model4 from "../../images/p208model4.jpg";
import p208model5 from "../../images/p208model5.jpg";
import p208panel from "../../images/p208panel.jpg";
import p208ekran from "../../images/p208ekran.jpg";
import p208intel from "../../images/p208intel.jpg";
import p208left from "../../images/208left.jpg";
import p208right from "../../images/208right.jpg";
import p208motor from "../../images/p208motor.jpg";
import p208motor1 from "../../images/p208motor1.jpg";
import p208motor2 from "../../images/p208motor2.jpg";
import p208motor3 from "../../images/p208motor3.jpg";
import p208tech from "../../images/p208tech.jpg";
import p208cover1 from "../../images/p208cover1.png";
import p208cover2 from "../../images/p208cover2.png";
import p208ndihma from "../../images/p208ndihma.jpg";
import p208kendi from "../../images/p208kendi.jpg";
import p208korsi from "../../images/p208korsi.jpg";
import p208g1 from "../../images/p208g1.jpg";
import p208g2 from "../../images/p208g2.jpg";
import p208g3 from "../../images/p208g3.jpg";
import p208g4 from "../../images/p208g4.jpg";
import p208g5 from "../../images/p208g5.jpg";
import p208gmotor from "../../images/p208gmotor.jpg";
import p208ge from "../../images/p208ge.jpg";
import p208gtel from "../../images/p208gtel.jpg";
import p208b1 from "../../images/p208b1.jpg";
import p208b2 from "../../images/p208b2.jpg";
import p208ulse from "../../images/p208ulse.jpg";
import p208vid1 from "../../images/p208vid1.mp4";
import p208vid2 from "../../images/p208vid2.mp4";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional
import ReactCompareImage from "react-compare-image";
import $ from "jquery";
window.$ = $;

export default class Peugeot208 extends Component {
  componentDidMount() {
    // mobile
    if (window.innerWidth < 992) {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().top;
      var elementPosition2 = document
        .getElementById("galeria")
        .getBoundingClientRect().left;

      //top offset of static/fixed div
      var staticOffset = $("#myHeader2").offset().top;
      //window height
      var wpHeight = $(window).height();

      //point when the user scrolls at the bottom of div
      //static/fixed div height + top offset - viewport height
      var treshold = staticOffset + $("#myHeader2").height() + wpHeight;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
          // bot
          $("#myHeader2").addClass("stickyb");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition2 + treshold + 3500) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }
    // desktop
    else {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().left;
      var elementPosition2 = document
        .getElementById("footer-content")
        .getBoundingClientRect().top;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition) {
          // bot
          $("#myHeader2").addClass("stickyb");
        }
        if ($(window).scrollTop() > elementPosition2 + 2300) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }
    $(function () {
      $(".fixed-menu .row p").click(function () {
        // remove classes from all
        $(".fixed-menu .row p").removeClass("current");
        // add class to the one we clicked
        $(this).addClass("current");
      });
    });

    $("#dizejno .overlay-foto").click(function () {
      $("html, body").animate(
        {
          scrollTop: $("#dizejno #accordion").offset().top - 400,
        },
        1000
      );
    });

    $("#pozicioni .overlay-foto").click(function () {
      $("html, body").animate(
        {
          scrollTop: $("#pozicioni #accordion2").offset().top - 400,
        },
        1000
      );
    });

    $(".scrollTo").on("click", function (e) {
      e.preventDefault();
      var target = $(this).attr("href");
      $("html, body").animate(
        {
          scrollTop: $(target).offset().top - 200,
        },
        1000
      );
    });
    // }
  }
  pauseVideo = () => {
    // Pause as well
    this.refs.vidRef.pause();
    this.refs.vidRef2.pause();
  };
  render() {
    return (
      <section className="car">
        <h2 className="nav-title">208</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/automjete">GAMA</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              208
            </li>
          </ol>
        </div>
        <img className="car-img" src={pgt208}></img>
        <p class="legal-notice">
          * Modelet e paraqitura janë vetëm për qëllime ilustrative. Peugeot
          rezervon të drejtën për të bërë ndryshime.
        </p>
        <div className="fixed-menu" id="myHeader" style={{ width: "100%" }}>
          <div className="row m-0 flex-nowrap">
            <p className="p-3 m-0 col-md-6 text-center current flex-nowrap">
              PEUGEOT 208
            </p>
            <div className="p-3 m-0 col-md-6 text-center flex-nowrap">
              <Link to="/peugeot-e-208">
                <p className="m-0">PEUGEOT E-208</p>
              </Link>
            </div>
          </div>
          <ul className="table-responsive">
            <a href="#dizejno" className="scrollTo">
              <li>DIZEJNO</li>
            </a>
            <a href="#pozicioni" className="scrollTo">
              <li>POZICIONI DREJTUES</li>
            </a>
            <a href="#motori" className="scrollTo">
              <li>MOTORI</li>
            </a>
            <a href="#teknologjia" className="scrollTo">
              <li>TEKNOLOGJIA</li>
            </a>
            <a href="#galeria" className="scrollTo">
              <li>GALERIA</li>
            </a>
          </ul>
        </div>

        <div className="content">
          <p className="text-center">
            Peugeot e re 208 thekson shpirtin e saj rinor me linjën e veçantë
            sportive. Brendësia e saj ofron risinë teknologjike PEUGEOT 3D
            i-Cockpit®. Peugeot e re 208 ju ofron lirinë për të zgjedhur midis
            motorit të benzinës, naftës dhe motorit elektrik.
          </p>
          <div id="content-butona" className="text-center pt-4">
            <button className="more-btn mb-3">KËRKONI NJË TEST DRIVE</button>
            <button className="more-btn mb-3">
              KËRKONI OFERTË PËR AUTOMJET
            </button>
          </div>
        </div>

        <div id="dizejno">
          <h2 className="text-center p-5">DIZENJO E PAREZISTUESHME</h2>
          <div className="row">
            <div className="col-md-6 pr-xl-1">
              <img className="w-100" src={p208d}></img>
            </div>
            <div className="col-md-6 pl-xl-1">
              <img className="w-100" src={p208d2}></img>
              <div className="pt-5 pr-2 pl-2 pl-lg-0"></div>
            </div>
            <div className="pr-2 pl-3">
              <b className="pl-3 pl-lg-0">PROJEKTIM SPORTIV DHE TËRHEQËS</b>
              <br />
              <p className="pt-3">
                Zbuloni personalitetin e fortë të PEUGEOT E RE 208 me shikim të
                parë. Profil i ulët, kofano e gjatë dhe format sensuale,
                nënkuptojnë shpirtin e saj sportiv joshës ndërsa ngjyrat e
                zgjedhura me kujdes dhe origjinale përmirësojnë më tej
                papërmbajtshmërinë e tij. Karakteri i saj i fuqishëm nënvizohet
                më tej nga tavani Black Diamond{" "}
                <Tippy content="Standart, opsional, ose i padisponueshëm sipas versionit">
                  <span className="isTip">i</span>
                </Tippy>
                , dritaret{" "}
                <Tippy content="Opsion standart në versionin GT Line dhe GT">
                  <span className="isTip">i</span>
                </Tippy>{" "}
                dhe disqet e aluminit në formë diamanti{" "}
                <Tippy content="Opsion standart në versionin GT Line dhe GT">
                  <span className="isTip">i</span>
                </Tippy>
                .
              </p>
            </div>
          </div>
          <div className="row m-0">
            <div
              class="container-foto col-md-6 p-0"
              data-toggle="collapse"
              data-target="#collapseOne"
              aria-expanded="true"
              aria-controls="collapseOne"
            >
              <div className="titull">
                <h5>PJESA PËRPARA</h5>
              </div>
              <img className="w-100 pr-1 " src={p208model} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-6 p-0"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
            >
              <div className="titull">
                <h5>DRITAT E PASME FULL LED</h5>
              </div>
              <img className="w-100 pl-1 " src={p208model2} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
          </div>

          <div className="row pt-1 m-0">
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseThree"
              aria-expanded="true"
              aria-controls="collapseThree"
            >
              <div className="titull">
                <h5>SPOILER I PASËM</h5>
              </div>
              <img className="img-fluid  pr-1 pb-md-0 pb-1" src={p208model3} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseFour"
              aria-expanded="true"
              aria-controls="collapseFour"
            >
              <div className="titull">
                <h5>TAVAN PANORAMIK</h5>
              </div>
              <img className="img-fluid  pr-1 pl-1" src={p208model4} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseFive"
              aria-expanded="true"
              aria-controls="collapseFive"
            >
              <div className="titull">
                <h5>МАТERIALET</h5>
              </div>
              <img className="img-fluid  pl-lg-1 pl-0" src={p208model5} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
          </div>
          {/* accordion 1 */}
          <div id="accordion">
            <div class="card">
              <div
                id="collapseOne"
                class="collapse"
                aria-labelledby="headingOne"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">E GUXIMSHME</h2>
                  Forma ekspresive e PEUGEOT E RE 208 stilohet me një grillë
                  kromi të madhe{" "}
                  <Tippy content="Standart, ose i padisponueshëm sipas versionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , dhe paraqet një pamje moderne dhe energjike që plotësohet me
                  dritat FULL LED{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm sipas versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  në formën e 3 kthetrave të luanit me dritat e ditës LED{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm sipas versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  të cilat aktivizohen automatikisht kur ndizet automjeti.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseTwo"
                class="collapse"
                aria-labelledby="headingTwo"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">PREZENCË LUMINOZE</h2>
                  Pjesa e pasme ka një dizenjo unike, e zezë me shkëlqim{" "}
                  <Tippy content="Standart, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  e plotësuar me dritat Full LED{" "}
                  <Tippy content="Standart, opsional, ose i padisponueshëm sipas versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  në formën e 3 kthetrave të luanit. Këto drita ndizen ditën
                  edhe natën për ta bërë automjetin më të sigurt dhe të dukshëm
                  në distancë të largët.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseThree"
                class="collapse"
                aria-labelledby="headingThree"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">PJESË FUNDORE EKSPERSIVE</h2>
                  Pjesa e pasme dinamike dhe e përkryer e PEUGEOT E RE 208 ka 2
                  lloje spoileri{" "}
                  <Tippy content="Opsional, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , një për të kontrolluar konsumin e karburantit dhe tjetri për
                  të përmirësuar performancën në rrugë. Tubi i shkarkimit
                  gjithashtu vjen në versionin e kromuar, duke i dhënë dhe një
                  pamje sportive{" "}
                  <Tippy content="Si standart në versionin GT Line">
                    <span className="isTip">i</span>
                  </Tippy>
                  .
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseFour"
                class="collapse"
                aria-labelledby="headingFour"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">TAVAN PANORAMIK</h2>
                  Ndriçojeni kabinën me dritë natyrale dhe ofrojini udhëtarëve
                  peizazhin e rrugës nga një këndvështrim që nuk e kanë parë
                  kurrë më parë. Tavani panoramik Cielo{" "}
                  <Tippy content="Opsional, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  që shtrihet deri tëk vendet e pasme, siguron shumë dritë
                  natyrale dhe pamje panoramike të qiellit.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseFive"
                class="collapse"
                aria-labelledby="headingFive"
                data-parent="#accordion"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">МАТЕRIALET</h2>
                  Me sediljet e rehatshme, ergonomike dhe materiale me cilësi të
                  lartë, do të ndiheni të qetë dhe komod gjatë gjithë udhëtimit
                  tuaj, pavarësisht distancës. Kjo e kthen ambjentin e brendshëm
                  në një oaz rehatie.
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="pozicioni">
          <h2 className="text-center pt-5 pb-1">POZICIONI DREJTUES</h2>
          <p className="text-center pb-2">TEKNOLOGJIA E GJENERATËS TJETËR</p>
          <ReactCompareImage leftImage={p208left} rightImage={p208right} />
          <p className="pr-2 pl-2 pl-md-0 pt-3">
            PEUGEOT i-Cockpit® 3D
            <br />
            <br /> Zbuloni panelin e ri PEUGEOT i-Cockpit® 3D{" "}
            <Tippy content="Standart, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>{" "}
            brenda një PEUGEOT E RE 208. Përjetoni drejtim elektrizues dhe
            intuitiv me timonin kompakt multi-funksional{" "}
            <Tippy content="Standart,opsional, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>
            , panelin 3D të konfigurueshëm{" "}
            <Tippy content="Standart, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>
            , ekranin me prerje të 10"{" "}
            <Tippy content="Standart,opsional, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>{" "}
            dhe 7 tastat e pianos{" "}
            <Tippy content="Tasta në harmoni me kabinën">
              <span className="isTip">i</span>
            </Tippy>
            . Ju gjithashtu mund të personalizoni ambientin tuaj të brendshëm me
            një zgjedhje prej 8 ngjyrash të ndryshme LED.
          </p>
          <div className="row pt-1 m-0">
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseOneP"
              aria-expanded="true"
              aria-controls="collapseOneP"
            >
              <div className="titull">
                <h5>PANELI I INSTRUMENTAVE 3D</h5>
              </div>
              <img className="img-fluid pr-1  pb-md-0 pb-1" src={p208panel} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseTwoP"
              aria-expanded="true"
              aria-controls="collapseTwoP"
            >
              <div className="titull">
                <h5>NJË EKRAN ME PREKJE ARGËTUES DHE INTUITIV</h5>
              </div>
              <img className="img-fluid" src={p208ekran} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
            <div
              class="container-foto col-md-4 p-0"
              data-toggle="collapse"
              data-target="#collapseThreeP"
              aria-expanded="true"
              aria-controls="collapseThreeP"
            >
              <div className="titull">
                <h5>PRAKTIKE DHE HAPËSIRA INTELIGJENTE</h5>
              </div>
              <img className="img-fluid  pl-lg-1 pl-0" src={p208intel} />

              <div class="overlay-foto">
                <div class="text-permas">+</div>
              </div>
            </div>
          </div>
          {/* accordion 2 */}
          <div id="accordion2">
            <div class="card">
              <div
                id="collapseOneP"
                class="collapse"
                aria-labelledby="headingOne"
                data-parent="#accordion2"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">PANELI I INSTRUMENTAVE 3D</h2>
                  Paneli{" "}
                  <Tippy content="Standart, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  , ka një ekran holografik futuristik 3D, i pozicionuar
                  saktësisht në lartësinë e syrit të drejtuesit. Informacioni i
                  performancës së automjetit dhe drejtimit është i lexueshëm në
                  2 nivele, bazuar në rëndësinë ose urgjencën e tyre (siguria,
                  ndihma e shoferit, navigimi). Mund ta konfiguroni ekranin për
                  të shfaqur informacionin e dëshiruar në çdo kohë, të tilla si
                  navigimi në rrugë me shërbimin e tij të integruar TomTom® që
                  ju siguron informacion në kohë reale për trafikun dhe zonat e
                  rrezikut.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseTwoP"
                class="collapse"
                aria-labelledby="headingTwo"
                data-parent="#accordion2"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">
                    NJË EKRAN ME PREKJE ARGËTUES DHE INTUITIV
                  </h2>
                  Ekrani i madh 10" HD{" "}
                  <Tippy content="Standart,opsional, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  mund të kontrollohet{" "}
                  <Tippy content="Mundësisht kur automjeti është i palëvizshëm">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  nëpërmjet përdorimit të zërit, kontrollit në timon, ose 7
                  tastave. Në ekranin e e-208, në kohë reale, përmes animacionit
                  teknik, mund të ndiqni sesi funksionon teknologjia elektrike.
                  Në sajë të funksionit Mirror Screen{" "}
                  <Tippy content="Mirror Screen përdor shfaqjen e të dhënave të telefonit tuaj në ekran, nëse ju keni internet duke përfshirë edhe qasjen në faqet online dhe nuk gjeneron kosto shtesë. Përdoruesit lidhin pajisjen e tyre përmes një kablloje USB. Funksioni i ekranit Mirror funksionon përmes Android Auto (në Android OS duke shkarkuar aplikacionin Android Auto kur automjeti është ndalur plotësisht), Apple CarPlay & trade; (telefonat OS), ose teknologji MirrorLink & reg; (telefonat të përputhshëm me MirrorLink). Për sigurinë tuaj, Android Auto, Apple CarPlay & trade; ose MirrorLink do të funksionojë vetëm kur makina është e palëvizshme, ose në lëvizje sipas rastit. Kur jeni në lëvizje, disa nga veçoritë e aplikacionit do të bllokohen. Ju mund të gjeni më shumë informacion në http://www.peugeot.fr/services-et-accessoires/services-connectes/mirror-screen.html">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  e vlefshme për Apple CarPlay™, Android Auto, ose MirrorLink®
                  ju mund të pasqyroni përbajtjen e telefonit tuaj në PEUGEOT
                  tuaj të re 208.
                </div>
              </div>
            </div>
            <div class="card">
              <div
                id="collapseThreeP"
                class="collapse"
                aria-labelledby="headingThree"
                data-parent="#accordion2"
              >
                <div class="card-body pb-5">
                  <h2 className="text-center p-3">
                    PRAKTIKE DHE HAPËSIRA INTELIGJENTE
                  </h2>
                  Në pjesën e brendshme, ky model ka hapësirë të bollshme të
                  projektuar për të karikuar{" "}
                  <Tippy content="Karikimi standart wireless Qi 1.1 standart, opsional, ose i padisponueshëm në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  telefonin tuaj Smartphone{" "}
                  <Tippy content="Bazuar në pajtueshmërinë e telefonit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  . Brendësia gjithashtu vjen me disa porta USB{" "}
                  <Tippy content="Deri në katër porta, në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  të dizenjuara për pajisje portative që mund të përdoren nga
                  pasagjerë të tjerë.
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="motori">
          <h2 className="text-center pt-5 pb-1">FUQIA MOTORIKE</h2>
          <p className="text-center pb-2">SHIJO LIRINË E ZGJEDHJES</p>
          <img className="pb-3 img-fluid" src={p208motor}></img>
          <b className="pl-2 pl-lg-0">PROJEKTIM SPORTIV DHE TËRHEQËS</b>
          <br />
          <br />
          <p className="pl-2 pl-lg-0">
            PEUGEOT E RE 208 përfiton nga platforma e re modulare dhe
            shumë-energjetike CMP{" "}
            <Tippy content="Common Modular Platform (Platforma e përbashkët modulare)">
              <span className="isTip">i</span>
            </Tippy>{" "}
            / eCMP{" "}
            <Tippy content="Electric Common Modular Platform (Platforma e përbashkët modulare elektrike)">
              <span className="isTip">i</span>
            </Tippy>{" "}
            duke ju dhënë lirinë të zgjidhni midis një motori benzine PureTech,
            naftë BlueHDi ose motori elektrik, pa pasur nevojë të bëni kompromis
            në hapësirë, teknologji, pamje apo me tej. Përjetoni një përvojë
            euforike drejtimi përmes qëndrimit shembullor në rrugë; hapësirës së
            brendshme të optimizuar; manovrimt të jashtëzakonshëm; ambientin
            futuristik dhe komfortin e brendshëm.
          </p>

          <div id="motor-deck">
            <div className="card-deck m-0">
              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p208motor1}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>MOTORË ME DJEGIE TË BRENDSHME</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Përjetoni drejtim intensiv me gjeneratën e fundit të
                    motorëve me benzinë dhe naftë me efikasitet të lartë që
                    plotësojnë normat Euro 6: - PureTech - BlueHDi falë konsumit
                    të tyre të optimizuar të karburantit{" "}
                    <Tippy
                      content="Të dhëna të vlerësuara, të dhëna vetëm si informacione që ende i nënshtrohen aprovimit.Vlerat e WLTP (1): Konsumi i karburantit 4.2 deri 5.9 në ciklin e kombinuar, 4.7 deri 7.5 me shpejtësi të ulët, 4 deri në 6 me shpejtësi mesatare, 3.6 në 5.1 me shpejtësi të lartë, dhe 4,6 deri në 6 WLTP me shpejtësi të lartë (l / 100 km) - emetim i CO2 (i kombinuar) WLTP: 109 deri në 134 (g/km).
                      (1) Konsumi i karburantit dhe vlerat e CO2 plotësojnë çertifikimin WLTP (Rregullorja e BE 2017/948). Nga 1 shtatori 2018, automjetet e reja pranohen bazuar në WLTP, e cila është një proçedurë e re, më realiste e provës për matjen e konsumit të karburantit dhe emetimeve të CO2 . Kjo proçedurë WLTP zëvendëson plotësisht ciklin e ri evropian të drejtimit (NEDC), një proçedurë e përdorur më parë. Për shkak se kushtet e provës ishin më realiste, konsumi i karburantit dhe emetimet e CO2 të matura me proçedurën WLTP ishin në shumë raste më të larta se ato të matura me proçedurën NEDC. Vlera e konsumit të karburantit dhe emetimeve të CO2 mund të ndryshojnë sipas pajisjeve, opsioneve dhe llojeve të gomave specifike. Kontaktoni pikën tuaj të shitjes për më shumë informacion, ose vizitoni faqen e internetit PEUGEOT të vendit tuaj. 
                      Vlerat e NEDC (2): Konsumi i karburantit nga 3.2 në 4.7 e kombinuar, nga 2.9 në 4.2 jashtë qytetit nga 3.7 në 5.5 në qytet sipas NEDC (l/100 km ) - Emetimi CO2 ( e kombinuar) NEDC: 85 deri 107 (g / km).
                      (2) Vlerat e konsumit të karburantit dhe emetimet e CO2 përcaktohen në bazë të WLTP (Rregullorja e BE 2017/948) dhe vlerat që rezultojnë janë konvertuar në NEDC për të lejuar krahasimin me automjetet e tjera. Ju lutemi kontaktoni shitësin tuaj për informacione të mëtejshme. Vlerat nuk marrin parasysh kushtet e përdorimit, stilin e drejtimit, pajisjet, ose opsionet dhe mund të ndryshojnë sipas llojit të gomës."
                    >
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    , këto motorë janë të pajisur edhe me sistemin Stop & Start.
                  </p>
                </div>
              </div>

              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p208motor2}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>TRANSMETIM AUTOMATIK I TEKNOLOGJISË SË LARTË</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Pasi të provoni saktësinë dhe efikasitetin e kutisë së re të
                    shpejtësisë EAT8i nuk do të donit të ndaloni dhe do të
                    dëshironi ta provoni përsëri! Falë teknologjisë Quickshift,
                    transmetimi është i shpejtë dhe i saktë, por gjithashtu mund
                    t'i ndryshoni ato manualisht përmes levave të vendosura në
                    timon.{" "}
                    <Tippy content="EAT8: Kambio automatike me 8 raporte - Në dispozicion në varësi të versionit">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    , këto motorë janë të pajisur edhe me sistemin Stop & Start.
                  </p>
                </div>
              </div>
              <div className="card m-0">
                <img
                  className="card-img-top pl-0"
                  src={p208motor3}
                  alt="Card cap"
                />
                <div className="card-body pl-2 pl-lg-0">
                  <b>MOTOR ELEKTRIK</b>
                  <br />
                  <br />
                  <p className="card-text">
                    Përjetoni emocione intensive në rrugë në një kabinë të qetë{" "}
                    <Tippy content="Sidoqoftë, mbani gjithçka nën kontroll në automjetin tuaj dhe të jeni të vetëdijshëm për ambientin përreth që të dalloheni nga automjetet e tjera">
                      <span className="isTip">i</span>
                    </Tippy>
                    falë: - Motorit elektrik 100 kW (136 kf) - Përgjigje e
                    menjëhershme me 260 Nm - Autonomia{" "}
                    <Tippy content="Të dhëna të vlerësuara, të dhëna vetëm si informacione që ende i nënshtrohen aprovimit">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    deri në 340 km sipas WLTP{" "}
                    <Tippy content="Të gjitha vlerat e listuara janë në përputhje me procedurën WLTP. Që nga 1 shtatori 2018, automjetet e reja po miratohen në bazë të WLTP, e cila është një proçedurë e re, më realiste për matjen e konsumit të karburantit dhe emetimeve të CO2 dhe autonomisë së automjeteve. Procedura WLTP zëvendëson plotësisht ciklin e ri evropian të drejtimit (NEDC), një proçedurë e përdorur më parë. Autonomia e matur për një automjet elektrik 100% WLTP është më i ulët në shumë raste sesa ai i matur me proçedurën e vjetër NEDC. Të gjitha vlerat e dhëna mund të ndryshojnë sipas pajisjes specifike, opsioneve dhe llojeve të gomave. Kontaktoni pikën tuaj të shitjes për më shumë informacion.">
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    / deri në 450 km sipas NEDC{" "}
                    <Tippy
                      content="Të dhëna të vlerësuara, të dhëna vetëm si informacione që ende i nënshtrohen aprovimit.Vlerat e WLTP (1): Konsumi i karburantit 4.2 deri 5.9 në ciklin e kombinuar, 4.7 deri 7.5 me shpejtësi të ulët, 4 deri në 6 me shpejtësi mesatare, 3.6 në 5.1 me shpejtësi të lartë, dhe 4,6 deri në 6 WLTP me shpejtësi të lartë (l / 100 km) - emetim i CO2 (i kombinuar) WLTP: 109 deri në 134 (g/km).
                      (1) Konsumi i karburantit dhe vlerat e CO2 plotësojnë çertifikimin WLTP (Rregullorja e BE 2017/948). Nga 1 shtatori 2018, automjetet e reja pranohen bazuar në WLTP, e cila është një proçedurë e re, më realiste e provës për matjen e konsumit të karburantit dhe emetimeve të CO2 . Kjo proçedurë WLTP zëvendëson plotësisht ciklin e ri evropian të drejtimit (NEDC), një proçedurë e përdorur më parë. Për shkak se kushtet e provës ishin më realiste, konsumi i karburantit dhe emetimet e CO2 të matura me proçedurën WLTP ishin në shumë raste më të larta se ato të matura me proçedurën NEDC. Vlera e konsumit të karburantit dhe emetimeve të CO2 mund të ndryshojnë sipas pajisjeve, opsioneve dhe llojeve të gomave specifike. Kontaktoni pikën tuaj të shitjes për më shumë informacion, ose vizitoni faqen e internetit PEUGEOT të vendit tuaj. 
                      Vlerat e NEDC (2): Konsumi i karburantit nga 3.2 në 4.7 e kombinuar, nga 2.9 në 4.2 jashtë qytetit nga 3.7 në 5.5 në qytet sipas NEDC (l/100 km ) - Emetimi CO2 ( e kombinuar) NEDC: 85 deri 107 (g / km).
                      (2) Vlerat e konsumit të karburantit dhe emetimet e CO2 përcaktohen në bazë të WLTP (Rregullorja e BE 2017/948) dhe vlerat që rezultojnë janë konvertuar në NEDC për të lejuar krahasimin me automjetet e tjera. Ju lutemi kontaktoni shitësin tuaj për informacione të mëtejshme. Vlerat nuk marrin parasysh kushtet e përdorimit, stilin e drejtimit, pajisjet, ose opsionet dhe mund të ndryshojnë sipas llojit të gomës."
                    >
                      <span className="isTip">i</span>
                    </Tippy>{" "}
                    , këto motorë janë të pajisur edhe me sistemin Stop & Start.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="teknologjia">
          <h2 className="text-center pt-5 pb-1">TEKNOLOGJIA</h2>
          <p className="text-center pb-2">NDIHMA NË DREJTIM DHE SIGURIA</p>
          <img className="pb-3 img-fluid" src={p208tech}></img>
          <p className="pl-2 pl-lg-0">
            PEUGEOT E RE 208 vjen me ndihmat më të fundit inteligjente të
            drejtimit{" "}
            <Tippy content="Standart, opsional, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>{" "}
            duke ofruar siguri në çdo udhëtim{" "}
            <Tippy content="Standart, opsional, ose i padisponueshëm në varësi të versionit">
              <span className="isTip">i</span>
            </Tippy>
            .
          </p>
          <div className="row pt-5">
            <div className="col-lg-3 col-12 pr-0">
              <div
                className="nav flex-column nav-pills"
                id="v-pills-tab"
                role="tablist"
                aria-orientation="vertical"
              >
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center active"
                  id="v-pills-home-tab"
                  data-toggle="pill"
                  href="#v-pills-home"
                  role="tab"
                  aria-controls="v-pills-home"
                  aria-selected="true"
                  onClick={this.pauseVideo}
                >
                  NDIHMA NË DREJTIM <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-profile-tab"
                  data-toggle="pill"
                  href="#v-pills-profile"
                  role="tab"
                  aria-controls="v-pills-profile"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  SISTEMI I FRENIMIT AKTIV{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-messages-tab"
                  data-toggle="pill"
                  href="#v-pills-messages"
                  role="tab"
                  aria-controls="v-pills-messages"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  NDIHMA E PLOTË NË PARKIM{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-settings-tab"
                  data-toggle="pill"
                  href="#v-pills-settings"
                  role="tab"
                  aria-controls="v-pills-settings"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  QËNDRIMI NË KORSI <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-korsia-tab"
                  data-toggle="pill"
                  href="#v-pills-korsia"
                  role="tab"
                  aria-controls="v-pills-korsia"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  MONITORIMI I KËNDIT TË VDEKUR{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-9 col-12 pl-0">
              <div className="tab-content" id="v-pills-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="v-pills-home"
                  role="tabpanel"
                  aria-labelledby="v-pills-home-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <video
                      ref="vidRef"
                      width="550"
                      poster={p208cover1}
                      controls
                    >
                      <source src={p208vid1} type="video/mp4"></source>
                    </video>
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">NDIHMA NË DREJTIM</p>

                      <p>
                        Sistemi i ndihmës së drejtimit{" "}
                        <Tippy content="Standard,opsional, ose i padisponueshëm, në varësi të versionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        ju udhëzon përmes drejtimit gjysmë autonom - Ndjekja e
                        një traktoreje duke zgjedhur një pozicion korsie -
                        Njohja e shenjës së tabelave rrugore - Rregullimi i
                        shpejtësisë dhe distancës së sigurisë me automjetin
                        përpara.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-profile"
                  role="tabpanel"
                  aria-labelledby="v-pills-profile-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <video
                      ref="vidRef2"
                      width="550"
                      controls
                      poster={p208cover2}
                    >
                      <source src={p208vid2} type="video/mp4"></source>
                    </video>
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">ACTIVE SAFETY BRAKE</p>
                      (Sistemi i frenimit automatik emergjent)
                      <br />
                      <br />
                      <p>
                        Nëse ky sistem{" "}
                        <Tippy content="Standard,opsional, ose i padisponueshëm, në varësi të versionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        zbulon rrezikun e një përplasjeje të mundshme përpara,
                        një makinë apo një këmbësor në rrugë, automatikisht
                        aktivizon frenat për të shmangur goditjen apo për të
                        reduktuar pasojat. Ky sistem aktiv funksionon me një
                        shpejtësi deri në 85 km/orë.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-messages"
                  role="tabpanel"
                  aria-labelledby="v-pills-messages-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p208ndihma} />
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">NDIHMA E PLOTË NË PARKIM</p>

                      <p>
                        Ndihma automatike e plotë për parkim paralel ose me ecje
                        prapa{" "}
                        <Tippy content="Ndihma automatike e plotë për parkim paralel ose me ecje prapa. – Standart, opsional, ose i padisponueshëm në varësi të versionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        . Nuk kërkon ndonjë veprim të drejtuesit në timon apo në
                        pedale, sepse pasi të zbulojë hapësirën e parkimit,
                        sistemi merr në zotërim drejtimin, shpejtësinë dhe
                        frenimin. Kërkohet vetëm veprimi i drejtuesit në levën e
                        marrshit (vendoseni në modalitetin P). Ky sistem po
                        ashtu ju lehtëson daljen nga hapësira e parkimit.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-settings"
                  role="tabpanel"
                  aria-labelledby="v-pills-settings-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p208korsi} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">QËNDRIMI NË KORSI</p>

                      <p>
                        Duke përdorur një aparat fotografik në krye të xhamit të
                        përparmë, sistemi{" "}
                        <Tippy content="Standard,opsional, ose i padisponueshëm, në varësi të versionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        njeh korsitë me vija të ndërprera dhe të pandërprera.
                        Kthen automjetin në korsi duke drejtuar timonin përpara
                        se të dali nga korsia apo nga rruga, pa aktivizuar
                        sinjalet e drejtimit. Sistemi është aktiv në shpejtësi
                        deri në 65 km/orë.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-korsia"
                  role="tabpanel"
                  aria-labelledby="v-pills-korsia-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img className="w-100" src={p208kendi} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">
                        MONITORIMI I KËNDIT TË VDEKUR
                      </p>

                      <p>
                        Me shpejtësi ndërmjet 65 km/orë dhe 140 km/orë, sistemi
                        <Tippy content="Si opsion">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        paralajmëron shoferin për praninë e automjetit në pikën
                        e verbër të tij. Paralajmërimi jepet me një dritë që
                        shfaqet në pasqyrë në anën nga bëhet parakalimi. Nëse
                        shoferi ende dëshiron të kalojë korsinë tjetër (dhe
                        treguesi i drejtimit, d.m.th. sinjali i kthesës është i
                        ndezur), atëherë sistemi nuk e lejon atë, duke e mbajtur
                        automjetin në drejtim dhe duke parandaluar kështu një
                        përplasje të mundshme.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="galeria">
          <h2 className="text-center pt-5 pb-1">GALERIA</h2>
          <Splide
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              // height: "360px",
              // autoplay: true,
              pagination: true,
            }}
          >
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208g1} />
                  <img className="w-100" src={p208g2} />
                  <img className="w-100" src={p208g3} />
                  <img className="w-100" src={p208g4} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208g5}
                />
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <img
                  className="img-fluid col-lg-6 pl-0 pr-1"
                  src={p208gmotor}
                />
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208ge} />
                  <img className="w-100" src={p208gtel} />
                  <img className="w-100" src={p208g4} />
                  <img className="w-100" src={p208motor3} />
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208b1} />
                  <img className="w-100" src={p208ulse} />
                  <img className="w-100" src={p208b2} />
                  <img className="w-100 h-100" src={p208right} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208tech}
                />
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="row m-0">
                <div className="grid-container col-lg-6">
                  <img className="w-100" src={p208b1} />
                  <img className="w-100" src={p208ulse} />
                  <img className="w-100" src={p208b2} />
                  <img className="w-100 h-100" src={p208right} />
                </div>
                <img
                  className="img-fluid col-lg-6 pl-0 pl-lg-1 pr-0 pr-lg-1"
                  src={p208tech}
                />
              </div>
            </SplideSlide>
          </Splide>
          <div className="pl-2 pt-5 pl-xl-0">
            <p>
              * Konsumi i karburantit dhe emetimit të CO<sub>2</sub>
            </p>
            <p>
              Të dhënat e vlerësuara, të siguruara vetëm si informacione që ende
              i nënshtrohen aprovimit.
            </p>
            <p>
              Vlerat WLTP (1): Konsumi i karburantit 4.2 deri 5.9 e kombinuar,
              4.7 deri 7.5 me shpejtësi të ulët, 4 deri në 6 me shpejtësi
              mesatare, 3.6 në 5.1 me shpejtësi të lartë, dhe 4,6 deri në 6 WLTP
              me shpejtësi shumë të lartë (l/100 km) - emetim i CO<sub>2</sub>{" "}
              (i kombinuar) WLTP: 109 deri në 134 (g / km).
            </p>
            <p>
              (1) Konsumi i karburantit dhe vlerat e CO<sub>2</sub> plotësojnë
              çertifikimin WLTP (Rregullorja e BE 2017/948). Nga 1 shtatori
              2018, automjetet e reja pranohen bazuar në standartin WLTP, e cila
              është një proçedurë e re, më realiste për matjen e konsumit të
              karburantit dhe emetimeve të CO<sub>2</sub>. Kjo proçedurë WLTP
              zëvendëson plotësisht ciklin e ri evropian të drejtimit (NEDC),
              një proçedurë e përdorur më parë. Për shkak se kushtet e provës
              ishin më realiste, konsumi i karburantit dhe emetimet e CO
              <sub>2</sub> të matura me procedurën WLTP ishin në shumë raste më
              të larta se ato të matura me procedurën NEDC. Vlera e konsumit të
              karburantit dhe emetimeve të CO
              <sub>2</sub> mund të ndryshojë sipas pajisjeve, opsioneve dhe
              llojeve të gomave specifike. Kontaktoni shitësin tuaj për më shumë
              informacion ose vizitoni faqen e internetit PEUGEOT në vendin
              tuaj.
            </p>

            <p>
              Vlerat e NEDC (2): Konsumi i karburantit nga 3.2 në 4.7 e
              kombinuar, nga 2.9 në 4.2 në rrugë jashtë qytetit dhe nga 3.7 në
              5.5 në qytet NEDC (l/100 km ) - CO<sub>2</sub> (kombinuar) NEDC:
              85 deri në 107 (g/km).
            </p>
            <p>
              (2) Vlerat e konsumit të karburantit dhe emetimet e CO<sub>2</sub>{" "}
              përcaktohen në bazë të standartit WLTP (Rregullorja e BE 2017/948)
              dhe vlerat që rezultojnë konvertohen në NEDC për të lejuar
              krahasimin me automjetet e tjera. Ju lutemi kontaktoni shitësin
              tuaj për më shumë informacion. Vlerat nuk marrin parasysh kushtet
              e përdorimit, stilin e drejtimit, pajisjet ose opsionet dhe mund
              të ndryshojnë sipas llojit të gomës.
            </p>
          </div>
        </div>

        <div className="crumbi mt-4" id="myHeader2">
          <div className="row">
            <div id="test" className="col-lg-4 text-center">
              <Link to="/automjete/test-drive">TEST DRIVE</Link>
            </div>
            <div id="ofert" className="col-lg-8 pl-5">
              <Link to="/automjete/oferte-automjete">
                KËRKONI OFERTË PËR AUTOMJET{" "}
              </Link>
              <i className="fas fa-angle-right pl-3"></i>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
