import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import pgt108 from "../../images/pgt108.jpg";
import diz108 from "../../images/diz108.jpg";
import dizz108 from "../../images/dizz108.jpg";
import p108c1 from "../../images/108c1.jpg";
import p108c2 from "../../images/108c2.jpg";
import p108c3 from "../../images/108c3.jpg";
import p108c4 from "../../images/108c4.jpg";
import p108c5 from "../../images/108c5.jpg";
import seri108 from "../../images/seri108.jpg";
import collect from "../../images/collect.jpg";
import gtline from "../../images/gtline.jpg";
import nd1 from "../../images/nd1.jpg";
import nd2 from "../../images/nd2.jpg";
import komf1 from "../../images/komf1.jpg";
import komf2 from "../../images/komf2.jpg";
import komf3 from "../../images/komf3.jpg";
import komf4 from "../../images/komf4.jpg";
import essence from "../../images/essence.jpg";
import sinjal from "../../images/sinjal.jpg";
import korsia from "../../images/korsia.jpg";
import presion from "../../images/presion.jpg";
import front108 from "../../images/front108.jpg";
import kalume108 from "../../images/kalume108.jpg";
import permas108 from "../../images/permas108.jpg";
import perpara108 from "../../images/perpara108.jpg";
import carplay from "../../images/carplay.jpg";
import perpara108gj from "../../images/perpara108gj.jpg";
import thumbnail1 from "../../images/thumbnail1.png";
import thumbnail2 from "../../images/thumbnail2.png";
import thumbnail3 from "../../images/thumbnail3.png";
import citybrake from "../../images/citybrake.mp4";
import kamerav from "../../images/kamera-v.mp4";
import nago from "../../images/nago.mp4";

import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional

import $ from "jquery";
window.$ = $;

export default class Peugeot108 extends Component {
  componentDidMount() {
    // mobile
    if (window.innerWidth < 992) {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().top;
      var elementPosition2 = document
        .getElementById("galeria")
        .getBoundingClientRect().left;

      //top offset of static/fixed div
      var staticOffset = $("#myHeader2").offset().top;
      //window height
      var wpHeight = $(window).height();

      //point when the user scrolls at the bottom of div
      //static/fixed div height + top offset - viewport height
      var treshold = staticOffset + $("#myHeader2").height() + wpHeight;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
          // bot
          $("#myHeader2").addClass("stickyb");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition2 + treshold + 3500) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }
    // desktop
    else {
      var elementPosition = document
        .getElementById("content-butona")
        .getBoundingClientRect().left;
      var elementPosition2 = document
        .getElementById("footer-content")
        .getBoundingClientRect().top;

      $(window).scroll(function () {
        // top
        if ($(window).scrollTop() >= elementPosition) {
          $("#myHeader").addClass("sticky");
        } else {
          $("#myHeader").removeClass("sticky");
        }
        // 2700
        if ($(window).scrollTop() >= elementPosition) {
          // bot
          $("#myHeader2").addClass("stickyb");
        }
        if ($(window).scrollTop() > elementPosition2 + 2300) {
          $("#myHeader2").removeClass("stickyb");
        }
      });
    }

    $(".scrollTo").on("click", function (e) {
      e.preventDefault();
      var target = $(this).attr("href");
      $("html, body").animate(
        {
          scrollTop: $(target).offset().top - 100,
        },
        1000
      );
    });
  }
  pauseVideo = () => {
    // Pause as well
    this.refs.vidRef.pause();
    this.refs.vidRef2.pause();
    this.refs.vidRef3.pause();
  };

  render() {
    return (
      <section className="car">
        <h2 className="nav-title">108</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <Link to="/">FAQJA KRYESORE</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/automjete">GAMA</Link>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              108
            </li>
          </ol>
        </div>
        <img className="car-img" src={pgt108}></img>
        <p className="legal-notice">
          * Modelet e paraqitura janë vetëm për qëllime ilustrative. Peugeot
          rezervon të drejtën për të bërë ndryshime.
        </p>
        <p className="text-center above">PEUGEOT 108</p>
        <div className="fixed-menu table-responsive" id="myHeader">
          <ul>
            <a href="#dizejno" className="scrollTo">
              <li>DIZEJNO</li>
            </a>
            <a href="#pershtat" className="scrollTo">
              <li>PËRSHTATSHMËRI</li>
            </a>
            <a href="#serite" className="scrollTo">
              <li>SERITË SPECIALE</li>
            </a>
            <a href="#komoditet" className="scrollTo">
              <li>KOMODITETI</li>
            </a>
            <a href="#motoret" className="scrollTo">
              <li>MOTORËT</li>
            </a>
            <a href="#teknologjia" className="scrollTo">
              <li>TEKNOLOGJIA</li>
            </a>
            <a href="#galeria" className="scrollTo">
              <li>GALERIA</li>
            </a>
          </ul>
        </div>

        <div className="content pb-5">
          <p className="text-center">
            PEUGEOT 108 krenohet me pamjen e saj të hijshme dhe moderne dhe në
            të njëjtën kohë siguron ngarje të lehtë dhe të rehatshme në rrugët e
            qytetit. Kjo makinë kompakte vjen totalisht ndryshe nga pjesa tjetër
            e gamës. Pajisjet e tij të lidhura me internetin do të ndryshojnë
            jetën tuaj të përditshme.
          </p>
          <div id="content-butona" className="text-center pt-4">
            <button className="more-btn mb-3">KËRKONI NJË TEST DRIVE</button>
            <button className="more-btn mb-3">
              KËRKONI OFERTË PËR AUTOMJET
            </button>
          </div>
        </div>

        <div id="dizejno">
          <h2 className="text-center p-5">DIZEJNO</h2>
          <div className="row">
            <div className="col-md-8 pr-0">
              <img className="w-100" src={diz108}></img>
              <h4 className="pt-3">SILUETË MODERNE</h4>
              <br />
              <p>
                PEUGEOT 108 është një ndërthurje e modernitetit dhe elegancës.
                Pamja e saj e sofistikuar me linja dinamike dhe detaje të
                zgjedhura me kujdes, bie në sy me shikim të parë. Ajo përshtatet
                për të gjitha dëshirat tuaja me një përzgjedhje prej tetë
                ngjyrash, temave të personalizimit, serive speciale dhe
                cilësimeve të ndryshme të brendshme.
              </p>
              <br />
              <p>
                Në versionin 108 TOP shijoni pamjen e ofruar nga çatia{" "}
                <Tippy content="Canvas e zezë, Jeans, Green Fizz, ose Bordeaux">
                  <span className="isTip">i</span>
                </Tippy>{" "}
                e zbuluar dhe të zbuloni qytetin në një mënyrë krejt ndryshe.
              </p>
            </div>
            <div className="col-md-4">
              <img className="w-100" src={dizz108}></img>
              <h4 className="pt-3">THJESHT E BUKUR</h4>
              <br />
              <p>
                Fenerët LED zbukurojnë më tej pamjen e saj moderne dhe të
                mrekullueshme. Dritat e pasme me funksionin e ndezjes në kthesa
                së bashku me dritat e mjegullës, krijojnë tre kthetra
                karakteristike të luanit që dalin nga trupi dhe plotësojnë
                pjesën e pasme të automjetit.
              </p>
              <br />
            </div>
          </div>
        </div>

        <div id="pershtat">
          <h2 className="text-center pt-5 pb-1">PËRSHTATSHMËRI</h2>
          <p className="text-center">PEUGEOT 108 për çdo shije!</p>
          <Splide
            id="pershtat-slide"
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              // arrows: false,
              // autoplay: true,
              pagination: true,
            }}
          >
            <SplideSlide className="d-flex pt-4">
              <img
                className="img-fluid"
                data-splide-lazy={p108c1}
                alt="Image 1"
              />
              <div className="pl-2 pt-2">
                <b>DISPLAY</b>
                <br />
                <p className="pt-4 pb-5">
                  Peugeot 108 me temë Display{" "}
                  <Tippy content="Kjo temë është në dispozicion në Peugeot 108 Bi-tone (tavan i zi) dhe TOP 108 (canvas e zezë)">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  konfirmon karakterin ekspresiv dhe dinamik të plotësuar me
                  detaje rozë (dizenjo e brendshme, linjat e derës së përparme),
                  ndërsa kapakët e zinj "Diamanti i Zi" mbi pasqyrat me shkëlqim
                  diamanti dhe çatia, nxjerrin në pah frymën e saj sportive.
                  Kënaquni gjatë ngarjes së automjetit
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img
                className="img-fluid"
                data-splide-lazy={p108c2}
                alt="Image 2"
              />
              <div className="pl-2 pt-2">
                <b>PLAYLIST</b>
                <br />
                <p className="pt-4 pb-5">
                  Shijoni pasionin tuaj për muzikë me temën e Playlist. Kjo temë
                  është frymëzuar nga komandat "Play", "Stop", "Tuning" me
                  ngjyra të ndryshme. Ajo që është e sigurt, është që ju do të
                  kënaqeni me udhëtimin duke dëgjuar muzikë të preferuar.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img
                className="img-fluid"
                data-splide-lazy={p108c3}
                alt="Image 3"
              />
              <div className="pl-2 pt-2">
                <b>PAKETA "AMBIANCE" blu</b>
                <br />
                <p className="pt-4 pb-5">
                  Zbuloni paketën "Ambiance" blu: Brendësia blu{" "}
                  <Tippy content="Në dispozicion në Peugeot 108 dhe në Peugeot 108 TOP, në nivelet Active dhe Allure të pajisjeve">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  Square Jusa, ventilimi dhe kutia e ingranazhit në Calvi të
                  shkëlqyeshëm blu, kapakë blu të pajisura me rripa alumini dhe
                  opsionale Jeans Canvas për 108 TOP.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img
                className="img-fluid"
                data-splide-lazy={p108c4}
                alt="Image 4"
              />
              <div className="pl-2 pt-2">
                <b>PAKETA "AMBIANCE" e gjelbër </b>
                <br />
                <p className="pt-4 pb-5">
                  Zbuloni paketën{" "}
                  <Tippy content="Në dispozicion në Peugeot 108 dhe në Peugeot 108 TOP, në nivelet Active dhe Allure të pajisjeve">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  "Ambiance" e gjelbër: Brendësia jeshile Square Jusa, ventilimi
                  dhe kutia e ingranazhit në Fizz jeshile të shkëlqyeshme,
                  kapakë jeshil me zbukurim alumini dhe opsionale në Fizz të
                  gjelbër të shkëlqyeshëm për 108 TOP.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img
                className="img-fluid"
                data-splide-lazy={p108c5}
                alt="Image 5"
              />
              <div className="pl-2 pt-2">
                <b>PAKETA "AMBIANCE" Antilopë</b>
                <br />
                <p className="pt-4 pb-5">
                  Zbuloni paketën "Ambiance" Antilopë{" "}
                  <Tippy content="Në dispozicion në Peugeot 108 dhe në Peugeot 108 TOP, në nivelet Active dhe Allure të pajisjeve">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  : Brendësia Square Jusa Corail, ventilim dhe ingranazh i kuq
                  Antilope, kapakë i kuq i pajisur me rripa alumini dhe kanavacë
                  opsionale në Bordo për 108 TOP.
                </p>
              </div>
            </SplideSlide>
          </Splide>
        </div>

        <div id="serite">
          <h2 className="text-center pt-5 pb-1">SERITË SPECIALE</h2>
          <Splide
            id="serite-slide"
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              // autoplay: true,
              // arrows: false,
              pagination: true,
            }}
          >
            <SplideSlide className="d-flex flex-row-reverse pt-4">
              <img className="" data-splide-lazy={seri108} alt="Image 2" />
              <div className="pl-2 pt-2">
                <b>108 TOP ROLAND GARROS</b>
                <br />
                <p className="pt-4">
                  PEUGEOT 108 TOP Roland Garros shpreh frymën sportive dhe
                  sofistikimin. Dinamika e jashtme i jep ngjyrë detajeve dhe
                  mbishkrimeve PEUGEOT. Tavani i zi, detajet e kromit, rripat e
                  aluminit, ose ventilatoret e zinj me shkëlqim, konfirmojnë
                  karakterin e tij bashkëkohor. 108 TOP vjen në dy ngjyra:
                  Lipizan i bardhë, ose Caldera i zi.
                </p>
                <br />
                <br />
                <p>
                  Kroskoti vjen i kombinuar me ngjyrën e trupit të 108 TOP
                  Roland Garros, ndërsa veshjet specifike të brendshme dhe
                  dyshemetë, kanë qepje portokalli me logon e turneut. Elementët
                  e aluminit përshtaten në mënyrë të përkryer me detajet.
                  Versioni standard ka pajisjet e mëposhtme: Ekrani me prekje, A
                  / C, ndriçimi automatik i dritës, ADML{" "}
                  <Tippy content="Hyrja “hands-free”">
                    <span className="isTip">i</span>
                  </Tippy>
                  ,Navigator{" "}
                  <Tippy content="Në varesi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , Active City Brake me funksionin e njohjes së trafikut,
                  alarmi{" "}
                  <Tippy content="Në varesi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  i korsisë së trafikut dhe kamera e pasme{" "}
                  <Tippy content="Në varesi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  .
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img className="" data-splide-lazy={collect} alt="Image 3" />
              <div className="pl-2 pt-2">
                <b>COLLECTION</b>
                <br />
                <p className="pt-4">
                  Në versionin Standard, ose me tavan të hapur, Koleksioni 108
                  në edicionin e tij 2019 sjell gjashtë ngjyra të mrekullueshme
                  dhe me shkëlqim: Green Fizz, Blue Clavi, White Lipizan, Grey
                  Gallium, Grey Carlinite, Black Caldera.
                </p>
                <br />
                <p className="pr-5">
                  Moderne dhe elegante. Kjo pamje plotësohet me pasqyra të
                  kromuara dhe të brendshme të tekstilit që mund të jenë Square
                  Jusa Green, Square Jusa Blue ose, Square Jusa Corail.
                  Elementët e aluminit, tapetet e dyshemesë të bardhë dhe veshja
                  e paneleve të instrumenteve, janë në përputhje me ngjyrën e
                  bardhë të trupit{" "}
                  <Tippy content="Në dispozicion në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  . Në versionin me çati të hapur, veshja mund të jetë në Black,
                  Jeans, ose Green Fizz{" "}
                  <Tippy content="Në dispozicion në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  .
                </p>
                <br />
                <p>
                  Stilimi i jashtëm i linjës dhe xhami i pasëm, alumini i bardhë
                  ose, me ngjyrë, theksojnë siluetën e Koleksionit 108, e cila
                  është sportive, moderne dhe dinamike në të njëjtën kohë.
                </p>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <img className="" data-splide-lazy={gtline} alt="Image 1" />
              <div className="pl-2 pt-2">
                <b>108 GT LINE</b>
                <br />
                <p className="pt-4">
                  Peugeot 108 GT Line merr stilin e grafikës nga makinat
                  sportive. Pamja e saj tërheqëse dhe moderne shkrihet në linjat
                  atletike. Vjen me pajisje të shumta: ekran me prekje, ajër të
                  kondicionuar automatik{" "}
                  <Tippy content="Në dispozicion në varësi të versionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  , Active City Brake{" "}
                  <Tippy content="Opsionale">
                    <span className="isTip">i</span>
                  </Tippy>
                  , kamera në pjesën e pasme{" "}
                  <Tippy content="Standard, ose opsionale në varësi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>
                  .
                </p>
              </div>
            </SplideSlide>
          </Splide>
        </div>

        <div id="nderlidhja">
          <h2 className="text-center p-5">NDËRLIDHJA</h2>
          <div className="row">
            <div className="col-md-6 pr-xl-1">
              <img className="w-100" src={nd1}></img>
              <div className="pt-5 pr-2 pl-2 pl-lg-0">
                <b>EKRAN ME PREKJE</b>
                <br />
                <p>
                  PEUGEOT 108 City Car ju prezanton me epokën e re teknologjike
                  me një ekran me prekje 7"{" "}
                  <Tippy content="Standard, ose opsionale në varësi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  i vendosur në qendër të kroskotit. Ai tregon të gjitha
                  informacionet e nevojshme dhe të dobishme për drejtimin:
                  navigim, tabelë kompjuteri, cilësimet e automjetit, radio, si
                  dhe ofron teknologjinë Mirror Screen.
                </p>
              </div>
            </div>
            <div className="col-md-6 pl-xl-1">
              <img className="w-100" src={nd2}></img>
              <div className="pt-5 pr-2 pl-2 pl-lg-0">
                <b>MIRROR SCREEN</b>
                <br />
                <p>
                  Falë teknologjisë Mirror Screen{" "}
                  <Tippy content="Standard, opsionale, ose i padisponueshëm në varësi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  , ekrani me prekje bëhet ekran për telefonin tuaj{" "}
                  <Tippy content="MirrorLink®, Android Auto® (Aplikacionet Apple CarPlay™ do të funksionojnë kur makina është në lëvizje, ose jo në lëvizje, në raste të caktuara. Disa nga funksionalitetet e këtyre aplikacioneve do të parandalohen në lëvizje. Disa lloje të përmbajtjes që mund të jenë lirisht të disponueshme në telefonin tuaj do të kërkojnë një abonim në regjistrin ekuivalent MirrorLink®, Android, ose Apple Auto®  CarPlay. Funksioni MirrorLink® punon përmes MirrorLink®  (për Android®, BlackBerry® & Windows Phone®, të cilat kanë funksionin MirrorLink®), ose Android Auto (për telefonat Android), ose Apple Carplay®  (për telefona celularë të aktivizuar me iOS), nëse abonimi i telefonit tuaj përfshin aksesin në Internet. Më shumë informacion në http://www.peugeot.fr/marque-et-technologie/technologies/connectivite.html">
                    <span className="isTip">i</span>
                  </Tippy>
                  . Do të gjeni menunë e zakonshme dhe do të menaxhoni telefonin
                  dhe aplikacionet e tij direkt dhe lehtë përmes ekranit të
                  prekjes.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div id="komoditet">
          <h2 className="text-center pt-5">KOMODITETI</h2>
          <p className="text-center pt-3">KOMFORTE DHE KOMPAKTE</p>
          <div className="row">
            <div className="col-md-6 pr-1">
              <img className="w-100" src={komf1}></img>
            </div>
            <div className="col-md-6 pl-1">
              <img className="w-100" src={komf2}></img>
            </div>
            <p className="p-4">
              Brendësia e PEUGEOT 108 ofron hapësirë maksimale dhe shumë hapsira
              magazinimi për shoferët dhe pasagjerët. Hapësirë e zgjerueshme,
              kur palosni sediljen e pasme në 50/50{" "}
              <Tippy content="Në varësi të destinacionit">
                <span className="isTip">i</span>
              </Tippy>{" "}
              , mund të arrijë nga 243 L{" "}
              <Tippy content="196 dm3 sipas standartit VDA">
                <span className="isTip">i</span>
              </Tippy>{" "}
              deri në 868 L{" "}
              <Tippy content="780 dm3 sipas standartit VDA">
                <span className="isTip">i</span>
              </Tippy>
              .
            </p>
            <div className="col-md-6 pr-xl-1">
              <img className="w-100" src={komf3}></img>
              <div className="pt-5 pr-3 pl-2 pl-lg-0">
                <b>PEUGEOT START & STOP</b>
                <br />
                <p>
                  Hapja pa duar{" "}
                  <Tippy content="Opsionale, ose i padisponueshëm, në varësi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  ju lejon për të zhbllokuar derën pa përdorur çelësin dhe të
                  ndizni makinën thjesht duke shtypur Start & Stop. Përshtatur
                  në mënyrë të përsosur për drejtimin në qytet, sistemi Start &
                  Stop (motori VTi 72 S&S BVM5) zvogëlon konsumin e karburantit,
                  emetimet e grimcave dhe nivelet e zhurmës kur automjeti është
                  i palëvizshëm.
                </p>
              </div>
            </div>
            <div className="col-md-6 pl-xl-1">
              <img className="w-100" src={komf4}></img>
              <div className="pt-5 pr-3 pl-2 pl-lg-0">
                <b>KONTROLLI AUTOMATIK I KLIMËS</b>
                <br />
                <p>
                  Ka kontroll manual të klimës{" "}
                  <Tippy content="Opsionale, ose i padisponueshëm, në varësi të destinacionit">
                    <span className="isTip">i</span>
                  </Tippy>{" "}
                  për të bërë çdo udhëtim sa më të rehatshëm të jetë e mundur
                  dhe kondicionimin automatik të ajrit të disponueshëm në gamën
                  108 të Koleksionit.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div id="motoret">
          <h2 className="text-center pt-5">MOTORËT</h2>
          <p className="text-center pt-3">EKONOMIKË DHE PERFORMANCË</p>
          <div className="row pt-3">
            <img className="w-100 col-md-6" src={essence}></img>
            <p className="col-md-6 p-4 p-lg-0">
              PEUGEOT 108 ka një motor 3 cilindër VTi 72 S&S BVM5{" "}
              <Tippy content="Konsumi i karburantit dhe vlerat e emetimit të CO2 tregohen gjatë homologimit">
                <span className="isTip">i</span>
              </Tippy>{" "}
              që është i fuqishëm dhe ekonomik. Ky motor benzine është kompakt
              dhe më i lehtë, ruan efikasitetin në drejtim të konsumit optimal
              të karburantit dhe fërkimit të zvogëluar.
            </p>
          </div>
        </div>

        <div id="teknologjia">
          <h2 className="text-center pt-5">TEKNOLOGJIA</h2>
          <p className="text-center pt-3">NDIHMA DHE SIGURIA E DREJTUESIT</p>
          <div className="row">
            <div className="col-lg-3 col-12 pr-0">
              <div
                className="nav flex-column nav-pills"
                id="v-pills-tab"
                role="tablist"
                aria-orientation="vertical"
              >
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center active"
                  id="v-pills-home-tab"
                  data-toggle="pill"
                  href="#v-pills-home"
                  role="tab"
                  aria-controls="v-pills-home"
                  aria-selected="true"
                  onClick={this.pauseVideo}
                >
                  ACTIVE CITY BRAKE <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-profile-tab"
                  data-toggle="pill"
                  href="#v-pills-profile"
                  role="tab"
                  aria-controls="v-pills-profile"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  KAMERA E PASME <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-messages-tab"
                  data-toggle="pill"
                  href="#v-pills-messages"
                  role="tab"
                  aria-controls="v-pills-messages"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  NDIHMA NË MALORE <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-settings-tab"
                  data-toggle="pill"
                  href="#v-pills-settings"
                  role="tab"
                  aria-controls="v-pills-settings"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  NJOHJA E SINJALISTIKËS{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-korsia-tab"
                  data-toggle="pill"
                  href="#v-pills-korsia"
                  role="tab"
                  aria-controls="v-pills-korsia"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  SINJALIZIMI I KORSISË{" "}
                  <i className="fas fa-angle-right pr-3"></i>
                </a>
                <a
                  className="nav-link d-flex justify-content-between p-3 align-items-center"
                  id="v-pills-presion-tab"
                  data-toggle="pill"
                  href="#v-pills-presion"
                  role="tab"
                  aria-controls="v-pills-presion"
                  aria-selected="false"
                  onClick={this.pauseVideo}
                >
                  PRESIONI I GOMAVE <i className="fas fa-angle-right pr-3"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-9 col-12 pl-0">
              <div className="tab-content" id="v-pills-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="v-pills-home"
                  role="tabpanel"
                  aria-labelledby="v-pills-home-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <video
                      ref="vidRef"
                      width="550"
                      poster={thumbnail1}
                      controls
                    >
                      <source src={citybrake} type="video/mp4"></source>
                    </video>
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">ACTIVE CITY BRAKE</p>

                      <p>
                        Active City Brake{" "}
                        <Tippy content="Opsionale, ose i padisponueshëm, në varësi të destinacionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        (FARC) është një pajisje për të aplikuar frenimin.
                        Drejtimi deri të paktën 30 km / orë zvogëlon rrezikun e
                        një përplasjeje, ose pasojat e një përplasjeje nëse
                        shoferi nuk ndërhyn në kohë.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-profile"
                  role="tabpanel"
                  aria-labelledby="v-pills-profile-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <video
                      ref="vidRef2"
                      width="550"
                      controls
                      poster={thumbnail2}
                    >
                      <source src={kamerav} type="video/mp4"></source>
                    </video>
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">KAMERA E PASME</p>

                      <p>
                        Kamera në pjesën e pasme{" "}
                        <Tippy content="Standart, ose opsionale në varësi të versioneve të pajisura me ekran me prekje">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        aktivizohet automatikisht kur automjeti lëviz mbrapa.
                        Për siguri më të madhe dhe lehtësinë e drejtimit, i
                        lejon shoferit të vizualizojë manovrimin në ekranin e
                        madh me prekje
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-messages"
                  role="tabpanel"
                  aria-labelledby="v-pills-messages-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <video
                      ref="vidRef3"
                      width="550"
                      controls
                      poster={thumbnail3}
                    >
                      <source src={nago} type="video/mp4"></source>
                    </video>
                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">NDIHMA NË MALORE</p>

                      <p>
                        Sistemi i asistencës{" "}
                        <Tippy content="Kjo veçori është e disponueshme si pajisje standarte dhe nuk mund të çaktivizohet">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        për fillimin e malores mban automjetin mobilizuar për
                        një periudhë më të shkurtër kohore. Në këtë mënyrë
                        shoferi ka kohë të lëvizë këmbën nga pedali i
                        përshpejtuesit në pedalin e frenave.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-settings"
                  role="tabpanel"
                  aria-labelledby="v-pills-settings-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img src={sinjal} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">NJOHJA E SINJALISTIKËS</p>

                      <p>
                        ACTIVE CITY BRAKE është e lidhur me sistemin e njohjes
                        së sinjalistikave të trafikut: zbulon dhe automatikisht
                        lexon paralajmërimet e kufirit të shpejtësisë dhe
                        përfundimet e kufirit të shpejtësisë duke përdorur
                        kamerën e vendosur në krye të xhamit të parë.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-korsia"
                  role="tabpanel"
                  aria-labelledby="v-pills-korsia-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img src={korsia} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">SINJALIZIMI I KORSISË</p>

                      <p>
                        Dalja nga korsia sinjalizohet{" "}
                        <Tippy content="Opsionale, ose i padisponueshëm, në varësi të versionit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        kur automjeti kalon korsinë kur ajo është me vija të
                        pandërprera, ose të ndërprera në një shpejtësi prej 50
                        km / orë nëse treguesi i drejtimit nuk është aktivizuar.
                        Paralajmërimet vizuale janë veçanërisht të dobishme në
                        rast të përgjumjes{" "}
                        <Tippy content="Për sigurinë tuaj, kushtojini vëmendje shenjave të lodhjes, pushoni çdo 2 orë dhe përqëndrohuni gjatë udhëtimit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        , ose humbjes së përqendrimit.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="v-pills-presion"
                  role="tabpanel"
                  aria-labelledby="v-pills-presion-tab"
                >
                  <div className="d-flex flex-lg-row">
                    <img src={presion} />

                    <div className="flex-lg-row pl-4">
                      <p className="pt-3 pt-lg-0">PRESIONI I GOMAVE</p>

                      <p>
                        Sistemi i sinjalizimit të presionit të gomës{" "}
                        <Tippy content="Standart, opsionale, ose i padisponueshëm, në varësi të versionit dhe motorit">
                          <span className="isTip">i</span>
                        </Tippy>{" "}
                        informon drejtuesit për presionin në të katër gomat.
                        Informacioni transmetohet duke ndezur dritat në panelin
                        e instrumentit, ose me një sinjal akustik.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="galeria">
          <h2 className="text-center pt-5">GALERIA</h2>
          <Splide
            options={{
              keyboard: "focused",
              type: "loop",
              rewind: true,
              lazyLoad: "nearby",
              height: "auto",
              // autoplay: true,
              pagination: true,
            }}
          >
            <SplideSlide className="d-flex flex-row-reverse pt-4">
              <div className="row">
                <div className="col-lg-3 pr-lg-0">
                  <img
                    className="img-fluid pb-lg-1"
                    data-splide-lazy={p108c2}
                    alt="Image 2"
                  />
                  <img
                    className="img-fluid"
                    data-splide-lazy={nd1}
                    alt="Image 2"
                  />
                </div>
                <div className="col-lg-6 pr-lg-1 pl-lg-1">
                  <img
                    className="w-100"
                    data-splide-lazy={seri108}
                    alt="Image 2"
                  />
                </div>
                <div className="col-lg-3 pl-lg-0">
                  <img
                    className="img-fluid pb-lg-1"
                    data-splide-lazy={gtline}
                    alt="Image 2"
                  />
                  <img
                    className="img-fluid"
                    data-splide-lazy={dizz108}
                    alt="Image 2"
                  />
                </div>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <div className="row">
                <img
                  className="img-fluid col-lg-6 pr-lg-1"
                  data-splide-lazy={front108}
                  alt="Image 2"
                />
                <img
                  className="img-fluid col-lg-6 pl-lg-1"
                  data-splide-lazy={kalume108}
                  alt="Image 2"
                />
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <div className="row">
                <div className="col-lg-6 pr-lg-1">
                  <img
                    className="img-fluid col-lg-6 p-1 p-md-0"
                    style={{ height: "42%" }}
                    data-splide-lazy={permas108}
                    alt="Image 2"
                  />
                  <img
                    className="img-fluid col-lg-6 p-1 p-md-0"
                    style={{ height: "42%" }}
                    data-splide-lazy={perpara108}
                    alt="Image 2"
                  />
                  <img
                    className="img-fluid col-lg-6 p-1 p-md-0"
                    style={{ height: "42%" }}
                    data-splide-lazy={carplay}
                    alt="Image 2"
                  />
                  <img
                    className="img-fluid col-lg-6 p-1 p-md-0"
                    style={{ height: "42%" }}
                    data-splide-lazy={perpara108gj}
                    alt="Image 2"
                  />
                </div>
                <div className="col-lg-6 pl-lg-1">
                  <img data-splide-lazy={front108} alt="Image 2" />
                </div>
              </div>
            </SplideSlide>
            <SplideSlide className="d-flex pt-4">
              <div className="row">
                <img
                  className="img-fluid col-lg-6 h-100 pr-lg-1"
                  data-splide-lazy={p108c1}
                  alt="Image 2"
                />
                <img
                  className="img-fluid col-lg-6 h-100 pl-lg-1"
                  data-splide-lazy={essence}
                  alt="Image 2"
                />
              </div>
            </SplideSlide>
          </Splide>
        </div>

        <div className="crumbi mt-4" id="myHeader2">
          <div className="row">
            <div id="test" className="col-lg-4 text-center">
              <Link to="/automjete/test-drive">TEST DRIVE</Link>
            </div>
            <div id="ofert" className="col-lg-8 pl-5">
              <Link to="/automjete/oferte-automjete">
                KËRKONI OFERTË PËR AUTOMJET{" "}
              </Link>
              <i className="fas fa-angle-right pl-3"></i>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
