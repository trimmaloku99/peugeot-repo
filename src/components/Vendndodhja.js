import React, { Component } from "react";

export default class Vendndodhja extends Component {
  render() {
    return (
      <section id="vendndodhja">
        <h2 className="nav-title">VENDNDODHJA</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              VENDNDODHJA
            </li>
          </ol>
        </div>

        <div id="vendndodhja-details">
          <div id="text" className="col-lg-3">
            <h4>Vendndodhja Peugeot në Kosovë</h4>
            <div className="row">
              <i className="fas fa-map-marker-alt col-lg-4 col-3">Prishtinë</i>
              <p className="col-lg-8">
                Çagllavicë, Kilomteri i dytë në rrugën Prishtinë-Shkup, 10000
                Prishtinë, Republika e Kosovës. +383 45 501 901
              </p>
            </div>
          </div>
          <iframe
            className="col-lg-9"
            src="https://www.google.com/maps/d/embed?mid=11mBE3OptyYXcATH6atQBWM7n0TLxFS6C"
          ></iframe>
        </div>
      </section>
    );
  }
}
