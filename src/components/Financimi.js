import React, { Component } from "react";
import financimi from "../images/financim.jpg";
import afinstitution from "../images/af-institution.png";
import bkt from "../images/bkt.png";
import credins from "../images/credins.png";
import intesa from "../images/intesa.png";
import landeslease from "../images/landeslease.png";
import otplogo from "../images/otp-logo.png";
import raiffeisen from "../images/raiffeisen.png";
import albania from "../images/albania.png";

export default class Financimi extends Component {
  render() {
    return (
      <section id="financimi">
        <h2 className="nav-title">FINANCIMI</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              FINANCIMI
            </li>
          </ol>
        </div>
        <img id="financimi-img" src={financimi}></img>

        <div id="financimi-sponsors">
          <p>
            AVEL Sh.p.k. Tirana, Albania bashkëpunon me të gjitha bankat e
            nivelit të dytë dhe institucionet finaciare që operojnë në
            Republikën e Shqipërisë
          </p>
          <div id="sponsors">
            <a>
              <img src={afinstitution}></img>
            </a>
            <a>
              <img src={bkt}></img>
            </a>
            <a>
              <img src={credins}></img>
            </a>
            <a>
              <img src={intesa}></img>
            </a>
            <a>
              <img src={landeslease}></img>
            </a>
            <a>
              <img src={otplogo}></img>
            </a>
            <a>
              <img src={raiffeisen}></img>
            </a>
            <a>
              <img src={albania}></img>
            </a>
          </div>
          <p>Për më shumë informacion na kontaktoni.</p>
        </div>
      </section>
    );
  }
}
