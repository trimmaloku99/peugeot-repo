import React from "react";
import { Link } from "react-router-dom";
import servicing1 from "../images/servicing1.jpg";
import servicing2 from "../images/servicing2.jpg";
import servicing3 from "../images/servicing3.jpg";

function Servicing() {
  return (
    <section className="pt-5 text-center">
      <div id="servicing" className="card-group">
        <div className="card">
          <Link to="/sherbime-servis/kit-distributori">
            <img
              className="card-img-top"
              src={servicing1}
              alt="Card cap"
              width="400px"
            />
          </Link>
        </div>

        <div className="card">
          <Link to="/sherbime-servis/kontrolli-i-kondicionerit">
            <img
              className="card-img-top"
              src={servicing2}
              alt="Card cap"
              width="400px"
            />
          </Link>
        </div>

        <div className="card">
          <Link to="/sherbime-servis/nderrimi-i-filtrave">
            <img
              className="card-img-top"
              src={servicing3}
              alt="Card cap"
              width="400px"
            />
          </Link>
        </div>
      </div>
    </section>
  );
}

export default Servicing;
