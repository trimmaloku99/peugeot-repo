import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import $ from "jquery";
window.$ = $;

class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      $("html,body").animate({
        scrollTop: 0,
      });
    }
  }

  render() {
    return <React.Fragment />;
  }
}

export default withRouter(ScrollToTop);
