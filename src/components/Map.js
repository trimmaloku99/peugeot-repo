import React, { Component } from "react";
import $ from "jquery";
window.$ = $;

export default class Map extends Component {
  componentDidMount() {
    // $(window).bind("scroll", function () {
    //   if (
    //     $(window).scrollTop() >=
    //     $("#footer-content").offset().top +
    //       $("#footer-content").outerHeight() -
    //       window.innerHeight
    //   ) {
    //     $("#go-top a").fadeIn("slow");
    //     $("#go-top a").removeClass("hide");
    //     $("#go-top a").addClass("show");
    //   } else {
    //     $("#go-top a").fadeOut("slow", function () {
    //       $("#go-top a").removeClass("show");
    //       $("#go-top a").addClass("hide");
    //     });
    //   }
    // });
  }
  render() {
    return (
      <section className="pt-5">
        <h2 className="text-center p-4">NA GJENI KËTU</h2>
        <div id="map">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2936.4848719329952!2d21.1354798156984!3d42.60867172735723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13549db3984b59bd%3A0xbe7eb67e78d9457c!2sPEUGEOT!5e0!3m2!1sen!2s!4v1596546130604!5m2!1sen!2s"
            width="600"
            height="450"
            frameBorder="0"
            style={{
              border: "0",
              height: "200px",
              padding: "20px",
              width: "100%",
            }}
            allowFullScreen=""
            aria-hidden="false"
            tabIndex="0"
          ></iframe>
        </div>
      </section>
    );
  }
}
