import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Vetura extends Component {
  render() {
    return (
      <Link
        onClick={this.props.kliku}
        className={this.props.classname}
        to={this.props.hrefi}
      >
        <img src={this.props.image} height={this.props.height} alt="card" />
        <p>{this.props.modelName}</p>
      </Link>
    );
  }
}
