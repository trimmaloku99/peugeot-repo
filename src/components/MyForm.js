import React from "react";

export default class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: "",
    };
  }

  render() {
    const { status } = this.state;
    return (
      <form
        onSubmit={this.submitForm}
        action="https://formspree.io/xyynvopd"
        method="POST"
        id="myform"
      >
        <div className="form-neccessary p-3">
          <label>* Emër dhe mbiemër</label>
          <input type="text" name="Emri" required />
          <label>* Е-mail</label>
          <input
            type="email"
            name="Emaili"
            maxLength="64"
            data-error="email"
            required
          />
          <label>* Nr. telefoni (pa +383)</label>
          <input
            type="text"
            name="Numri i telefonit"
            maxLength="64"
            data-minlength="6"
            placeholder="04"
            pattern="[0-9]{3}[0-9]{3}[0-9]{3}"
            required
          />
          <label>* Nr. i shasisë</label>
          <input
            type="text"
            name="Numri i shasisë"
            maxLength="64"
            placeholder="VF"
            data-minlength="7"
            pattern="^VF[a-zA-Z0-9]{15}$"
            required
          />
        </div>
        <div className="form-group backu">
          <label>Informacione shtesë:</label>
          <textarea
            type="textarea"
            name="Informacione shtesë"
            className="form-control"
            rows="3"
          ></textarea>
        </div>
        <div id="checkbox-form">
          <input id="checkbox-kerkesa" type="checkbox" required />
          <label for="checkbox-kerkesa" className="pl-2 mb-0">
            * Unë dëshiroj të marr njoftime, lajme dhe oferta nga PEUGEOT
          </label>
        </div>
        {status === "SUCCESS" ? (
          <div className="alert alert-success" role="alert">
            Dërgesa juaj u bë me sukses!
          </div>
        ) : (
          <button
            type="submit"
            className="more-btn align-self-center mt-4 mb-4"
          >
            DËRGO
          </button>
        )}
        {status === "ERROR" && (
          <div className="alert alert-danger" role="alert">
            Diqka nuk shkoi në rregull!
          </div>
        )}
      </form>
    );
  }

  submitForm(ev) {
    ev.preventDefault();
    const form = ev.target;
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}
