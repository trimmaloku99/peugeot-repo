// Customize this 'myform.js' script and add it to your JS bundle.
// Then import it with 'import MyForm from "./myform.js"'.
// Finally, add a <MyForm/> element whereever you wish to display the form.

import React from "react";

export default class OferteForm extends React.Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: "",
    };
  }

  render() {
    const { status } = this.state;
    return (
      <form
        onSubmit={this.submitForm}
        action="https://formspree.io/mwkrzjpb"
        method="POST"
        id="oferteform"
      >
        <div className="form-neccessary">
          <div id="zgjedh-modelin" className="p-3">
            <label>* Zgjidhni modelin:</label>
            <select id="modeli-vetures" name="Modeli" required>
              <option selected="true" value="" disabled="disabled">
                -
              </option>
              <option value="108">108</option>
              <option value="208">208</option>
              <option value="2008">2008</option>
              <option value="301">301</option>
              <option value="308">308</option>
              <option value="3008">3008</option>
              <option value="saab">508</option>
              <option value="5008">5008</option>
              <option value="Rifter">Rifter</option>
              <option value="Traveller">Traveller</option>
              <option value="Partner">Partner</option>
              <option value="Expert">Expert</option>
              <option value="Boxer">Boxer</option>
            </select>

            <br />
            <label>* Zgjidhni kambion:</label>
            <select id="kambio" name="Kambio" required>
              <option selected="true" disabled="disabled" value="">
                -
              </option>
              <option value="Manuale">Manuale</option>
              <option value="Automatike">Automatike</option>
            </select>
            <br />
            <label>* Zgjidhni vellimin:</label>
            <select id="vellimi" name="Vellimi" required>
              <option selected="true" disabled="disabled" value="">
                -
              </option>
              <option value="1.2 L BENZINE">1.2 L BENZINE</option>
              <option value="1.5 L NAFTE">1.5 L NAFTE</option>
              <option value="1.6 L BENZINE">1.6 L BENZINE</option>
              <option value="2.0 L NAFTE">2.0 L NAFTE</option>
            </select>
            <br />
          </div>
          <div className="col-lg-12 backu">
            <span>TE DHENA PERSONALE</span>
          </div>
          <div id="te-dhena" className="p-3">
            <label>* Emër dhe mbiemër</label>
            <input type="text" name="Emri" required />
            <label>* Е-mail</label>
            <input
              type="email"
              name="Emaili"
              maxLength="64"
              data-error="email"
              required
            />
            <label>* Nr. telefoni (pa +383)</label>
            <input
              type="text"
              name="Numri i telefonit"
              maxLength="64"
              data-minlength="6"
              placeholder="04"
              pattern="[0-9]{3}[0-9]{3}[0-9]{3}"
              required
            />
            <div className="form-group backu">
              <label>Informacione shtesë:</label>
              <textarea
                type="textarea"
                name="Informacione shtesë"
                className="form-control"
                rows="3"
              ></textarea>
            </div>
            <div id="checkbox-form">
              <input id="checkbox-kerkesa" type="checkbox" required />
              <label className="pl-2 mb-0">
                * Unë dëshiroj të marr njoftime, lajme dhe oferta nga PEUGEOT
              </label>
            </div>
          </div>
        </div>
        {status === "SUCCESS" ? (
          <div className="alert alert-success" role="alert">
            Dërgesa juaj u bë me sukses!
          </div>
        ) : (
          <button
            type="submit"
            className="more-btn align-self-center mt-4 mb-4"
          >
            DËRGO
          </button>
        )}
        {status === "ERROR" && (
          <div className="alert alert-danger" role="alert">
            Diqka nuk shkoi në rregull!
          </div>
        )}
      </form>
    );
  }

  submitForm(ev) {
    ev.preventDefault();
    const form = ev.target;
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}
