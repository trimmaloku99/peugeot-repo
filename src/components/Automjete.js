import React, { Component } from "react";
import automjete from "../images/automjete.jpg";

export default class Automjete extends Component {
  render() {
    return (
      <section id="automjete">
        <h2 className="nav-title">AUTOMJETE TË PËRDORURA</h2>
        <div aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="/">FAQJA KRYESORE</a>
            </li>
            <li className="breadcrumb-item" aria-current="page">
              AUTOMJETE TË PËRDORURA
            </li>
          </ol>
        </div>
        <img id="automjete-img" src={automjete}></img>

        <div id="description-panel-automjete">
          <h5>
            Mundësi e përkryer për të blerë një automjet të përdorur në kushte
            të shkëlqyera!
          </h5>
          <p>
            Për të gjitha automjetet e përdorura sipas kësaj oferte, Peugeot
            garanton: <br></br>● Origjinën e automjetit<br></br>● Km e kryera{" "}
            <br></br>● Sigurinë e saj
          </p>
          <h5>Tani keni një zgjedhje!</h5>
        </div>
      </section>
    );
  }
}
