import React from "react";
import { Link } from "react-router-dom";
import foryou1 from "../images/foryou1.jpg";
import foryou2 from "../images/foryou2.jpg";
import foryou3 from "../images/foryou3.jpg";

function FirstSection() {
  return (
    <section>
      <p className="p-3">
        {" "}
        Në zbatim të PROTOKOLLIT TË VERDHË të miratuar nga Ministria e
        Shëndetësisë, AVEL Sh.p.k. po zbaton dhe ndjek në mënyrë të përpiktë të
        gjitha masat higjeno sanitare të këtij protokolli. Klikoni në linkun më
        poshtë për t'u njohur me masat higjeno sanitare sipas këtij protokolli.{" "}
      </p>
      <a
        id="protocol-btn"
        href="https://www.peugeot.al/PROTOKOLLI_VERDHE_PERGJITHSHEM.pdf"
        target="_blank"
        rel="noopener noreferrer"
      >
        PROTOKOLLI I VERDHË I MASAVE HIGJENO SANITARE
      </a>
      <h2 className="p-4 text-center">Gjithmonë këtu për ju</h2>
      <div id="for-you" className="card-group">
        <div className="card">
          <img
            className="card-img-top"
            src={foryou1}
            alt="Card"
            width="400px"
          />
          <Link className="card-btn" to="/automjete/oferte-automjete">
            KËRKONI OFERTË PËR AUTOMJET
          </Link>
        </div>

        <div className="card">
          <img
            className="card-img-top"
            src={foryou2}
            alt="Card"
            width="400px"
          />
          <Link className="card-btn" to="/sherbime-servis/pjese">
            KËRKESË PËR SERVIS DHE PJESË KËMBIMI
          </Link>
        </div>

        <div className="card">
          <img
            className="card-img-top"
            src={foryou3}
            alt="Card"
            width="400px"
          />
          <Link className="card-btn" to="/automjete/test-drive">
            TEST DRIVE
          </Link>
        </div>
      </div>
    </section>
  );
}

export default FirstSection;
