import React from "react";
import ScrollToTop from "./components/ScrollToTop";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Vendndodhja from "./components/Vendndodhja";
import Financimi from "./components/Financimi";
import Automjete from "./components/Automjete";
import Automjetet from "./components/Automjetet";
import TestDrive from "./components/TestDrive";
import SherbimeServis from "./components/SherbimeServis/SherbimeServis";
import SistemiFrenimit from "./components/SherbimeServis/SistemiFrenimit";
import KerkeseKembimi from "./components/SherbimeServis/KerkeseKembimi";
import IntervaliSherbimit from "./components/SherbimeServis/IntervaliSherbimit";
import KontrollKondicioner from "./components/SherbimeServis/KontrollKondicioner";
import AntiAlergji from "./components/SherbimeServis/AntiAlergji";
import KitDistributori from "./components/SherbimeServis/KitDistributori";
import KontrolliBaterise from "./components/SherbimeServis/KontrolliBaterise";
import PaketaXhamave from "./components/SherbimeServis/PaketaXhamave";
import NderrimiFiltrave from "./components/SherbimeServis/NderrimiFiltrave";
import RrethNesh from "./components/RrethNesh";
import Kontakt from "./components/Kontakt";
import Politikat from "./components/Politikat";
import HartaFaqes from "./components/HartaFaqes";
import Oferta from "./components/Oferta/Oferta";
import OferteAutomjete from "./components/OferteAutomjete";
import Lajme from "./components/Lajme/Lajme";
import LajmePartner from "./components/Lajme/LajmePartner";
import LajmeDakar from "./components/Lajme/LajmeDakar";
import Lajme508 from "./components/Lajme/Lajme508";
import LajmePuretech from "./components/Lajme/LajmePuretech";
import ActiveDrive from "./components/Oferta/ActiveDrive";
import WinterDrive from "./components/Oferta/WinterDrive";
import Peugeot108 from "./components/Modelet/Peugeot108";
import Peugeot208 from "./components/Modelet/Peugeot208";
import Peugeot2008 from "./components/Modelet/Peugeot2008";
import Footer from "./components/Footer";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      {/* <ScrollToTop> */}
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route exact path="/vendndodhja" component={Vendndodhja}></Route>
          <Route exact path="/financimi" component={Financimi}></Route>
          <Route exact path="/rreth-nesh" component={RrethNesh}></Route>
          <Route exact path="/kontakt" component={Kontakt}></Route>
          <Route exact path="/politikat" component={Politikat}></Route>
          <Route exact path="/harta-e-faqes" component={HartaFaqes}></Route>
          {/* oferta */}
          <Route exact path="/oferta" component={Oferta}></Route>
          <Route
            exact
            path="/oferta/peugeot-active-drive"
            component={ActiveDrive}
          ></Route>
          <Route
            exact
            path="/oferta/peugeot-winter-drive"
            component={WinterDrive}
          ></Route>

          {/* lajme */}
          <Route exact path="/lajme" component={Lajme}></Route>
          <Route
            exact
            path="/lajme/partner-furgoni-nderkombetar-i-vitit"
            component={LajmePartner}
          ></Route>
          <Route exact path="/lajme/dakar-2018" component={LajmeDakar}></Route>
          <Route
            exact
            path="/lajme/508-makina-me-e-bukur"
            component={Lajme508}
          ></Route>
          <Route
            exact
            path="/lajme/puretech-motori-vitit"
            component={LajmePuretech}
          ></Route>

          {/* automjete */}
          <Route exact path="/automjete" component={Automjetet}></Route>
          <Route
            exact
            path="/automjete/peugeot-108"
            component={Peugeot108}
          ></Route>
          <Route
            exact
            path="/automjete/peugeot-208"
            component={Peugeot208}
          ></Route>
          <Route
            exact
            path="/automjete/peugeot-2008"
            component={Peugeot2008}
          ></Route>
          <Route
            exact
            path="/automjete/automjete-te-perdorura"
            component={Automjete}
          ></Route>
          <Route
            exact
            path="/automjete/oferte-automjete"
            component={OferteAutomjete}
          ></Route>
          <Route
            exact
            path="/automjete/test-drive"
            component={TestDrive}
          ></Route>

          {/* sherbime servis */}
          <Route
            exact
            path="/sherbime-servis"
            component={SherbimeServis}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/intervali-sherbimit"
            component={IntervaliSherbimit}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/pjese"
            component={KerkeseKembimi}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/sistemi-i-frenimit"
            component={SistemiFrenimit}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/kontrolli-i-kondicionerit"
            component={KontrollKondicioner}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/filtri-i-antialergjise"
            component={AntiAlergji}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/kit-distributori"
            component={KitDistributori}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/kontrolli-i-baterise"
            component={KontrolliBaterise}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/paketa-e-xhamave"
            component={PaketaXhamave}
          ></Route>
          <Route
            exact
            path="/sherbime-servis/nderrimi-i-filtrave"
            component={NderrimiFiltrave}
          ></Route>
        </Switch>
        <Footer />
      </div>
      <ScrollToTop />
    </Router>
  );
}

export default App;
